---
layout: article
title: Test concept of the Swiss Post voting system
author: © 2024 Swiss Post Ltd.
date: "2024-05-02"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Test concept of the Swiss Post Voting System

## Introduction

This document describes the test concept for the Voting System. The test concept shows the test procedure and defines the cooperation and the responsibilities of all persons involved in the test process. It contains the requirements for the tests, the necessary test environment (including the tools used and their application guidelines), and the test stages to be carried out (including acceptance criteria). It also defines which type of test is to be used at which test level, under which conditions and in which system environment. The test concept also ensures conformity with the Ordinance of the Federal Chancellery (FC) on Electronic Voting (OEV).

The Swiss Post Voting System enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various software components and the system architecture are explained in the [system specification and system architecture documents](../System).

## Test procedure

The test procedure is based, among other things, on the software development process, the organisation and the internal requirements of Swiss Post\'s IT department. The software is developed according to an agile process model. The Post Agile Methodology (PAM) is a process model adapted to Swiss Post IT. It is based, among other things, on the Scrum process model. A Scrum team develops the software in two-week sprints. Software Development, Swiss Post Ltd and Test Management and the cantons are involved in the tests. Organisationally, Software Development and Test Management are independent units. Five stages are defined for the test process. The diagram below shows the test procedure accordingly.

![Graphic of the test procedure -- representation of five stages](../.gitlab/media/test_concept/image2.png)

### Stage 0

Software Development continuously performs manual and automated tests on the development environment. This includes unit tests, integration tests and end-to-end tests. Due to the principle of segregation of duties, Software Development has exclusive access to the development environment. The transfer to test management takes place via a software release. This is depicted in the diagram by the yellow dot "Trusted build (unobserved)". Product Management is responsible for release planning and the product road map. As soon as a release is ready, it is deployed to the internal test and integration environments.

### Stages 1 and 2

Stage 1 provides for the smoke tests after the deployment and installation tests. The smoke tests include, for example, "Does the ballot box work?" and "Tests with different numbers of ballot boxes and sizes".

At this stage, the deployment to other test environments is carried out. The tests to be carried out include the function and regression tests and the security tests.

### Stage 3

In Stage 3, the system tests are carried out. These always include installation tests, functional tests and regression tests. If required, additional tests such as security tests, business logic security tests, load and performance tests, penetration tests, disaster and recovery tests, bug bounty tests or accessibility tests are also carried out at this stage. The different types of tests can sometimes be carried out on different environments in order to reduce the turnaround time.

Not all tests are carried out for every software release, but only when required. The need is defined in consultation with Product Management and Development depending on the changes compared to the previous release, the use of the release (delivery to the cantons, interim release) and any other dependencies. These tests are listed as "optional tests" (load / performance tests, external penetration tests, disaster and recovery, accessibility) in the diagram. Depending on the defined tests, different system environments are used. 

After the internal tests have been carried out, the release is handed over to the cantons. The release is installed on the cantons' integration environment. Each canton has its own system environment. If the tests are successful, the release is made productive.

### Stage 4

In Stage 4, the cantons carry out their tests and acceptance on their respective integration environments. The selection and scope of the tests is up to the cantons, but they are informed about new features and bug fixes in the release notes.

### Stage 5

The release is created as a trusted build (observed). After deployment, the installation is checked and tested on the production environment. Smoke tests are also carried out and, finally, the end-to-end of the cantons. This stage is a repetition of the acceptance tests in stage 4 and serves to ensure the compatibility of the tested release with the canton's productive environment. The software release is then approved.

The next chapter gives an overview of the quality features for the software products.

#### Overview of quality features

The software is tested according to Post CH Ltd's internal specifications and the requirements of the Ordinance of the Federal Chancellery on Electronic Voting (VEleS). The internal test specifications are based on the ISO 25010 standard. The following diagram shows the quality features according to ISO 25010. The quality features are covered by the corresponding test types.

![Graphic of ISO 25010](../.gitlab/media/test_concept/image3.png)

The requirements of the VEleS are defined in the requirements catalogue "Technical and administrative requirements for electronic voting". All information can be found on the Federal Chancellery website.

Link: <https://www.bk.admin.ch/bk/de/home/politische-rechte/e-voting.html>

#### Test levels

The test activities are based on the general V model. As a rule, the test activities can be divided into two categories. Firstly, there are the static tests, such as the review of documents or code, which are listed on the left-hand side of the V model. Secondly, there are the dynamic tests on the right-hand side, in which the test objects are executed. Here, both the functional and non-functional quality features are checked.

The change from one test level to the next takes place only when the current test level delivers no or only a few defects. If the defect density is still too high, numerous subsequent defects can occur in the next test level, which mask the real defects that should be detected in that level. The test stages do not always have to be conducted in a purely sequential manner. Different test objects can be placed in different test stages. In an iterative incremental procedure (for example, Scrum) in particular, a test object can perform several test stages again and again very quickly one after the other. The following graphic shows the test levels used.

![Graphic of the test levels](../.gitlab/media/test_concept/image4.png)

The test levels are described in detail in the following chapters. Listed are the test level with a description, the test system, which test data are used, and the start, end and termination conditions.

##### Test level - unit test

The unit test is the responsibility of the developer. A unit test executes the smallest piece of testable software in the application to check that it behaves as expected. Unit tests are defined as follows:

- Unit tests must be carried out with each merge
- Multiple runs must always produce the same result
- Implementation via Jenkins ([Jenkins server]([https:](https://jenkinsevoting.tools.pnet.ch/)//jenkinsevoting.tools.pnet.ch))
- Multiple runs should always produce the same result
- Code coverage of the unit test is defined for each unit in accordance with VEleS
- Local test data packaged with the unit test, defined by the developers
- Each unit test should be independent of all other unit tests

##### Test level - component test

The component test is the responsibility of the test team. It is a white box test, i.e. the behaviour of the component is examined, taking into account the concrete implementation.

The component tests cover the following aspects:

- Each user story is recorded in at least one test case

- All possible alternatives for a process are captured in at least one test case

The general test objective is to test all basic functions.

###### Test system - component test

- Development and/or specific test platform for software development.

###### Test data - component test

- The test data from the test data inventory is used.

###### Start, end and termination conditions - component test

- Start

  - The test is started when the work on a component has been completed (creation, further development or bug fixing of a component)

- End

  - If the component test for a component has been carried out successfully, the test can be ended.

- Termination

  - If the test was not completed successfully, it is aborted and the developer analyses the defect as the first step. If the developer cannot correct the defect, the test is cancelled and a bug fix is planned for the component.

##### Test level - integration test

In the integration tests (interaction tests), the cooperation of the components from the component test is checked. The focus is not on the complete E2E process, but on the individual interfaces themselves. These tests are usually the responsibility of the developer. They are also white box tests. Because the number of implemented functions is increasing, the focus will increasingly be on integration tests. One of the main goals is to test the interaction of the different system components. In addition, there are tests for interaction with external systems via the respective interfaces.

###### Test system - integration test

- The tests from the integration test level are carried out as follows with regard to the system and test environment used:
- The tests are carried out by Swiss Post on the test IT environment (IT-TEST).
- The tests are carried out by the canton on the corresponding cantonal test environment (TG-TEST and other cantonal environments).

###### Test data - integration test

- The test data that is used is always based on the latest version of the eCH standard for e-voting systems and from votes that have been conducted. The following eCH standards are used:
  - eCH-0045 (voting register)
  - eCH-0157 (candidates and lists)
  - eCH-0159 (questioning for voting proposals)

###### Start, end and Termination conditions - integration test

The conditions listed below must be fulfilled before the integration tests can be started.

- Start
  - The developer has successfully completed the unit tests, the results have been logged and no aspects of these tests are pending
  - The corresponding new software release was made available for the integration and system test platform and released for testing

- End

  - The tests have been carried out and the acceptance criteria have been met

- Termination

  - Blocking errors are present that make testing of the system or a software module impossible (the product owner and the test manager must make a decision together)
  - An accumulation of defects has led to the fact that further tests of the system or of a software module are no longer useful (the product owner and the test manager have to make a decision together)^
  - Other reasons exist, such as missing or non-functioning peripheral systems, or missing or non-functioning systems

- Resumption

  In order for the tests to be resumed, the following requirements must be met:

- There is a confirmation that the blocking errors that occurred have been eliminated and that, at a minimum, all changed components and the peripheral systems involved have been successfully checked and tested by the supplier

- The test platform and infrastructure must be available and prepared as before the test termination.

##### Test level - system test

In the system test, the system is tested against the requirements in an environment as close to production as possible. These are black box tests, which means that only the behaviour of the system is analysed externally.

The system tests cover the following aspects:

- Check whether the functional expectations and requirements are met

- Check whether all components are implemented correctly

- Check if the system output is correct

- Check whether the results of the load and performance tests meet the defined expectations

The regression tests guarantee that new software releases do not affect existing system functions. Due to the large number of software releases, automatic regression tests will be considered in the future.

###### Test system - system test

- The tests in the system test level and regression tests are carried out as follows with regard to the system and test environment used:
- The test types E2E function test, bug bounty tests, accessibility test and source code analysis are carried out on the test IT environment (IT-TEST)
- The test types penetration test, business logic security test, load and performance test, and disaster and recovery test are carried out on the corresponding cantonal test environment (TG-TEST)

###### Test data  - system test

- The test data that is used is always based on the latest version of the eCH standard for e-voting systems and from votes that have been conducted. The following eCH standards are used.
  - eCH-0045 (voting register)
  - eCH-0157 (candidates and lists)
  - eCH-0159 (questioning for voting proposals)

###### Start, end and Termination conditions - system test

The conditions listed below must be fulfilled before the system tests can be started.

- Start

  - The integration tests have been carried out successfully, the results have been logged and there are no unaccepted defects unresolved
  - The corresponding new software release has been made available for the integration and system test platform and released for testing
- End
  - The tests have been carried out and the acceptance criteria have been met
- Termination
  - Blocking errors exist that make the testing of the system or a software module impossible (the product owner and the test manager must make a decision together)
  - An accumulation of defects has led to the fact that further tests of the system or of a software module are no longer useful (the product owner and the test manager have to make a decision together)
  - Other reasons exist, such as missing or non-functioning peripheral systems, or missing or non-functioning systems

- Resumption
  In order for the tests to be resumed, the following requirements must be met:
  - Confirmation is available that the blocking errors that occurred have been rectified and that, at a minimum, all modified components and the peripheral systems involved have been successfully checked and tested by the supplier

  - The test platform and infrastructure must be available and prepared as before the test termination

##### Test level - acceptance test

The acceptance test is not used to find errors, unlike the previous test stages! This test stage checks whether the software can also be used in the productive environment and whether all specified criteria are met. The aim is to provide a basis for the decision to go live.

The acceptance tests are used to check whether the contractually agreed acceptance criteria for the e-voting system are fulfilled with regard to BUC, technical, operational and security aspects. In addition to the main aspects of the tests that have already been mentioned, the connection to the e-voting system from other countries and the system performance in such cases are also integral test points.

###### Test system  - acceptance test

- The tests carried out by the canton during the acceptance phase of the e-voting system are based on the real productive environment. No other tests may be carried out in the productive environment

###### Test data - acceptance test

- The test data used is always based on the latest version of the eCH standard for e-voting systems and from votes that have been conducted. The following eCH standards are used:

  - eCH-0045 (voting register)
  - eCH-0157 (candidates and lists)
  - eCH-0159 (questioning for voting proposals)

###### Start, end and abort conditions - acceptance test

The conditions listed below must be fulfilled before the system tests can be started:

- Start

  - The system tests have been successfully performed, the results have been logged and no aspects of these tests are pending
  - The corresponding new software release has been made available for the acceptance test platform and released for testing

- End
  - The tests have been carried out and the acceptance criteria have been met

- Termination

  - Blocking errors are present that make the testing of the system or a software module impossible (the product owner and the test manager must make a decision together)

  - An accumulation of defects has led to the fact that further testing of the system or a software module is no longer useful (the product owner and the test manager must make a decision together)
  - Other reasons exist, such as missing or non-functioning peripheral systems, or missing or non-functioning systems

- Resumption
   In order for the tests to be resumed, the following requirements must be met:
  - There is confirmation that the blocking errors that occurred have been rectified and that, at a minimum, all changed components and the peripheral systems involve have been successfully checked and tested by the supplier

  - The test platform and infrastructure must be available and prepared as before the test termination

The next chapter describes the test objects.

## Test objects

This chapter lists the test objects. Test objects are the components or systems that are to be tested. The test objects are shown in the diagram below. The different colours each correspond to a defined test object.

![Graphic of the test objects](../.gitlab/media/test_concept/image5.png)

A description and the interfaces of the test objects are listed below:

### "Voter Portal" test object 

Web-based interface, execution of voting Interfaces

- Voting Server

### "Secure Data Manager (SDM)" test object

Locally installed program for uploading, editing and configuring ballot rounds. Interfaces

- Voting Server

### "Verifier" test object

The e-voting system produces evidence of the correct determination of results, which the Verifiers check using a technical tool (verification software, or Verifier for short).

The next chapter describes the acceptance criteria at each test level.

## End of test and acceptance criteria

The acceptance criteria for each test level are listed below.

### Test level - Component test

#### Participant - Component test

- Development

#### Acceptance criteria - Component test

- The component tests have been carried out and recorded on a test system prior to delivery and installation.

### Test level - Integration test

#### Participant - Integration test

- Development
- Testing

#### Acceptance criteria - Integration test

Test cases specified according to test sets were executed in order to check that:

- The new release is deployed correctly on the integration and works.
- The interfaces to the surrounding systems work.
- Non-functional tests can be successfully carried out.

Tickets from previous test rounds and releases have been resolved or scheduled for subsequent test rounds or releases in agreement with the test manager, application manager and specialist service.

The tickets that have been solved according to the release notes have been successfully retested.

Maximum of 0 blocker, maximum of 5 critical and 20 other defects.

### Test level - System test

#### Participant - System test

- Testing

#### Acceptance criteria - System test

100% coverage of the requirements with priority MUST.

Test cases specified according to test sets were executed to check that:

- The implementation of the different screens is correct.
- The translations of the screens and forms are correct.
- The processes are implemented correctly and work.

Tickets from previous test rounds and releases have been resolved or scheduled for subsequent test rounds (acceptance tests) or releases in agreement with the test manager, application manager and specialist service.

The tickets that have been solved according to the release notes have been successfully retested.

Regression test and continuity test have been successfully carried out.

Maximum of 0 blocker, 4 critical and 15 other defects.

### Test level - Acceptance test

#### Participant - Acceptance test

- Customer / Cantons

#### Acceptance criteria - Acceptance test

100% coverage of the requirements with priority MUST.
Test cases specified according to test sets were executed to check that:

- The processes are implemented correctly and work.
- Non-functional tests can be successfully carried out.

The tickets that have been solved according to the release notes have been successfully retested.

Regression test and continuity test have been successfully carried out.

Maximum of 0 blocker, 3 critical and 10 other defects.

The number of critical defects must not have the same combined effect as a blocking defect.

With the client’s consent, the specialist service is free to issue a GO for the start of production, even if the number of defects is higher.

Unresolved defects must be prioritised after the GO and rescheduled for the next release or possibly for a hotfix.

### Test types

This chapter gives an overview of the types of tests. The following table lists the test types with a description:

#### Test type - Unit tests / component tests

There are white box tests, carried out by a developer, i.e. the behaviour of the component is examined, taking into account the concrete implementation.

Implementation by Development after each merge

#### Test type - Integration tests

In the integration tests (interaction tests), the cooperation of the components from the component test is checked. The focus is not on the complete E2E process, but on the individual interfaces themselves. These tests are usually the responsibility of the developer. These are also white box tests (see above). They are usually also carried out in the development environment. Often the boundary between component test and integration test is fluid.

Implementation by Development after each release by Testing after each release

#### Test type - Installation tests

These tests check whether software can be installed properly.  

Implementation by Testing after each release

#### Test type - Smoke tests

Smoke tests should cover the rough functional testing of the main functionalities of the defined test objects, as specified in the relevant user manuals.

Implementation by Testing after each release

#### Test type - Regression tests

If a system is expanded or changed, the new or added functionality must be thoroughly tested. However, the parts of the system that have not been changed or added can indirectly be affected and thus become defective in previously error-free areas (regression). In order to exclude such defects, tests that have already been successfully carried out are repeated. These tests are called regression tests. Depending on the risk assessment, all tests that have been carried out so far or a part of them are repeated.

Implementation by Testing after each release, but not always the full scope of the tests

#### Test type - Functional / non-functional tests

The tests can also be differentiated according to the type of requirements. In functional tests, the actual, mostly technical functionality of a software is tested. In non-functional tests, all other properties of the software are tested.

Implementation by Testing after each release, but not always the full scope of the tests

#### Test type - End-to-end tests

The test cases for the end-to-end tests are derived from the requirements issued by the Federal Chancellery and TAF.

Implementation by Testing after each release

#### Test type - Load and performance tests

In addition to manual tests, automated tests are also carried out using suitable test automation tools, with reference to regression tests in order to check performance.

Implementation by Testing after specific releases*

#### Test type - Disaster and recovery tests

The disaster and recovery tests ensure that the failure of the secondary data center has no noticeable impact on the functionality and behaviour of the e-voting system for voters. Voting by means of the load simulation should be able to continue without interruptions or restrictions.

Implementation by Testing after specific releases*

#### Test type - Accessibility tests

The accessibility tests check whether users with impairments in seeing, hearing, moving or processing information can use a website without restriction.

Implementation by Testing after specific releases*

#### Test type - Business logic security tests

Based on the defined API of the e-voting system, the business logic security tests serve to ensure the security of the e-voting platform with regard to external access and manipulation risks. The external testers use their own independent voting clients for this purpose. These are compatible with the existing API of the e-voting system.

Implementation by Testing (external) after specific releases*

#### Test type - Penetration testing

These tests are carried out based on the audit points OWASP Top 10, OWASP-ASVS and OWASP-OTG and include ensuring the security of the e-voting platform with regard to external access and manipulation risks.

Implementation by Testing (external) after specific releases*

#### Test type - Auditability

In the context of the publication of the source code, the prior analysis of the source code serves to verify and ensure the flawless quality of the e-voting system source code.

Implementation by Development (external) after specific releases*

> *For more information on which tests are carried out after a release, see chapter "Test procedure".

The following chapters shows which test type is to be applied to which test object at which test level. The quality features specified above must be covered in this table by corresponding test types.

#### Test object - Voter Portal (VP)

##### Component test - Voter Portal (VP)

- Smoke tests
- Unit tests

##### Integration test - Voter Portal (VP)

- Smoke tests
- Installation tests
- Integration tests
- End-to-end tests
- Auditability

##### System test - Voter Portal (VP)

- Smoke tests
- Regression tests
- Functional / non-functional tests
- Load and performance tests
- Security tests

##### Acceptance test - Voter Portal (VP)

- Smoke tests
- Regression tests
- End-to-end tests

#### Test object - Secure Data Manager (SDM)

##### Component test - Secure Data Manager (SDM)

- Smoke tests
- Unit tests

##### Integration test - Secure Data Manager (SDM)

- Smoke tests
- Installation tests
- Integration tests
- End-to-end tests
- Auditability

##### System test - Secure Data Manager (SDM)

- Smoke tests
- Regression tests
- Functional / non-functional tests
- Load and performance tests
- Security tests

##### Acceptance test - Secure Data Manager (SDM)

- Smoke tests
- Regression tests
- End-to-end tests

#### Test object - Integration tools

###### Component test - Integration tools

- Smoke tests
- Unit tests

##### Integration test - Integration tools

- Smoke tests
- Installation tests
- Integration tests
- End-to-end tests
- Auditability

##### System test - Integration tools

- Smoke tests
- Regression tests
- Functional / non-functional tests
- Load and performance tests
- Security tests

##### Acceptance test - Integration tools

- Smoke tests
- Regression tests
- Functional / non-functional tests
- End-to-end tests

### Test design

The requirements are based on the Federal Chancellery's document "Technical and administrative requirements for electronic voting". The test manager is responsible for creating and updating the test cases. The test cases were created in accordance with these requirements.

The test cases are revised or new test cases are created in the following circumstances:

- The requirements issued by the Federal Chancellery are changing

- The software is modified; for example, through adaptations in the source code, bug fixes, architecture changes, change requests

- Bugs indicate that an area needs to be retested or tested differently to detect such bugs in the future

### Test data

This chapter describes what test data is used. The administration of the eCH test data that is used -- eCH-0045 (electoral register), eCH-0157 (candidates and lists) and eCH-0159 (questions for voting proposals) is centralised and handled by the test manager. Changes to the eCH test data inventory are communicated and documented by the test manager.

When using test data from productive operation, the consent of the data owner must be obtained. The process "Provision of productive data for test purposes", which is documented in Adonis, must be taken into account. This ensures that the consent of the data owner is available.

## Test infrastructure

This chapter describes which software and hardware is used for the execution and management of the tests. The changes, bugs or new features are managed in Atlassian Jira. Atlassian Jira is a comprehensive task management tool for a wide range of use cases and offers a wide range of configurations for issues, processes and reports. For test management, the add-on Xray is used. This is specifically designed for testing and test management and works seamlessly with Jira. Docker is partially used for test environments. Docker is a free software for creating container virtualisations.

The following test environments are relevant in testing:

| **Test environment** | **Comment**               |
| -------------------- | ------------------------- |
| XX-PROD              | Internal test environment |
| XX-TEST              | Docker test environment   |

### Tools testing

The tests are carried out by Software Development and Test Management. In addition to the manual tests, the tests are carried out automatically. The following is a list of the tools used with the test objectives and the test scope:

| **Software**                          | **Description**                                              | **Test objectives**                                          | **Test scope**                  |
| ------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------- |
| TOSCA                                 | Software tool for the automated execution of functional and regression tests with integrated test management by TRICENTIS Technology & Consultirng GmbH | Perform automated tests on VP. (Regression tests)            | Voter Portal                   |
| Xray Test Management for Jira         | Test management add-on for Jira.  Used for test design, test orders and test execution | Manual end-to-end tests                                      | All components                  |
| HP Performance Center SaaS            | Set of software tools for application development and testing from Micro Focus | Check load and performance                                   | All components                  |
| [saucelabs.com](http://saucelabs.com) | A web-based cloud application for test runs on various simulated web and mobile platforms | Cross-browser testing                                        | Voter Portal                   |
| Jenkins                               | Web-based software system based on Java for the continuous integration of individual software components into an Oracle application program | The software is needed for the execution of the automated tests | End-to-end tests All components |

## Defect management

If defects are found during a test, the defect and all relevant information is documented as a bug in Jira.

Relevant information when recording a defect:

- Time stamp (date and time) at which the defect occurred

- Which steps can reproduce the defect?

- Can the defect be reliably reproduced?

- Exact description of the defect

- On which environment was the defect discovered?

- Screenshots if necessary

- Log files if necessary

The defect is then passed on to the team responsible for the suspected defect. Depending on the severity of the defect, it is either fixed immediately or scheduled for the next sprint.

When the defect is fixed, the Jira issue is reassigned to Testing. Testing performs a retest and closes the defect if the retest is successful. If the retest is not successful, the defect is sent back to the Development team with the data relating to the retest (the same as when the defect was recorded). This cycle continues until the retest is successful.

### Defect weighting and prioritisation of defects

Severity is an error weighting that influences the priority of the defect. It is a characteristic of how often this defect occurs and, above all, what effects it can cause (severity of the defect). Severity has a decisive influence on the test end and acceptance criteria. In the test tools that are used, the defect weightings have different names: Jira = Priority; ALM QC = Severity.

#### Defect weighting of the defects

The error weighting below is applied:

##### 1 - Blocker

Defect which prevents further processing, e.g. termination of a function that runs daily or is necessary, or a defect which produces incorrect results and cannot be circumvented or leads to data loss, data inconsistency or data destruction. Verifiability cannot be guaranteed or the protocol implementation is not correct.

###### Blocker Examples

The system crashes when a function is called. The process cannot be continued. The object cannot be used in this way. Further process steps on the object are not possible. The defect must be remedied directly.

Possible examples (not exhaustive):

- Generated codes do not correspond to the ballot paper
- User cannot log in
- Website (Voter Portal) is not accessible
- Vote/election does not appear
- Urn encryption does not work
- Browser is not supported~~.~~
- Electoral authority cannot authorise (smart card lost/defective)
- Batch ends unscheduled
- System hangs and does not respond to any input
- Data is calculated incorrectly, no workaround possible

##### 2 - Critical

Not a blocker, as further processing is possible. However, one or more functions of an essential business process either do not work at all or do not work properly. There is no workaround for this.

###### Critical Examples

- In elections: alliances, cumulations and write-ins do not work
- Checkboxes cannot be selected


##### 3 - Major

Impairment of essential functions. As point 2, but the defect can be circumvented with a reasonable instruction to the user. The workaround must be appropriate, reasonable and proportionate in terms of additional effort and duration.

###### Major Examples

- Language change does not work
- Help not available
- Performance problems (encryption 20+ seconds)
- The contractually agreed system performance is not met
- Data is calculated correctly using the bypass solution

##### 4 - Minor

Defect that impedes use but allows further processing without affecting essential functions.

###### Minor Examples

- Result data is not displayed
- File was not created automatically
- The screen is missing non-mandatory data

##### 5 - Trivial

Defect without impairment of essential functions, e.g. insufficient user comfort

These defects do not affect the functionality. These are usually defects in the layout or handling. The system can be used with restrictions

- The labels displayed are incorrectly positioned or written
- Incorrect cursor positioning

In the Jira tool, five levels are maintained by default. The classification "Trivial" should not (or no longer) be used as an error weighting.

#### Prioritisation of defects

If circumstances require, defects can be prioritised within the individual categories of defect weighting. This serves to determine the order in which the defects are to be remedied. The priority of the defects includes both the urgency of the defect removal and the severity of the defect effect.

##### Priority - High = functionality not testable

Deficiency that results in central requirements not being met and the function being largely unusable. The process cannot be continued. The object cannot be used and tested further. Further process steps on the object are not possible.

Priority - High Examples (not exhaustive)

- The system crashes when a function is called
- User cannot log in
- Website (Voter Portal) is not accessible
- Vote/election does not appear
- Urn encryption does not work
- Browser is not supported
- Electoral authority cannot be authorised (smart card  lost/defective)
- Generated codes do not correspond to the ballot paper
- In elections: alliances, cumulations and~~,~~  write-ins do not work
- Checkboxes cannot be selected
- Batch ends unscheduled
- System hangs and does not respond to any input
- Calculation of amount and time limits does not work
- Data is calculated incorrectly;~~,~~ no workaround possible

##### Priority - Medium = wrong result

This defect provides incorrect result values or prevents further execution of the individual tests. The test is terminated immediately and can only be continued after the error has been corrected. However, the defect has no effect on other tests.

Priority - Medium Examples (not exhaustive)

- Language change does not work
- Help not available
- Performance problems (encryption 20+ seconds)
- Document was not created
- Data that is not absolutely necessary is missing from the printout
- Workflow refers to an incorrect subsequent function
- Defect in the screen layout
- Incorrect formatting on cover letters
- Handling input errors in a field

##### Low = cosmetic defect

The defect concerns a test condition with low priority and, although this defect has occurred, the test can be continued and the entire scenario can be tested in order to detect further defects. The system can be used without major restrictions so that the defect can be rectified at a later time / in a later release.

Priority - Low Examples (not exhaustive)

- Defect in the screen layout
- Incorrect formatting on cover letters
- Handling input errors in a field

## Test reporting

All planned and executed tests, the acceptance steps and their results are documented. The test documentation consists of the following elements:

- Test plans / test protocols
- Documentation per test case
- The execution date, the test results and the tested software release version are documented in Xray for each test case
- Test coverage
- The test coverage of the test cases shows how far the various requirements are covered with tests
- Status of test automation
- The test automation status shows how many test cases have been automated

Test reporting is performed as follows for the different test levels:

- Component test
  The developer logs the results in a list in the release notes

- Integration test
  The results of the integration test are documented in the test report, which is created by the test manager

- System tests
  The results of the system test are documented in the test report, which is created by the test manager

- Acceptance tests
  The results of the acceptance test are documented in the test report, which is created by the test manager. This test report is made available to the client

### Process overview of the test procedure and the preparation of the test report

The following figure schematically shows the process flow for the test procedure and the creation of the software release test report:

![Graphic of the process overview of the test procedure](../.gitlab/media/test_concept/image6.png)

## Test organisation

This chapter describes the test organisation. It lists the persons and responsibilities, how the test efforts are estimated and how the test plans are created. It also describes the necessary training and the approvals and releases for the tests.

### Persons and responsibilities

The test organisation and main responsibilities are described here.

#### Product Management

- Definition of the conditions under which the different test types are required
- Categorisation of the defects that have occurred
- Ensuring communication for development

##### Business Analyst

- Ensuring the completeness of technical test cases

##### Test Manager

Coordinates all activities around the tests (e.g. preparation and provision of the test platform)

- Develops the test concept
- Coordinates and develops the test cases
- Ensures the availability of test data and test execution
- Valuates the benefits of test automation and ensures that it is implemented
- Management and recording of defects and deficiencies that have occurred
- Writes the test report and reports to the project manager

##### Tester

- Carries out the tests with the appropriate test tools and logs their status
- Collects all error messages
- Test data generation  

##### Test Automation Specialist

- Automates specific test cases  

##### Specialist for stress and performance tests

- Coordinates all activities around the tests (e.g. preparation and provision of the test platform)
- Coordinates and develops the cases for the stress and performance tests
- Performs the load and performance tests with the appropriate test tools and logs their status
- Collects all error messages
- Evaluates the results of the load and performance tests carried out
- Submits the collected and evaluated results of the stress and performance tests to the test manager

##### Development

- Implements change requests and eliminates defects based on the prioritisation of product management
- Performs the component tests  

##### Infrastructure Competence Center (CC)

Responsible for the deployment of releases on the corresponding environment

- Runs a checklist with smoke tests after installation has been completed |

#### Outlay

For the standard tests that are carried out with each release, standardised outlay and processing times are planned. However, these apply only if the release and the test environment are ready for testing on time according to the plan and if no defects that block testing occur during the tests.

If a test is required that is not covered by the standard tests, an outlay estimate must be prepared. For this purpose, the client must provide the Test Manager with the specifications of the object to be tested and the objective of the test. The Test Manager is responsible for either estimating the outlay him- or herself or obtaining the outlay estimates from the appropriate persons such as testers and compiling an outlay estimate.

#### Test plan

The test planning is created by the Test Manager in consultation with Product Management and is produced both in clear table form and in Outlook.

#### Training

One quality aspect is training. The Test Managers and Testers are trained on the internal requirements. This includes training on Post Agile Methodology (PAM) and training on, among other things, the Code of Conduct, safety and testing standards.

#### Approval / release

Development is responsible for approving the release for deployment. Infrastructure is responsible for deploying a release and releasing it for testing. The Test Manager approves a release and an environment based on the acceptance criteria after the planned tests have been successfully carried out. The current test status is presented in the weekly interlude meeting. The final test report after the testing of a release has been completed is prepared by the Test Manager and approved by Product Management. The test report can be viewed by all those involved in the e-voting project. If test cases are adapted or newly created, they are reviewed and approved by the Test Manager.

#### Links and documents

1. [ISTQB Glossary](https://glossary.istqb.org/en/search)
2. [Ordinance of the FC on Electronic Voting (VEleS)](https://www.fedlex.admin.ch/eli/cc/2022/336/en)
3. [Technical and administrative requirements for electronic voting (TAF)](https://www.fedlex.admin.ch/eli/oc/2018/321/de)
4. [Common Criteria Protection Profile for Basic Set of Security Requirements for Online Elective Products, Version 1.0](https://www.bsi.bund.de/SharedDocs/Zertifikate_CC/PP/Archiv/PP_0037.html)
5. [Open Web Application Security Project OWASP Top 10, 2017](https://owasp.org/www-project-top-ten/)
6. [ISO/IEC 27002:2017 Standard](https://www.iso.org/standard/54533.html)
7. The various parties and software components are explained in the [system specification and system architecture](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/System) documents.

