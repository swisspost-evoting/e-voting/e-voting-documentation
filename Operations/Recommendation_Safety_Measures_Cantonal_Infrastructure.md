---
layout: article
title: Recommendation Safety Measures (Cantonal infrastructure)
author: © 2024 Swiss Post Ltd.
date: "2024-10-22"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Recommendation safety measures (cantonal infrastructure)

## Introduction

The cantons operate some components of the Swiss Post Voting System (see [system specification](../System/System_Specification.pdf)). Swiss Post recommends the following generic security measures for the secure operation of the cantonal Computers involved in the configuration and tally phase of the protocol. These recommendations complement the canton's existing security concepts.

## Protection goals

- Correct execution of the vote/election (conformity with the law)
- Secrecy of the physical identities of the voters (confidentiality)
- Secrecy of the individual votes (confidentiality)
- Correctness of the results (integrity)
- Proof that no manipulation or other interference has taken place (verifiability)

## Threats (generic catalogue of threats from BSI Germany)

- Spying on information / espionage
- Disclosure of sensitive information
- Manipulation of hardware or software
- Unauthorised intrusion into IT systems
- Software vulnerabilities or errors
- Unauthorised use or administration of devices and systems
- Malware

## Procedure and scope

The [system specification](../System/System_Specification.pdf) describes the configuration, voting, and tally phase of an election event. The cantonal infrastructure comprises the following:

- 1 online Computer
- multiple offline Computers
- external storage media

## Catalogue of generic security measures

We recommend the following generic security measures for the secure operation of the SDM Computers. These recommendations complement the canton's existing security concepts.
It must be checked in each case whether the measures are compatible with the existing configurations of the clients.
For the conception and, if necessary, implementation, the involvement of persons with the appropriate knowledge is recommended.

## Generic security measures – Online Computer

The Online Computer establishes communication relations with the remote servers at SwissPost. For this reason, it requires a network connection with Internet access. In addition, files are copied to the device via USB keys. The threats and the number of possible risk scenarios are therefore considerably higher than for the isolated offline Computers.

### Technical measures

- Block booting from external devices
- Activate BitLocker (with TPM) and encrypt hard disk (additional PIN authentication if necessary)- Activate Secure Boot
- Block access to BIOS/UEFI for the user (password)
- Hardening OS
  - Basis CIS Benchmark (<www.cisecurity.org>)
  - Disable unneeded interfaces (if not already disabled in BIOS)
  - Disable unneeded devices (e.g. CD drive, smart card, camera, microphone)
  - Have a defined process to to ensure OS Update (for instance when installing the image)
  - Disable/block mobile hotspot
  - Ensure that NTFS is used on all volumes
  - Enable Windows Firewall (no inbound traffic for all profiles, outbound whitelist approach)
  - Enable UAC (User Account Control)
  - Enable DEP (Data Execution Prevention)
  - Prevent ICMP tunnels
  - Prevent Teredo tunneling
  - Disable IPv6
  - Prevent adding printers (incl. IPP)
  - Activate Enhanced Protected Mode (EPM) for browsers
  - Enable auditing (define event logs large enough, min. 200MB)
  - Configure Account Logon audit policy
  - Configure Account Management audit policy
  - Configure Logon/Logoff audit policy
  - Configure Policy Change audit policy
  - Configure Privilege Use audit policy
  - Prevent the creation or use of Microsoft Accounts
  - Prevent sending of unencrypted passwords to SMB shares
  - Block remote registry access
  - Prevent RDP access
  - Prevent automatic administrative logon to Recovery Console
- Configure Windows Defender (Windows Defender Advanced Threat Protection if applicable)
- Defender: Turn off cloud-based protection and automatic submission of samples
- Manually set the correct system time
- Implement the appropriate authorization concept (least privilege principle)
  - Restrict access to the Computer to authenticated users and administrators
  - Deactivate guest accounts
  - Users with standard rights (users)
  - Complex passwords
- Check DNS settings (Auto or fixed DNS?) and proxy settings (Auto-detect?)

## Generic security measures – Offline Computer

### Technical measures – Offline Computer

- Block booting from external devices
- Activate BitLocker (with TPM) and encrypt the hard disk (additional PIN authentication if necessary)
- Activate Secure Boot
- Block access to BIOS/UEFI for the user (password)
- Hardening OS
  - Basis CIS Benchmark (<www.cisecurity.org>)
  - Deactivate all interfaces (except USB)
  - Disable unnecessary devices (e.g. CD drive, smart card, camera, microphone)
  - Disable network devices
  - Disable/block mobile hotspot
  - Disable OS Update
  - Ensure NTFS is used on all volumes
  - Enable UAC (User Account Control)
  - Enable DEP (Data Execution Prevention)
  - Prevent ICMP tunnels
  - Prevent Teredo tunneling
  - Prevent adding printers (incl. IPP)
  - Enable Enhanced Protected Mode (EPM) for browsers
  - Enable auditing (define event logs large enough, min. 200MB)
    - Configure Account Logon audit policy
    - Configure Account Management audit policy
    - Configure Logon/Logoff audit policy
    - Configure Policy Change audit policy
    - Configure Privilege Use audit policy
  - Prevent the creation or use of Microsoft Account
  - Prevent sending of unencrypted passwords to SMB shares
  - Block remote registry access
  - Prevent RDP access
  - Prevent automatic administrative logon to Recovery Console
- Configure Windows Defender (Windows Defender Advanced Threat Protection if applicable).
- Defender: turn off cloud-based protection and automatic submission of samples
- Implement the appropriate authorisation concept (least privilege principle)
  - Restrict access to the Computer to authenticated users and administrators
  - Deactivate guest accounts
  - Users with standard rights (users)
  - Complex passwords

## Generic security measures – external storage media

### Technical measures – external storage media

- Use a USB stick with hardware encryption
- Use a USB stick that is protected against the malware BadUSB (e.g. SafeToGo, Ironkey, datAshur)

## Organizational measures

- Limit the physical access of laptops and storage media to authorized employees only
- Set up the device only in a secure environment behind IPS and/or proxy
- Carry out a security check of the fully configured device (once)
- Ensure that the operating system is up to date when installing a new image.
- Install the machines via an image that ensures that only the necessary applications are installed.
- Mark devices physically and unambiguously (e.g. seal adhesive)
- Use devices (Computer, USB sticks) only for the intended SDM applications
- Do not leave devices (Computer, USB sticks) unattended (if you do, secure them with sealing glue)
- Define the process for handling devices before, during and after a vote
- Define the process for security-relevant events
- Integrate the devices into a lifecycle management process
