---
layout: article
title: Operation whitepaper of the Swiss Post e-voting system
author: © 2024 Swiss Post Ltd.
date: "2024-10-22"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Operation whitepaper of the Swiss Post Voting System

## Introduction

The operation whitepaper describes the E-Voting operational procedures with all implemented security aspects. Aspects include information on the operational organisation, support regulation, change & maintenance and backup & restore procedures.

The Swiss Post Voting System enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various software components and the system architecture are explained in the [system specification and system architecture documents](../System).

## Operational organisation

### Data centers and business continuity

Further information can be found in the Infrastructure whitepaper of the Swiss Post voting system (”Infrastructure Whitepaper”).

### Safety precautions

The E-Voting infrastructure is set up for use within the cantons. Each canton has its own E-Voting environment, which is logically completely separated from the environments of the other cantons. To guarantee the segregation of duties and the four-eyes principle there are a large number of teams involved. The four-eyes principle is put into practice with by having at least two people from different teams. For the four-eyes principle, each token request will be recorded and compared to the access protocols.

In general, the operation mode of each of the various E-Voting components depends on different aspects.
Among others, the following aspects are considered as being of particular importance:

- trustworthiness (is a component especially safety-critical and trusted or can it be considered as a standard case?)

- degree of specialisation (is a component for a specific project case or a general-purpose?)

In the Swiss Post E-Voting solution, safety-critical and trusted components are always in different dedicated E-Voting environments and network areas. This includes:

- each Control Component

- SDM (Secure Data Manager) implementing the Setup Component and Tally Control Component functionality. The cantons operate the SDM in their own infrastructure, see [Recommendation Safetey Measures](../Operations/Recommendation_Safety_Measures_Cantonal_Infrastructure.md)

Moreover, the Swiss Post has different dedicated E-Voting environments and network areas for the following other components:

- E-Voting proxy

- E-Voting platform

- E-Voting message broker

- E-Voting databases

Other components are in the standard Swiss Post environment, for example:

- Data centers

- Network infrastructure

- Different monitoring infrastructures

The standard Swiss Post environment and network are ISO certified and ensure:

- robust operation

- a high standard of security

- high-quality monitoring

- the segregation of duties

- the latest technical level (since it is used by many other projects and each of them benefits from any operation and security findings of the other project)

### Safety precaution on Control Components

#### General - Safety precaution on Control Components

All critical operations on the Control Components (”CC's, CC1, CC2, CC3, CC4”) are based on the four-eyes principle.

- For example, the first person in the first group carries out operations and the second person in a second group supervises the first person.

All critical knowledge such as administrator passwords etc. are kept apart and secret.

- For example, the first person in a first group knows the administrator password but has no system access rights and the second person in a second group does not know the administrator password but has system access rights.

Each access to the control components is routed through a dedicated jump host, which serves as an intermediary that enhances security by controlling and monitoring all connections to the control components.

- The jump host securely logs all user actions and commands executed on the control components. The user does not have root access on the jump host, and each control component is assigned its own jump host for added security.

![Teams](../.gitlab/media/operation_whitepaper/image2.png)

#### Teams

At least one member of a team is hereinafter referred to as the Team. All team members working on Control Components are well known.

Token Team

- The Token Team is the only team that creates and hands out security tokens.

- It has no system access to any Control Component.

- Before any token handout, the Token Team checks who-is-who based on a change, an incident or an emergency change.

- All requests directed to the E-Voting components are recorded in protocols.

- Any access to a Control Component triggers an alarm which will be forwarded to the corresponding CC Team.

Vault Teams

- The Vault Teams are the only teams that create and know administrator passwords and embedded server management interface passwords.

- They have no system access to any Control Component.

Storage Team

- The Storage Team is the only team that receives monitoring data from the Control Components.

- It has no system access to any Control Component.

Infra Team

- The Infra Team is able to execute the physical installation (hardware mounting) of any Control Component.

- They do not know the embedded server management interface password, which is owned by the Vault Team.

- All the work from Infra Team has to be done in an observed process.

OS Team

- The OS Team is able to install the operating system on any Control Component.

- It cannot do anything without the administrator password of a Vault Team.

- All the work from OS Team has to be done in an observed process.

4 CC Teams

- Each of the four CC Teams is able to install the software components on their dedicated Control Component.

- But they cannot do anything without a token from the Token Team.

- Each of the four CC Teams receives alerting data from their dedicated Control Component.

#### System modifications

Physical installation on the Control Components:

- The physical installation (hardware mounting) needs a change, an incident or an emergency change.

- It will be executed by the Infra Team on all Control Components.

- On each Control Component, the corresponding CC group supervises the infra team in accordance with the four-eyes principle.

- Both parties sign a protocol with the executed activities.

Installation of the operating system on the Control Components:

- The installation of the operating system needs a change, an incident or an emergency change.

- It will be executed by the OS team on all Control Components but it needs the administrator passwords entered by the Vault Team.

- On each Control Component, the corresponding CC Group supervises the Infra Team in accordance with the four-eyes principle.

- Both parties sign a protocol with the executed activities.

Software installation on the Control Components:

- The installation of software components needs a change, an incident or an emergency change.

- It will be executed by each CC Team on their dedicated Control Component.

- None of the CC Teams has network access to their dedicated Control Component due to firewall rules.

- Each CC Team needs a token from the Token Team to install software components.

- Without a token, no CC Team has any write access.

Monitoring collection:

- Every action on each Control Component will be monitored and recorded and forwarded to the Storage Team.

Alarm forwarding:

- Any alarm on a Control Component will be sent to the corresponding CC Team.

## Change & maintenance procedure

### General

The goal of a change & maintenance standard is the official, uniform, standardized, secure, economic and on-time execution of service and infrastructure changes and updates.

The Swiss Post standard change & maintenance procedure (”Standard C&M Procedure”):

- sets down executions rules according to uniform principles and within a defined scope

- enables a maximum degree of topicality, correctness, completeness and quality to reduce any failures to a strict minimum

- is dedicated to all divisions and employees of Swiss Post IT

- is compulsorily required for any kind of service and infrastructure modifications

![Swiss Post standard Change & Maintenance Procedure](../.gitlab/media/operation_whitepaper/image3.png)

The complex E-Voting system has further particularities defined by the Swiss Post E-Voting Change & Maintenance Procedure (”E-Voting C&M Procedure”), which:

- is an extension to the general Standard C&M Procedure

- includes requirements and recommendations

- comprises restrictions, differences and enhancements

- is geared towards integration and production environments

Both kinds of Change & Maintenance Procedures can be initiated:

- by Swiss Post IT or

- by third parties such as  a canton

Any E-Voting C&M Procedure initiated by a canton is the responsibility of the Swiss Post E-Voting Competence Center (”E-Voting Competence Center”). The Standard C&M Procedure and E-Voting C&M Procedure comprises complete documentation with concepts, procedure runs, detailed technical guidelines, work instructions, protocols and so on.

### Change & maintenance window

There are two types of change & maintenance windows:

- On one hand, the Standard C&M Procedure has regular, Group-wide Swiss Post change & maintenance windows (”Standard C&M Window”).

- On the other, Switzerland has four predefined official blank voting dates a year, which are the reference points for any change & maintenance windows (further dates can be defined by the cantons).

Since both types of windows do not correspond to each other, the E-Voting System (”E-Voting System”) has its own Swiss Post E-Voting change & maintenance windows (”E-Voting C&M Window”). The E-Voting C&M Window fits in two consecutive voting dates, that are after the end of a voting event (”Voting Event”) and before the beginning of the next Voting Event:

- The beginning of a Voting Event is approximately 2 weeks before the official voting phase begins.

- The end of a vote is normally on Monday after the official voting phase ends.

The cantons

- define all final appointments and points in time during a Voting Event

- can extend the voting phase, e.g. for subsequent or re-elections

Although the change & maintenance windows are different, a change on the E-Voting system always needs an official Swiss Post change to guarantee a clearly defined procedure.

To avoid failures and interruptions, two kinds of E-Voting Change & Maintenance phases are defined:

- Red phase (”Red Phase”): during any productive Voting Event

- Green phase (”Green Phase”): outside any productive Voting Event

During the Red Phase:

- Any actions that can influence the current Voting Event are prohibited or highly limited for all services, infrastructures and teams.

- For any actions, the E-Voting Competence Center has to be involved and decide about the procedure.

- Automated operations such as automated patching need to be deactivated temporarily as well.

## Backup & restore procedure

### E-Voting backup & restore

The databases are triple-mirrored.
Each transaction has to be written to two databases in order to accept the transaction.
A transaction log will always be written on each database.

The Swiss Post backup & restore procedure will be verified each year.

After the results of a vote have been accepted the canton requests the deletion of all backups.

## Support regulation

### General - support regulation

General E-Voting information can be found on the following sites:

| **Content**                                                     | **URL**                                                 |
| --------------------------------------------------------------- | ------------------------------------------------------- |
| Information for cantons, municipalities and experts            | <https://www.post.ch/evoting>   |
| Blog for experts and interested parties                         | <https://digital-solutions.post.ch/en/e-government/blog> |
| Community with news, insights etc.                              | <https://evoting-community.post.ch>                     |
| Security advice, help for browser, certificate, hashcode etc. | <https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Security-advices>                               |

### Voter

During an open Voting Channel, the canton is the support contact point for voters. Usually, the Voting Card or the canton website will provide support contact information.

### Canton

The canton can always contact Swiss Post for all types of support requests.
