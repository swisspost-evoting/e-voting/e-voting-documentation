# Changelog

| Tracked documents                                                                  |
|:-----------------------------------------------------------------------------------|
| [Architecture of the Swiss Post Voting System](#changelog-architecture-doc)        |
| [Verifier Specification of the Swiss Post Voting System](#changelog-verifier-spec) |
| [System Specification of the Swiss Post Voting System](#changelog-system-spec)     |

---

<a id="changelog-architecture-doc"></a>
## Changelog for the Architecture of the Swiss Post Voting System

### Changes in version 1.4.0

Version 1.4.0 of the architecture document includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

- Thomas Edmund Haines (Australian National University), Olivier Pereira (Université catholique Louvain), Vanessa Teague (Thinking Cybersecurity)
- Aleksander Essex (Western University Canada)
- Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.4.0 of the Architecture of the Swiss Post Voting System contains the following changes and improvements:

- Removed the crypto-primitives-domain component, which is replaced by the e-voting-libraries.
- Removed the command-messaging component, which is integrated now in the control-component.
- Updated the description of the message broker usage which is migrated from RabbitMQ to Artemis.
- Updated the description of the native code usage which is migrated from GMP to VMGJ.
- Updated the description of the DEV database which is migrated from H2 to PostgreSQL.
- Added explanation concerning the recursiveHash implementation and the Hashable interface.
- Minor corrections and updates.

---

### Changes in version 1.3.0

Version 1.3.0 of the architecture document incorporates some of the feedback from the Federal Chancellery's mandated experts (see above)

Version 1.3.0 of the Architecture of the Swiss Post Voting System contains the following changes and improvements:

- Removed the cryptolib and cryptolib-js components, which were replaced by the crypto-primitives and crypto-primitives-ts.
- Detailed the new voting-server architecture based on SpringBoot instead of JavaEE.
- Removed SpringBoot in the Secure Data Manager.
- Minor corrections and updates.

---

### Changes in version 1.2.0

Version 1.2.0 of the architecture document incorporates some of the feedback from the Federal Chancellery's mandated experts (see above)

Version 1.2.0 of the Architecture of the Swiss Post Voting System contains the following changes and improvements:

- Better justified the use of frameworks and design patterns in e-voting and the verifier (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Explained the code structure of the crypto-primitives library (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Elaborated on the defense-in-depth mechanism for sub-algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).

---

### Changes in version 1.1.0

Version 1.1.0 of the Architecture of the Swiss Post Voting System contains the following changes and improvements:

- Added the architecture description of the Swiss Post Verifier.

---

<a name="changelog-verifier-spec"></a>
## Changelog for the Verifier Specification of the Swiss Post Voting System

### Changes in version 1.5.2

Version 1.5.2 of the verifier specification includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

- Thomas Edmund Haines (Australian National University), Olivier Pereira (Université catholique Louvain), Vanessa Teague (Thinking Cybersecurity)
- Aleksander Essex (Western University Canada)
- Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.5.2 of the verifier specification contains the following changes and improvements:

- Include the entire Election Event Context in Verification 5.04 VerifySchnorrProofs, thereby ensuring that the Control Components and the Verifier agree on the Election Event Context (feedback from Thomas Haines).
- Clarified the origin of the context variables in the auditors' manual checks (feedback from Thomas Haines).
- For the completeness checks, defined notation for placeholders used in tables 2, 3 and 6 (feedback from Thomas Haines).
- Minor corrections and clarifications.

---

### Changes in version 1.5.1

Version 1.5.1 of the verifier specification includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Version 1.5.1 of the verifier specification contains the following changes and improvements:

- Separate the setup tally dataset into context, setup, and tally (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Clarified in the manual checks by the auditors that the context dataset must remain constant between VerifyConfigPhase and VerifyTally (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Clarified the VerifyConfirmedEncryptedVotesConsistency check (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Changed the folder names to camel case for better consistency with the e-voting system.

---

### Changes in version 1.5.0

Version 1.5.0 of the verifier specification includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Version 1.5.0 of the verifier specification contains the following changes and improvements:

- Using the new concept of Context/Input variables - as described in the system specification - for all algorithms of the verifier specification.
- Improved the structure and pseudo-code formalism of the VerifyTally verifications and algorithms.
- Encryption Parameters removed from the table on the required elements for the setup verification since they are incorporated into the election event context.
- Removed SetupComponentEncryptionParameters from the overview of the authenticity checks for the setup verification, corresponding verification VerifySignatureSetupComponentEncryptionParameters removed.
- Updated the table with the overview of the different voting option types necessary to validate the pTable.
- VerifyPrimesMappingTableConsistency now also checks the correctness information.
- Added description of the verification of the seed in VerifyEncryptionParameters.
- Renamed the verification VerifyKeyGenerationSchnorrProofs to VerifySchnorrProofs and use the centralized Schnorr proof verification algorithms from the system specification.
- VerifyProcessPlaintexts now uses the algorithm ProcessPlaintexts from the system specification.
- Clarified the declaring of invalid votes in the tally files generation.
- Fixed minor error and inconsistencies and made the necessary alignments to the system specification.

---

### Changes in version 1.4.1

Version 1.4.1 of the verifier specification includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Version 1.4.1 of the verifier specification contains the following change:

- Merged verifications VerifySignatureSetupComponentVerificationData, VerifySignatureControlComponentCodeShares, VerifyEncryptedPCCExponentiationProofs, VerifyEncryptedCKExponentiationProofs into one verification VerifySignatureVerificationDataAndCodeProofs.

---

### Changes in version 1.4.0

Version 1.4.0 of the verifier specification includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Version 1.4.0 of the verifier specification contains the following changes and improvements:

- Added pseudocode algorithms for the VerifySetupCompleteness and VerifyTallyCompleteness verifications (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the semantic information of the voting options to the primes mapping table (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Extended the VerifyPrimesMappingTableConsistency verification to check that the setup component's primes mapping table corresponds to the canton's configuration XML.
- Modified the VerifyProcessPlaintexts verification to use the GetEncodedVotingOptions and GetActualVotingOptions algorithm.
- Added the verification of the eCH-0222 raw ballot data XML file to the VerifyTallyFiles algorithm.

---

### Changes in version 1.3.1

Version 1.3.1 of the verifier specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following improvement:

- Elaborated on the auditors' manual checks (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).

---

### Changes in version 1.3.0

Version 1.3.0 of the verifier specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Clarified that all verifications of a certain phase must be executed (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Specified the auditors' manual checks (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Added pseudo-code algorithms for the authenticity verifications (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added pseudo-code algorithms for the consistency verifications (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Modified the verification of the signature of the election event configuration by using the canton's certificate.
- Separated the setup component public keys and the election event context object.
- Added some minor clarifications and corrections.

---

### Changes in version 1.2.0

Version 1.2.0 of the verifier specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Added the verification of write-in decodings in the VerifyProcessPlaintexts algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the verification of the eCH-0222 file to the algorithm VerifyTallyFiles and added the corresponding authenticity check.
- Added the missing truncation of the Electoral Board Public Key in the algorithm VerifyTallyControlComponentBallotBox.

---

### Changes in version 1.1.0

Version 1.1.0 of the verifier specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Ensured that all protocol participants have a consistent view of the primes mapping table (pTable) by adding the actual and encoded voting options to the voting client proofs (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Incorporated the algorithm VerifyTallyFiles to the verification VerifyTallyControlComponent (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the verification of the Schnorr proofs in the verification VerifyKeyGenerationSchnorrProofs (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Added a verification to the algorithm VerifyEncryptionParameters that we use the EXTENDED security level (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Constrained the domain of the actual voting options.
- Added a comment clarifying that the domain of input and context elements must be verified (feedback from Aleksander Essex).

---

### Changes in version 1.0.1

Version 1.0.1 of the verifier specification contains the following changes and improvements:

- Detailed the tally verification algorithms.
- Detailed the different consistency checks.
- Aligned the Completeness sections to the Channel Security section.
- Small fixes and improvements.

---

<a name="changelog-system-spec"></a>
## Changelog for the System Specification of the Swiss Post Voting System

### Changes in version 1.4.1.1

Version 1.4.1.1 of the system specification includes the following change:

- Increased the maximum number of write-ins from 15 to 30.

---

### Changes in version 1.4.1

Version 1.4.1 of the system specification includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

* Thomas Edmund Haines (Australian National University), Olivier Pereira (Université catholique Louvain), Vanessa Teague (Thinking Cybersecurity)
* Aleksander Essex (Western University Canada)
* Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.4.1 of the system specification includes the following changes and improvements:

- Define new algorithm GetHashElectionEventContext which hashes the global Election Event Context (feedback from Thomas Haines).
- Use the new algorithm in the Configuration Phase by algorithms SetupTallyCCM and SetupTallyEB to integrate the Election Event Context in the challenges of the zero-knowledge proofs, thereby ensuring consensus of the Election Event Context between the Control Components and the Setup Component (feedback from Thomas Haines).
- Necessary corresponding changes in algorithms VerifyKeyGenerationSchnorrProofs and VerifyCCSchnorrProofs for verification of the proofs.
- Improved auditability of algorithm VerifyCCSchnorrProofs by streamlining the structure of auxilary information included in the proofs (feedback from Rolf Haenni, Reto Koenig, Philipp Locher).
- Clarifications on what the election event configuration defines (feedback from Rolf Haenni, Reto Koenig, Philipp Locher).
- Clarification in algorithm 6.10 RequestProofNonParticipation that the list of confirmed voting cards contains the voting cards from all ballot boxes of the election event (feedback from Thomas Haines).
- Modification of the explanation in section 3.3 Keys and Codes of the Voting Card (feedback from Rolf Haenni, Reto Koenig, Philipp Locher).
- Fixed a mistake in the GenVerDat algorithm, GetEncodedVotingOptions instead of GetActualVotingOptions.
- Minor corrections and clarifications (feedback from Rolf Haenni, Reto Koenig, Philipp Locher).

---

### Changes in version 1.4.0

Version 1.4.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- New concept for Context/Input variables - added detailed description of the concept and applied it to all algorithms of the system specification.
- Defined key-value maps in Conventions.
- Reduced the length of the election keys (ELpk, ELsk) and the CCR choice return codes encryption keys (pkCCR, skCCR) to the maximum length needed for the verification card set instead of the maximum supported values of the system.
- Specified the format of the seed used for the encryption parameters.
- Added a check in GetElectionEventEncryptionParameters that the product of the primes used as encoded voting options cannot be larger than the group modulus.
- Renamed the old section Usability to Entropy of Keys and Codes and updated the description.
- Added new section Electoral Model including a more complete description of the election event parameters and encoding of the voting options.
- Extended the pTable with the correctness information. Specified new algorithms GetCorrectnessInformation and GetBlankCorrectnessInformation.
- Refined the subsection about Valid Combinations of Voting Options. The algorithms GetCorrectnessIdForSelectionIndex and GetCorrectnessIdForVotingOptionIndex were removed.
- Specified new algorithms GetWriteInEncodedVotingOptions, GetPsi and GetDelta.
- Specified new algorithm GetHashContext and use it in several algorithms.
- Improved the algorithm Factorize.
- Added domain separation in DeriveBaseAuthenticationChallenge, GetAuthenticationChallenge and VerifyAuthenticationChallenge.
- Removed the description of the write-in alphabet.
- Use the extended Latin alphabet from the crypto primitives specification for the write-ins.
- Improved the description of creating a vote with write-ins and specified the algorithm EncodeWriteIns.
- Improved algorithm DecodeWriteIns by truncating the write-in values to prevent the creation of invalid eCH-files.
- New algorithm VerifyKeyGenerationSchnorrProofs which was moved from the verifier specification.
- Specified new algorithm VerifyCCSchnorrProofs and use it in VerifyKeyGenerationSchnorrProofs, GenVerCardSetKeys, SetupTallyEB.
- Changed the SetupVoting protocol so that the algorithm GenKeysCCR is executed at a later point in the process.
- Improved the sequence diagrams SetupVoting and SetupTally to adhere more closely to the actual process of the system.
- Defined new algorithm GenSetupData which replaces the algorithm GenSetupEncryptionKeys and additionally executes the algorithm GetElectionEventEncryptionParameters and generates all the pTables of the election event.
- Use the new algorithm GenRandomString from the crypto primitives specification to improve several algorithms.
- Removed the algorithm GenRandomSVK.
- Do not use subscript j in any algorithm names.
- Small simplifications to the SetupTallyEB due to the removal of the key truncation operation.
- CreateVote takes as input the actual voting options and write-ins instead of already encoded values.
- CreateVote ensures that the selected actual voting options are all from the correct pTable.
- CreateVote restricts the length of the write-ins and prevents usage of # within the write-in fields.
- CreateVote now includes the contents of E2 in the auxiliary info of the proofs to avoid vote malleability (feedback from Ben Smyth).
- PartialDecryptPCC now includes the contents of E2 in the auxiliary info of the proofs.
- CreateLCCShare and CreateLVCCShare do not anymore create exponentiation proofs for the shares lCC and lVCC.
- All 4 CCs agree on the initial ciphertexts by running GetMixnetInitialCiphertext and computing a hash of the confirmed votes.
- MixDecOnline verifies the hash of all other control components before performing the mixing (and would stop the process otherwise).
- Using consistent notation for the group G_q throughout the system specification (no use of H_l).
- ProcessPlaintexts checks if any vote contains an invalid combination, and if so the algorithm aborts and returns nothing.
- Removed the message SetupComponentEncryptionParameters from the overview of the authenticated messages in the setup phase.
- Added the message ControlComponentVotesHash to the overview of the authenticated messages in the tally phase.
- Minor corrections and improvements.

---

### Changes in version 1.3.2

Version 1.3.2 of the system specification includes the following changes and improvements:

- Increased the voter authentication time step to 300 seconds, added a forward time step, and improved the error message for desynchronized clients.
- Replaced two characters in the alphabet of the start voting key for increased usability by using the new GenRandomSVK algorithm.
- Increased the maximum number of supported voting options from 3,000 to 5,000.

---

### Changes in version 1.3.1

Version 1.3.1 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Modified the voter authentication protocol by introducing a voting client generated nonce to allow authentications within a 30-seconds window.
- Clarified the role of the base authentication challenge as the shared secret of the voter authentication protocol (feedback from Thomas Haines, Olivier Pereira, Vanessa Teague).
- Clarified the reasons behind using an extended authentication factor in the voter authentication protocol (feedback from Thomas Haines, Olivier Pereira, Vanessa Teague, Alexander Essex, Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Clarified the minimum length requirement of the electoral board passwords.
- Minor corrections and fixes (feedback from Thomas Haines, Olivier Pereira, Vanessa Teague, Alexander Essex, Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).

---

### Changes in version 1.3.0

Version 1.3.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Specified the new voter authentication protocol (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, and Aleksander Essex).
- Completed the sequence diagrams of the sub-protocols SendVote and ConfirmVote with the messages from the voter authentication protocol (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, and Aleksander Essex).
- Added the semantic information of voting options to the primes mapping table and included it in the voting client's zero-knowledge proofs (CreateVote, VerifyBallotCCR, VerifyVotingClientProofs) and in the associated data of the verification key store (GenCredDat, GetKey) (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Added the GetSemanticInformation algorithm.
- Renamed the EncodeVotingOptions and DecodeVotingOptions to GetEncodedVotingOptions and GetActualVotingOptions and slightly modified the input parameters and operations.
- Clarified the Argon2id profile used with references to the crypto-primitives-specification (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, and Aleksander Essex).
- Increased the maximum size of supported voting options from 1200 to 3000.
- Added some minor clarifications and formatting improvements.

---

### Changes in version 1.2.0

Version 1.2.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

* Added a section about handling inconsistent views of confirmed votes (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Explained how a voter can receive a proof of non-participation (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the definition of other CCR's indices in the DecryptPCC and VerifyLVCCHash algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Avoided the reuse of variables in the ExtractCRC and ExtractVCC algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the specification of the VerifyMixDecOnline and VerifyVotingClientProofs algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Added a reference about fulfilling the requirement 2.7.3 from the Federal Chancellery's ordinance (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Simplified the QuadraticResidueToWrite algorithm (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Detailed how the cantonal authority can block voting cards and query the voting card status.
* Clarified that the "CANTON" signs the election event's configuration.
* Clarified the return value when the method GenUniqueDecimalStrings returns a single element (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Clarified the nesting of lists in pseudo-code.

---

### Changes in version 1.1.1

Version 1.1.1 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Added a section about the encoding and decoding of write-ins. Integrated the decoding of write-ins into the ProcessPlainTexts algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the eCH-0222 file to the tally files and included the file in the Channel Security section.
- Clarified the definition and usage of context variables (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Improved input validation by adding additional require statements (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the incomplete TallyComponentVotes message in the Channel Security section.

---

### Changes in version 1.1.0

Version 1.1.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Ensured that all protocol participants have a consistent view of the primes mapping table (pTable) by adding the actual and encoded voting options to the voting client proofs (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Incorporated the DecodeVotingOptions method into the ProcessPlaintexts algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added a section about the generation of the tally files (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the input and output XML files to the Channel Security section and specified the signature of XML files (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added the generation and verification of the Schnorr proofs in the algorithms GenKeysCCR, GenVerCardSetKeys, and SetupTallyEB (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Integrated the updated Argon2id functions (GenArgon2id and GetArgon2id) into the GetKey and GenCredDat algorithms.
- Specified the GetElectionEventsParametersPayload algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Added additional context variables to the GenEncLongCodeShares and CombineEncLongCodeShares algorithm.
- Constrained the domain of the actual voting options.
- Added a comment clarifying that the domain of input and context elements must be verified (feedback from Aleksander Essex).

---

### Changes in version 1.0.0

Version 1.0.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Specified the Tally control component's VerifyVotingClientProofs algorithm (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Specified the derivation of the Electoral Board secret key from their passwords.
- Replaced PBKDF with Argon2 (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Renamed DecodePlaintexts to ProcessPlaintexts and GetMixnetInitialPayload to GetMixnetInitialCiphertexts.
- Detailed some consistency checks.
- Minor corrections.

---

### Changes in version 0.9.9

Version 0.9.9 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above).
Version 0.9.9 of the system specification strengthens the cryptographic protocol by shifting responsibilities from the auditors and the voting server to the control components. Namely, each control component verifies the correctness of a confirmation attempt, and each control component verifies the preceding shuffle and decryption proofs. Making the control components perform additional validations prevents attacks against privacy, as highlighted by Cortier, Gaudry, and Debant (see [Gitlab issue #11](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11)) without relying on a trustworthy auditor and prevents inconsistent views across control components.

Version 0.9.9 of the system specification contains the following changes and improvements:

- Replaced the random value generation algorithms with crypto-primitive methods.
- Explained operations on lists and key-value maps (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Introduced a usability section justifying the length of the Start Voting Key and the number of PBKDF iterations (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Extended the channel security section with tables detailing the context for every exchanged message (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Highlighted the MixOnline protocol with a separate sequence diagram.
- Ensured that the GenCMTable algorithm produces distinct Choice Return Codes for each voting option per voter.
- Removed the Shamir Secret Sharing algorithms since we no longer require this method in the strengthened cryptographic protocol.
- Removed the SecureLogs algorithms since we no longer require the SecureLogs in the strengthened cryptographic protocol version.
- Clarified minor points in the sequence diagrams.
- Fixed minor mistakes in various algorithms.

---

### Changes in version 0.9.8

Version 0.9.8 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Removed key compression in favor of dropping excess public keys (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Replaced the KeyDerivationFunction and DeriveKey method with the KDF and KDFtoZq algorithms (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Minor corrections in the DecryptPCC and MixDecOffline algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Fixed minor mistakes and typos in multiple algorithms.

---

### Changes in version 0.9.7

Version 0.9.7 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes and improvements:

- Updated the system specification to the new version of the Ordinance on Electronic Voting (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Specified the generation of SecureLogs. Carsten Schürmann raised this issue in his maturity analysis of the system documentation ([see Gitlab issue #9](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/9)).
- Added a HashAndSquare operation before exponentiating the Ballot Casting Key to facilitate the security reduction in the computational proof (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Specified a GetMixnetInitialPayload algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
- Removed the BasicDataTypes section.
- Clarified the Testing-only security level.
- Fixed the SendVote diagram.
- Fixed an incorrect verification in the DecryptPCC_j algorithm (thanks to Marc Wyss from ETHZ).
- Fixed some minor errors in the CreateLCCShare and CreateLVCCShare algorithm.
- Fixed minor mistakes and typos in the document.
- Changes in version 0.9.6

---

### Changes in version 0.9.6

Version 0.9.6 of the system specification contains the following changes and improvements:

- Strengthened the protocol to verify sent-as-intended properties and vote correctness online in the control components. See [Gitlab issue #7](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/7) for further explanations.
- Specified a recovery scenario for incorrectly inserted or discarded votes (tally phase). See [Gitlab issue #8](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/8) for further explanations.
- Expanded the section on two-round return code schemes with alternative scenarios.
- Explained the election context and its relevance to the protocol's algorithms.
- Moved the GetEncryptionParameters algorithm to the crypto-primitives specification.
- Streamlined the usage and name of lists between the computational proofs and the system specifications.
- Improved clarity of sequence diagrams.
- Fixed smaller typos and errors in various sections.
