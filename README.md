# E-voting documentation

>**General information**
>
>As part of the [**e-voting community programme**](https://evoting-community.post.ch/en), Swiss Post is disclosing the beta-version of its future e-voting system.
>
>Get an overview [**about**](ABOUT.md) Swiss Post's e-voting system disclosure and all the related projects. Before you contribute please read [**how to submit**](REPORTING.md) and acknowledge our [**code of conduct**](CONTRIBUTING.md).  
>
>Note: A continuous public [**bug bounty programme**](https://yeswehack.com/programs/swiss-post-evoting) with attractive rewards, is in place.
  
This project contains all the relevant documents needed to understand how the source code works and performs certain operations.

Swiss Post's e-voting system draws on the [VEleS (Federal Chancellery Ordinance on Electronic Voting)](https://www.bk.admin.ch/bk/en/home/politische-rechte/e-voting/versuchsbedingungen.html) and is based on the following components.
The various components are published in several GitLab projects, [please read the about](ABOUT.md) to get an overview.

## Core documents

The most essential documents to understand the source code and the components:

| Document |Content | Download |
| :------- |:------ |:---------|
| [Protocol of the Swiss Post Voting System](Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf)| This document describes the cryptographic building blocks and shows how they work together to ensure verifiability and vote privacy. Moreover, it provides a mathematical proof that the cryptographic protocol achieves the desired security objectives under a minimal set of assumptions. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf?inline=false"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [System specification](System/System_Specification.pdf) | The system specification provides a detailed specification of the cryptographic protocol—from the configuration phase to the voting phase to the counting phase—and includes pseudo-code representations of most algorithms. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/System_Specification.pdf?inline=false"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [E-Voting Architecture](System/SwissPost_Voting_System_architecture_document.pdf) | Architecture documentation of the e-voting system based on the [arc42](https://arc42.org/) architecture documentation style. In case this is unfamiliar the [arc42](https://arc42.org/) site provides a concise overview and examples which are worth taking a moment to review . | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/SwissPost_Voting_System_architecture_document.pdf?inline=false"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Software development process](Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md) | This document describes the software development process. Among other things, it provides information on the agile approach and the tools used, shows the quality aspects of software development and gives an overview of software specification for mathematical algorithms. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.pdf?job=CONVERT"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Test Concept of the Swiss Post voting system](Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md) | This document describes the test concept for the e-voting service. The test concept shows the test procedure, defines the cooperation and the responsibilities of all persons involved in the test process. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.pdf?job=CONVERT"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Infrastructure whitepaper](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md) | The infrastructure whitepaper describes the e-voting infrastructure with all implemented security aspects. This includes information on the data centers, the structure and use of the infrastructure and databases. The various security measures are also described. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.pdf?job=CONVERT"><img src=".gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |

## Folders and their Content

If you are looking for a specific document, we shortly describe what you can find in the folders of this project.

| Folder | Content                                                                                                                                                                                                                  | Type of documents |
| :------- |:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| :---- |
|[Operation](Operations) | Documentation on how to operate the e-voting system.                                                                                                                                                                     | Manuals, Guidelines |
|[Product](Product) | Documentation related to the product e-voting and the software development process.                                                                                                                                      | Information |
|[Protocol](Protocol) | Contains the cryptographic protocol of the Swiss Post e-voting system in mathematical form. It demonstrates that the cryptographic elements protect voting secrecy and guarantee individual and universal verifiability. | Specifications |
|[Reports](Reports) | Contains reports and articles about the e-voting system.                                                                                                                                                                 | Information |
|[Security advices](Security-advices) | Tips and instructions that voters can carry out on the device used for voting to ensure their vote has not been manipulated.                                                                                             | Information for voters |
|[Symbolic-models](Symbolic-models)| We provide a symbolic model of the Swiss Post Voting System and use the cryptographic protocol verifier ProVerif to prove verifiability (both individual and universal) and vote privacy.                                | Specifications, ProVerif files|
|[System](System)| The specifications of the system, the verifier, and the architecture.                                                                                                                                                    | Specifications |
|[Testing](Testing)| Disclosure of our testing policies and procedures.                                                                                                                                                                       | Processes |
|[Trusted-Build](Trusted-Build)| Disclosure of all the relevant information needed to perform your own trusted build and compare your hash results with those of Swiss Post.                                                                              | Processes |

## FAQ

Answers to frequently asked questions on the community programme and definition of terms can be found in the [FAQ](https://evoting-community.post.ch/en/help-and-contact/faq) section of the community website.

## Changelog

Please consult our [changelog](CHANGELOG.md) for a summary of all notable changes made to the project.

## Support & contact

If you have any questions or require help, please do not hesitate to [contact](https://evoting-community.post.ch/en/help-and-contact/support#contact) us.
Our specialists will be pleased to answer your questions regarding cryptography, security, documentation, the Code of Conduct or any other subject relating to Swiss Post’s e-voting system.
