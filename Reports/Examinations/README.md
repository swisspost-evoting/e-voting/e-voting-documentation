# Independent Examinations of the Swiss Post Voting System

The Federal Chancellery mandates independent experts who have rigorously assessed the system as per the Ordinance on Electronic Voting (OEV, [section 26](https://www.fedlex.admin.ch/eli/cc/2022/336/en#lvl_u1/lvl_u1/lvl_26)) and have made their reports [public](https://www.bk.admin.ch/bk/en/home/politische-rechte/e-voting/ueberpruefung_systeme.html). The experts scrutizine the system in four areas: 
- Cryptographic protocol (Scope 1)
- System software (Scope 2)
- Security of infrastructure and operation (Scope 3)
- Protection against attempts to infiltrate the infrastructure (Scope 4). 

Swiss Post has incorporated numerous suggested enhancements in both the software and its documentation. Furthermore, the Vote Électronique steering committee has validated the [Catalogue of Measures](https://www.bk.admin.ch/bk/en/home/politische-rechte/e-voting/versuchsuebersicht.html) for the e-voting trials, ensuring a comprehensive tracking of unresolved issues that necessitate an extended resolution timeline. 

Swiss Post is committed to transparency and continuous improvement and looks forward into enhancing the system and engaging in meaningful dialogue with leading experts in the field of e-voting through this independent examination.

## Swiss Post's responses

Swiss Post conducts thorough analyses of each report and provides a written feedback below.

| **Date**| **Report**    |
|--------------|--------------|
|22.11.2023|[Examination-eVoting-System-Response-Swiss-Post_221123](Examination-eVoting-System-Response-Swiss-Post_221123.pdf)|
|09.02.2024|[Examination-eVoting-System-Response-Swiss-Post_090224](Examination-eVoting-System-Response-Swiss-Post_090224.pdf)|
|12.08.2024|[Examination-eVoting-System-Response-Swiss-Post_120824](Examination-eVoting-System-Response-Swiss-Post_120824.pdf)|
|23.08.2024|[SwissPost_Response_August2024_Scope_3.pdf](SwissPost_Response_August2024_Scope_3.pdf)|
|14.02.2025|[SwissPost_Response_February2025_Scope_1&2.pdf](SwissPost_Response_February2025_Scope_1&2.pdf)|


## Expert's reports

| **Release**| **Date**| **Scope**    | **Expert / Organisation**                     | **Report**                         |
|--------------|--------------|--------------|-----------------------------------------------|------------------------------------|
|-| 03.11.2023|Scope 3|SCRT|[Examination of the Swiss Internet voting system, Version: 2.0 / Audit scope Infrastructure and operations (3) – Measures of the system provider – Round 3 & changes](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_November2023/Scope%203%20(Post)%20Final%20Report%20SCRT%2003.11.2023.pdf.download.pdf/Scope%203%20(Post)%20Final%20Report%20SCRT%2003.11.2023.pdf)|
|Release 1.3.3|21.11.2023|Scopes 1 and 2|Bern University of Applied Sciences|[Examination of the Swiss Post Internet Voting System, 21.11.2023](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_November2023/Scope%201%20and%202%20Final%20Report%20BFH%2021.11.2023.pdf.download.pdf/Scope%201%20and%202%20Final%20Report%20BFH%2021.11.2023.pdf)|
|Release 1.3.3.2|22.12.2023|Scope 2|Bern University of Applied Sciences|[Examination of the Swiss Post Internet Voting System, 22.12.2023](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_Reports_February_2024/Scope%201%20and%202%20Final%20Report%20Addendum%20BFH%2022.12.2023.pdf.download.pdf/Scope%201%20and%202%20Final%20Report%20Addendum%20BFH%2022.12.2023.pdf)|
||19.12.2023|Scope 2|Objectif Sécurité|[Code Review of Voting Card Print Service, added appendix regarding version 2.19.2, 19.12.2023](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_Reports_February_2024/Scope%202%20(VCPS)%20Final%20Report%20Objectif%20S%C3%A9curit%C3%A9%2017.12.2023.pdf.download.pdf/Scope%202%20(VCPS)%20Final%20Report%20Objectif%20S%C3%A9curit%C3%A9%2017.12.2023.pdf)|
|Release 1.3.3.2|29.01.2024|Scope 4|SCRT|[E-Voting Web Application 1.3.3.2, Security Audit Report, 29.01.2024](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_Reports_February_2024/Scope%204%20Final%20Report%20SCRT%2029.01.2024.pdf.download.pdf/Scope%204%20Final%20Report%20SCRT%2029.01.2024.pdf)|
|Release 1.4.3|25.07.2024|Scope 1 and 2|Berner Fachhochschule (BFH)|[Examination of the Swiss Post Internet Voting System](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scopes%201%20and%202%20Final%20Report%20BFH%2025.07.2024.pdf.download.pdf/Scopes%201%20and%202%20Final%20Report%20BFH%2025.07.2024.pdf)|
|Release 1.4.3|11.07.2024|Scope 1 and 2|Thomas Haines|[Final Report of Release 1.4.0 to 1.4.3](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scopes%201%20and%202%20Final%20Report%20Thomas%20Haines%2011.07.2024.pdf.download.pdf/Scopes%201%20and%202%20Final%20Report%20Thomas%20Haines%2011.07.2024.pdf)|
|Release 1.4|24.07.2024|Scope 1|Surrey Centre for Cyber Security, University of Surrey, UK|[Review of the Symbolic Models V1.3 of the Swiss Post Voting System V1.4](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%201%20Final%20Report%20University%20of%20Surrey%2024.07.2024.pdf.download.pdf/Scope%201%20Final%20Report%20University%20of%20Surrey%2024.07.2024.pdf)|
|Release 1.4.3|16.07.2024|Scope 1|Aleksander Essex|[Rolling Re-Evaluation of the Swiss Post e-Voting System: Versions 1.4.0 and 1.4.3](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%201%20Final%20Report%20Aleksander%20Essex%2016.07.2024.pdf.download.pdf/Scope%201%20Final%20Report%20Aleksander%20Essex%2016.07.2024.pdf)|
|Release 1.4.2|26.07.2024|Scope 2|Objectif Sécurité|[Security audit of the e-voting backend version 1.4.3](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%202%20(Backend)%20Final%20Report%20Objectif%20Securit%C3%A9%2026.07.2024.pdf.download.pdf/Scope%202%20(Backend)%20Final%20Report%20Objectif%20Securit%C3%A9%2026.07.2024.pdf)|
|Release 1.4.2|26.07.2024|Scope 2|Objectif Sécurité|[Code review of the Data Integration Service v2.8.3.1](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%202%20(DIS)%20Final%20Report%20Objectif%20Securit%C3%A9%2026.07.2024.pdf.download.pdf/Scope%202%20(DIS)%20Final%20Report%20Objectif%20Securit%C3%A9%2026.07.2024.pdf)|
|Release 1.4.2|22.07.2024|Scope 4|Orange Cyberdefense|[Evoting Web Application 1.4.2.1 – Security Audit Report](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%204%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2022.07.2024.pdf.download.pdf/Scope%204%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2022.07.2024.pdf)|
|Release 1.4.3|19.08.2024|Scope 3|Orange Cyberdefense|[Examination of the Swiss Internet voting system Version: 1.1 / Audit scope: Infrastructure and operations (3) – Measures of the system provider](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_August2024/Scope%203%20(Post)%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2019.08.2024.pdf.download.pdf/Scope%203%20(Post)%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2019.08.2024.pdf)|
|Release 1.4.4|07.01.2025|Scope 4|Orange Cyberdefense|[E-voting Web Application 1.4.4.4](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_January_2025/Scope%204%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2007.01.2025.pdf.download.pdf/Scope%204%20Final%20Report%20Orange%20Cyberdefense%20(SCRT)%2007.01.2025.pdf)|
|Release 1.4.1|10.01.2025|Scope 1 and 2|Johannes Müller|[A Review of the Swiss Post Voting System](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/E_Voting/Examination_reports_January_2025/Scopes%201%20and%202%20Final%20Report%20Johannes%20M%C3%BCller%2010.01.2025.pdf.download.pdf/Scopes%201%20and%202%20Final%20Report%20Johannes%20M%C3%BCller%2010.01.2025.pdf)|