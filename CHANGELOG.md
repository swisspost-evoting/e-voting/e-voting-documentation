# Changelog

## Update 2025-02-14

### Added (2)
- the [Swiss Post’s response report to the expert's examination on Release 1.4 (Scope 1&2)](Reports/Examinations)
- [White Paper on the Auditability of E-Voting Software](Reports/Whitepaper-Sieberpartners-SwissPost-Evoting-2025.pdf)

## Update 2024-12-19

### Added (1)
- the [Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.4.4/Protocols) for the latest productive version of the E-Voting.

### Updated (1)
- the Statement of applicability (SOA) of the [certification ISO/IEC 27001:2022 for Post CH](Product/ISO/README.md).

## Update 2024-12-05

### Updated (1)
- the [ISO/IEC 27001:2020 certificates](Product/ISO/README.md) of Swiss Post.

## Update 2024-11-25

### Added (1)
- the [Trusted Build protocols](Trusted-Build/E-Voting/Release-1.4.4.4/Protocols) for the latest productive version of the DIS, E-Voting and Verifier.


## Update 2024-10-25

### Updated (13)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository
- the [Security Advices](Security-advices).
- the [Software development process](Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- the [Roadmap Swiss Post's e-voting system](Product/Product_Roadmap.md)
- the [Operation whitepaper](Operations/Operation%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- the [Technical Specifications of the Main Components](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)
- the [Recommendation safety measures (cantonal infrastructure)](Operations/Recommendation_Safety_Measures_Cantonal_Infrastructure.md)

## Update 2024-10-02

### Added (1)
- the [Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.3.3/Protocols) for the latest productive version of the E-Voting.

## Update 2024-09-11

### Added (1)
- the [Trusted Build and Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.3.3/Protocols) for the latest productive version of the E-Voting.

## Update 2024-08-28

### Added (1)
- the [Swiss Post’s response report to the expert's examination on Release 1.4 (Scope 3)](Reports/Examinations)

## Update 2024-08-12

### Added (3)
- the final report of the [Public Intrusion Test 2024](Reports/PublicIntrusionTest)
- the [Swiss Post’s response report to the expert's examination on Release 1.4 (Scope 1, 2, 4)](Reports/Examinations)
- the [Trusted Build and Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.3.2/Protocols) for the latest productive versions of the DIS, E-Voting and Verifier.

## Update 2024-07-30

### Updated (3)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [System specification](System)

## Update 2024-07-09

### Added (1)
- the [Trusted Build and Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.3.1/Protocols) for the latest productive versions of the DIS, E-Voting and Verifier.

## Update 2024-06-21

### Updated (12)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository
- the [System specification](System)
- the [Crypto primitives specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- the [Verifier specification](System/Verifier_Specification.pdf)
- the [Trusted Build protocols for E-Voting 1.4.3.0](Trusted-Build/E-Voting/Release-1.4.3.0/Protocols)
- the [Technical Specifications of the Main Components](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)

## Update 2024-06-13

### Added (1)
- the [Trusted Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.4.2.1/Protocols) for the versions used for the public intrusion test 2024.

## Update 2024-06-06

### Added (1)
- the [Trusted Build protocols](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Trusted-Build/E-Voting/Release-1.4.2.1/Protocols) for the versions of the DIS, E-Voting and Verifier used for the public intrusion test 2024

## Update 2024-05-03

### Updated (11)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [DIS script](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Scripts/DIS/DIS.ps1).
- the [Security Advices](Security-advices).
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository
- the [Test Concept](Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)
- the [Technical Specifications of the Main Components](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)

## Update 2024-03-28

### Added (1)
- the [Trusted Build and Deployment Acceptance protocols](Trusted-Build/E-Voting/Release-1.3.4.0/Protocols) for the latest productive versions of the DIS, E-Voting and Verifier.

### Updated (12)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [Verifier specification](System/Verifier_Specification.pdf)
- the [Infrastructure whitepaper of the Swiss Post voting system](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- the [Technical specifications of the main components](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)
- the [Security Whitepaper of the Swiss Post Voting System](Product/Security%20Whitepaper%20of%20the%20Swiss%20Post%20Voting%20System.md)
- the [Roadmap of Swiss Post's e-voting system](Product/Product_Roadmap.md)
- the [DIS script](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Scripts/DIS/DIS.ps1).

## Update 2024-02-16

### Updated (12)
- the [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [Computational proof](Protocol)
- the [System specification](System)
- the [Symbolic Analysis](Symbolic-models)
- the [Crypto primitives specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- the [Verifier specification](System/Verifier_Specification.pdf)
- the [Architecture document](System/SwissPost_Voting_System_architecture_document.pdf)

## Update 2024-02-09

### Added (1)
- the [Swiss Post’s response report to the expert's examination on Release 1.3.3](Reports/Examinations)

### Updated (8)
- the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository

## Update 2023-12-21

### Added (1)
- the [Trusted Build protocols](Trusted-Build/E-Voting/Release-1.3.3.2/Protocols) for the latest productive versions of the DIS, E-Voting and Verifier.


## Update 2023-11-22

### Added (2)

- [Improving the Swiss Post Voting System: Practical Experiences from the Independent Examination and First Productive Election Event, E-Vote-ID 2023](Reports/e-vote-2023-Improving_the_SwissPostVotingSystem.pdf)
- the [Swiss Post’s response report to the expert's examination on Release 1.3.3](Reports/Examinations)

### Fixed (2)

- Clarified a sentence in the security advice instructions for manually checking the hash values.
- Moved the document about the trusted build to the appropriate directory.

## Update 2023-10-27

### Updated (12)

- the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [DIS script](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Scripts/DIS/DIS.ps1).
- the [Security Advices](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Security-advices).
- the [System specification](System)
- the [technical specifications of the main components](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)
- the [Trusted Build protocols](Trusted-Build/E-Voting/Release-1.3.2/Protocols) used for the Federal elections 2023

---

## Update 2023-09-28

### Updated (1)

- the [document](Reports/E-Voting-Community%20und%20Bug-Bounty-Programm%20Post.pdf) providing information on the activities carried out by Swiss Post to ensure public scrutiny

---

## Update 2023-09-25

### Added (1)

- Additional control added to the security advices: [Guide to Displaying and Synchronizing System Time](Security-advices/en/timecheck.md)

---

## Update 2023-09-08

### Added (2)

- the final report in english of the [Public Intrusion Test 2023](Reports/PublicIntrusionTest)
- the [Trusted Build protocols](Trusted-Build/E-Voting/Release-1.3.2/Protocols) for the versions of the DIS, E-Voting and Verifier used for the Federal elections 2023

### Fixed (2)

- wording improvements in the chapters §4.2 and §4.4.6 of the final report in german of the [Public Intrusion Test 2023](Reports/PublicIntrusionTest)
- changes in chapter 6 in the [Swiss Post’s response report to the expert's follow-up examination reports 2023](Reports/Examination2023)

---

## Update 2023-08-16

### Added (2)

- the final report of the [Public Intrusion Test 2023](Reports/PublicIntrusionTest)
- the [Swiss Post’s response report to the expert's follow-up examination reports 2023](Reports/Examination2023)

### Fixed (2)

- images in [Infrastructure whitepaper of the Swiss Post voting system](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- the [Roadmap of Swiss Post's e-voting system](Product/Product_Roadmap.md)

---

## Update 2023-07-21

### Updated (8)

- the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository

---

## Update 2023-07-07

### Updated (1)

- the [Trusted Build protocols](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Trusted-Build/E-Voting/Release-1.3.1.1/Protocols) for the versions of the DIS, E-Voting and Verifier used for the public intrusion test 2023

---

## Update 2023-06-15

### Added (1)

- the [Security Whitepaper of the Swiss Post Voting System](Product/Security%20Whitepaper%20of%20the%20Swiss%20Post%20Voting%20System.md)

### Updated (15)

- the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- the [System specification](System)
- the [Symbolic Analysis of the Swiss Post Voting System](Symbolic-models)
- the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- the [Verifier specification](System/Verifier_Specification.pdf)
- the [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries) repository
- the [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service) repository
- the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository
- the [Script for the Data Integration Service (DIS)](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Scripts/DIS/DIS.ps1)
- the [Trusted Build of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)
- the [Technical specifications of the main components of the e-voting system](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)


---

## Update 2023-05-04

### Added (2)

- the [Trusted Build DIS directory](Trusted-Build/DIS) for the disclosure of the Swiss Post trusted build data-integration-service releases, their hash values and scripting procedures
- the [Accessibility report](Reports/Evoting-Accessibility_WCAG21_April-2023.pdf)

### Updated (1)

- the [Trusted Build report of the Cantons](Trusted-Build/Trusted_Build_Report_Thurgau_TG_SG_BS.pdf)

---

## Update 2023-04-19

### Added (3)

- New repository [E-voting-libraries](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries)
- New repository [Data-integration-service](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service)
- A new [script-directory](Scripts) with PowerShell scripts for executing command-line interface tools

### Updated (15)

- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [Computational proofs](Protocol)
- updated the [System specification](System)
- updated the [Symbolic Analysis of the Swiss Post Voting System](Symbolic-models)
- updated the [Architecture of the Swiss Post Voting System](System/SwissPost_Voting_System_architecture_document.pdf)
- updated the [Trusted Build directory](Trusted-Build)
- updated the [Infrastructure whitepaper of the Swiss Post voting system](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- updated the [Technical specifications of the main components of the e-voting system](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- updated the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- updated the [Verifier specification](System/Verifier_Specification.pdf)
- updated the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository

---

## Update 2023-03-03

### Added (3)

- [Swiss Post’s report in response to the expert reports 2022](Reports/Examination2022)
- [Accessibility report of the eCH-005913 standard](Reports/Evoting-Accessibility_WCAG21_January-2023.pdf)
- [Technical specifications of the main components of the e-voting system](Operations/Technical%20specifications%20of%20the%20main%20components%20of%20the%20e-voting%20system.md)

---

## Update 2023-02-24

### Updated (7)

- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- updated the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- updated the [Verifier specification](System/Verifier_Specification.pdf)

---

## Update 2022-12-13

### Updated (2)

- updated the [Symbolic Analysis of the Swiss Post Voting System](Symbolic-models)
- updated the [Architecture of the Swiss Post Voting System](System/SwissPost_Voting_System_architecture_document.pdf)

---

## Update 2022-12-10

### Added (3)

- documents related to the ISO/IEC 27001:2013 certification of Swiss Post: [two ISO Certificates and the Statement of applicability (SOA)](Product/ISO/README.md)
- the [Trusted Build Verifier directory](Trusted-Build/Verifier) for the disclosure of the swiss post trusted build releases and their hash values, protocols and scripting procedures of the Verifier
- a [document](Reports/E-Voting-Community%20und%20Bug-Bounty-Programm%20Post.pdf) providing information on the activities carried out by Swiss Post to ensure public scrutiny

### Updated (11)

- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [Computational proofs](Protocol)
- updated the [Infrastructure whitepaper of the Swiss Post voting system](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- updated the [System specification](System)
- updated the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- updated the [Verifier specification](System/Verifier_Specification.pdf)
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- updated the [Roadmap of Swiss Post's e-voting system](Product/Product_Roadmap.md)

### Fixed (1)

- fixed some layouts, texts and images

---

## Release 1.1.0.0 (2022-10-31)

### Added (2)

- a separate [changelog](System/CHANGELOG.md) for all the specifications under [System](System)
- the build and deployment protocols for the 1.0.0.2 [Release](Trusted-Build)

### Updated (9)

- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [System specification](System)
- updated the [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) repository
- updated the [Verifier specification](System/Verifier_Specification.pdf)
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- updated the [Product Roadmap](Product/Product_Roadmap.md)

### Fixed (1)

- fixed some layouts, texts and images

---

## Release 1.0.0.0 (2022-10-03)

### Added (1)

- added the build and deployment protocols to the [Trusted Build](Trusted-Build) directory

### Updated (8)

- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [System specification](/System)
- updated the [Verifier specification](System/Verifier_Specification.pdf)
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [Crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository
- updated the [Product Roadmap](Product/Product_Roadmap.md)

### Fixed (1)

- fixed some layouts, texts and images

---

## Release 0.9.2.0 (2022-09-28)

### Added (1)

- added the final report of the [Public Intrusion Test 2022](Reports/PublicIntrusionTest) in english and german

---

## Release 0.9.1.1 (2022-08-18)

### Added (1)

- response to the [Scopes 1+2+3 Final Report by Bryan Ford](/Reports/Examination2021/Scope-1-2-3-Ford-Examination-eVoting-System-Response-Swiss-Post.pdf)

### Updated (5)

- updated [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier)
- updated [Verifier specification](System/Verifier_Specification.pdf)
- updated [Architecture of the Swiss Post Voting System](System/SwissPost_Voting_System_architecture_document.pdf)
- updated [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev)
- updated the [Product roadmap](Product/Product_Roadmap.md)

### Fixed (1)

- fixed missing parts, text and layout in the [Run-Guide of the e2e](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/-/blob/master/Run_Election_Event.md)

---

## Release 0.9.0.0 (2022-07-29)

### Updated (7)

- updated the [Computational proofs](Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf)
- updated the [Product roadmap](Product/Product_Roadmap.md)
- updated the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev) repository to the latest version of evoting
- updated the [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) repository
- updated the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) repository
- updated the [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) repository
- updated the [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) repository

### Fixed (4)

- fixed text and images in [Infrastructure whitepaper of the Swiss Post voting system](Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- fixed text and smaller typos in [Software development process of the Swiss Post voting system](Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- fixed text and images in [Test concept of the swiss post voting system](Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)
- fixed text and images in [Trusted Build](Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)

### Added (2)

- added the [Trusted Build](Trusted-Build) directory for the disclosure of the swiss post trusted build releases and their hash values, protocols and scripting procedures
- re-assessment of the E-Voting Auditability is available under [Reports](Reports/Evoting-Auditability-July-2022.pdf)

---

## Release 0.8.5.8 (2022-07-07)

### Updated (1)

- updated [Symbolic Models](/Symbolic-models)

## Release 0.8.5.7 (2022-06-24)

### Updated (2)

- updated [Verifier specification](https://gitlab.com/swisspost-evoting/System/Verifier_Specification.pdf)
- updated [Swiss Post Voting Protocol](Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf)

---

## Release 0.8.5.6 (2022-06-24)

### Updated (9)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting#change-log-release-015)
- updated [Architecture of the Swiss Post Voting System](System/SwissPost_Voting_System_architecture_document.pdf)
- updated [System specification](/System)
- updated [crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-015)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-015)
- updated [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts#change-log-release-015)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- updated [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.5.5 (2022-06-24)

### Fixed (5)
- minor corrections [Recommendation safety measures (cantonal infrastructure)](/Operations/Recommendation_Safety_Measures_Cantonal_Infrastructure.md)
- minor corrections [Operation whitepaper of the Swiss Post voting system](/Operations/Operation%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- minor corrections [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- minor corrections [ModSec & CRS Tuning process](Operations/ModSecurity-CRS-Tuning-Concept.md)
- minor corrections [Test concept of the swiss post voting system](Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.5.4 (2022-04-21)

### Updated (8)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting#change-log-release-014)
- updated [System specification](/System#changes-in-version-099)
- updated [crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-014)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-014)
- updated [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts#change-log-release-014)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- updated [Readme of Swiss Post's reponses to Examination 2021](/Reports/Examination2021/README.md)

### Fixed (1)
- correction to the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev)

---

## Release 0.8.5.3 (2022-04-20)

### Added (1)

- [Swiss Post’s reports in response to the expert reports](/Reports/Examination2021)

---

## Release 0.8.5.2 (2022-02-18)

### Added (2)

- New repository [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) for the typescript of the e-voting-client
- [Security advices](/Security-advices): Tips and instructions that voters can carry out on the device used for voting to ensure their vote has not been manipulated

### Updated (8)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting)
- updated [system specification](/System#changes-in-version-098)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-013)
- updated [crypto-primitives specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-013)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-013)
- updated [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- images in [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md) and [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)

### Fixed (4)

- fixed smaller typos and text in [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)
- fixed smaller typos and text in [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- fixed broken urls in [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- correction to the [E2E(evoting-e2e-dev)environment](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev)

---

## Release 0.8.5.1 (2022-02-04)

### Added (1)

- [The challenges of enabling public scrutiny, E-Vote-ID 2021](/Reports/e_vote_2021.pdf) under /Reports

### Fixed (2)

- fixed whitespaces in .gitlab-ci.yml
- fixed smaller typos and errors

---

## Release 0.8.5.0 (2021-11-15)

### Updated (3)

- [README](README.md)
- [E-voting sourcecode](https://gitlab.com/swisspost-evoting/e-voting/e-voting)
- [Crypto-primitives sourcecode](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives)

### Added (5)

- Gitlab-script based on pandocker with custom Eisvogel-template to convert markdown documents into pdf documents
- [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)
- [Operation whitepaper of the Swiss Post voting system](/Operations/Operation%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- [Crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain)
- [Verifier sourcecode](https://gitlab.com/swisspost-evoting/verifier/verifier)

### Fixed (1)

- fixed smaller typos and errors

---

## Release 0.8.4.0 (2021-10-15)

### Updated (3)

- [Protocol](/Protocol)
- [System Specification](/System/System_Specification.pdf)
- [Verifier Specification](https://gitlab.com/swisspost-evoting/verifier/verifier)

---

## Release 0.8.3.0 (2021-09-01)

### Added (7)

- [Symbolic proofs](Symbolic-models)
- [ModSec & CRS Tuning process](Operations/ModSecurity-CRS-Tuning-Concept.md)
- [Verifier specification](https://gitlab.com/swisspost-evoting/verifier)
- [SDM Hardening guidlines](Operations/Recommendation_Safety_Measures_SDM.md)
- [ABOUT](ABOUT.md)
- [Auditability report](Reports/Evoting-Auditability-August-2021.pdf)
- [E-voting sourcecode](https://gitlab.com/swisspost-evoting/e-voting/e-voting)

### Fixed (4)

- fixed smaller typos and errors
- improved project structure and wording
- improved README
- improved markdown compliance according to markdownlint

---

## Release 0.8.2.0 (2021-07-19)

### Added (4 Changes)

- [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- [E-voting Architecture documentation of the Swiss Post Voting System](/System/SwissPost_Voting_System_architecture_document.pdf)
- [Test Concept of the Swiss Post Voting System](/Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.1.0 (2021-06-25)

### Fixed (6 Changes)

- minor structural and wording improvements
- fixed smaller typos and errors
- markdown compliance according to markdownlint
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)
- new version of the [system specification](/System/System_Specification.pdf)

---

## Release 0.8.0.0 (2021-05-04)

### Added (5 Changes)

- new directory [System](/System) in /Documentation
    - [Readme](/System/README.md) as accompanying document for the System specifications
        - [System specification](/System/System_Specification.pdf)
- new file [ElectoralModel](/Product/ElectoralModel.md)
- added a changelog-section for the crypto-primitives in the [crypto-primitives readme](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/README.md)

### Fixed (5 Changes)

- structural and wording improvement for the [/Documentation/README.md](/README.md)
- structural and wording improvement for the [overviewfile](Product/Overview.md)
- structural and wording improvement for the [reportingfile](/REPORTING.md)
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)

---

## Release 0.7.0.0 (2021-03-23)

### Added (7 changes)

- new repository [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
    - [README.md](https://gitlab.com/swisspost-evoting/crypto-primitives/readme.md) as accompanying document for the crypto-primitives
        - [Specification](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/cryptographic_primitives_specification.pdf) of the crypto-primitives
- new directory [src](https://gitlab.com/swisspost-evoting/crypto-primitives/src) in /crypto-primitives
    - [Crypto-primitives sourcecode](https://gitlab.com/swisspost-evoting/crypto-primitives/src)
- new directory [product](Product)
    - new File [overview](Product/Overview.md)

### Fixed (4 changes)

- renamed 'Protocol definition' to [/Documentation/Protocol](Protocol) for user-friendliness
- wording improvement for the [/Documentation/Protocol/README.md](/Protocol/README.md)
- Changelog-section in README of [/Documentation](/README.md)
- Overview-section in README of [/Documentation](/README.md)
