---
layout: article
title: Electoral Model
author: © 2023 Swiss Post Ltd.
date: "2021-11-15"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# A quickstart and overview of the electoral model supported

## Introduction

One of the challenges an e-voting system faces in Switzerland, as a federal country, is handling the different voting possibilities in the various entities (Confederation, cantons and communes) that constitute the Swiss federal state. More precisely, it is not only a matter of covering them, but of handling them all with one e-voting system.

This short document describes the approach that Swiss Post has chosen and the goals it has achieved.

## Approach and scope

In an environment that is federal and as diverse as Switzerland, a global approach that considers the complete landscape is doomed to failure. Here are the reasons why we decided to take an iterative top-down approach:

**Why iterative?** This is the safest way to set up a model (as we call it) and to incorporate new elements without risking huge drawbacks at some point in the journey.

**Why top-down?** The legal structure in a federal country like Switzerland follows a mainly top-down approach. Federal law provides a framework that is implemented, and sometimes adapted, at cantonal level. The communes usually base regulation of their political rights on the cantonal law that contains the ground rules. Another reason is that the Ordinance on Electronic Voting (VEleS) covers federal level, which means that we need to ensure compliance with federal level first.

As this analysis is substantial, we needed to define our scope clearly upfront. Indeed, taking account of the complete federal law and 26 sets of cantonal law is a Herculean task. Moreover, it is not useful. The law on political rights not only regulates votes and elections, but also the conditions for referendums and initiatives. This is not part of our scope. Therefore, we formulated our target as follows, in such a way that it is useful for e-voting: ensure the ballot is valid. As a result, we set up a model that enables the system to be configurable efficiently for each situation (type of vote/election, federal/cantonal level, etc.) and **to make sure that a ballot is valid at all times**.

With this in mind, we started to analyze the Federal Act on Political Rights and set up our first version of the model. We then analyzed a single canton and incorporated it in the model, ensuring it did not break the compatibility with the first version. We then took a second canton, a third canton, and so on, always making sure that they integrated well into the model and did not break anything. The analysis focused on both cantonal law and the cantonal constitutions, which can contain relevant content with regard to political rights. Finally, we did this for all 26 cantons. We extended our analysis not only to federal, cantonal and communal level, but also to special-purpose associations, courts, churches, schools, and so on.

Initially, we aimed to make a very generic and abstract model that was as simple and decoupled from its context as possible. We quickly noticed that people to whom we presented our intermediary results found contextualization (very) helpful and welcome. We have therefore adapted our goal and expanded the model in order to communicate and scrutinize our model much more efficiently.

Finally, we applied this process at all levels of state power for both votes and elections. Obviously, the degree of difficulty for votes is completely different from that of elections. For this reason, we will concentrate on elections in the remainder of this document.

## Result

The electoral model is composed of two elements:

1. Firstly, a DMN (Decision Model and Notation) model that represents the decision flow to ensure a valid ballot at all relevant steps and the related decision inputs (laws, data, etc.)
2. Secondly (and more tangibly for the e-voting software definition and implementation), a set of Excel sheets containing all the features of an election (or a vote) that can have different values depending on the canton and / or power level (federal, cantonal, communal). This point is more important, as it has to be used for the configuration of the e-voting software, whereas the first point is the same in all cases (only a distinction between votes and elections is necessary).

To explain the second point, let us illustrate it through these examples:

Consider the election of the Council of States. This chamber is part of the Federal Assembly, though the law governing election is at cantonal level, which means that it can vary from one canton to another. And this is indeed the case. One feature that can change from one canton to the next is the electoral system: though more than 90 percent of the cantons use the first-past-the-post system, two cantons use proportional representation. Due to the fact that there are differences between cantons, this feature has to be part of our model.
Another example is the number of seats allocated to each canton. While most cantons have two, the so-called half-cantons have only one. Again, this is a feature that needs consideration, as different values are possible.

Analysis of all the constitutions and laws leads us to a set of features that can have different values. Surprisingly, the number of features is not that big and – besides a few exceptions – does not provide any causality (meaning if one feature has a certain value, you can deduce that a subset of other features will have a given value).

The model is based on analysis of the law, i.e. it covers the explicitly written rules. As is generally the case with legal texts, you also have to consider the implicit aspects as well as any customs. For that reason, it is necessary to check the model with each canton to finalize and validate it.

## Conclusion

Eventually, this approach proved to be very robust, with a broad coverage so far. As well as the fact that it has provided us with valuable input that enabled us to make our e-voting system configurable and flexible, it also helps a great deal to have a common language with the cantons and to obtain an overview of potentially discovered business requirements very quickly. Last but not least, it requires a regular review, because laws change, which could mean that modifications are required.

The current Swiss Post e-voting system implements all(1) the defined features of the electoral model. As soon as the source code has been published, we will provide some insight on how to link the main components of the model to source code.

## Glossary

- *Council of States*: the Council of States is one of the two chambers of the Federal Assembly of Switzerland. It is constituted of 46 members (seats) that represents with equal representation the cantons (2 seats per canton) excepted for the so-called half cantons that only have 1 seat. The Council of States is considered as being the upper chamber.
- *Election*: Procedure by which the members of certain public bodies or other public offices are appointed based on the votes cast in their favour by those eligible to vote or by the members of an electoral body (e. g. the Federal Assembly or the Federal Council)(2).
- *First-past-the-post election*: Electoral system in which the seats to be allocated go to those obtaining either the most votes or more than half of the votes cast(3).
- *Proportional election*: Election in which the seats are allocated among the parties in proportion to the number of votes cast in their favour(4).
- *Vote*: Mostly called referendum in the common language, a vote is the process to enable citizens to give their opinion by casting votes on given subjects.

Notes:

1 There is one feature – namely, that the number of candidates on a list can be greater than the number of mandates seats – that is not implemented, as more clarification is needed.  
2,3,4 Source: [TERMDAT](https://www.bk.admin.ch/bk/en/home/dokumentation/languages/termdat.html), the Federal Administration’s terminology database
