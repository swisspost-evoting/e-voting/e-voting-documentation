# E-Voting ISO/IEC 27001:2022 Certification

This folder contains documents related to the ISO/IEC 27001:2022 certification of Swiss Post

| Document | Content |
|:-------|:--------|
| [ISO Certificate Swiss Post](ISO_27001_Certificate_SwissPost.pdf) | ISO/IEC 27001:2022 certificate for Swiss Post Ltd.| 
| [ISO Certificate Communication Services](ISO_27001_Certificate_PostCHCommunication.pdf) | ISO/IEC 27001:2022 certificate for Post CH Communication Ltd. | 
| [SOA for the Certification CS](SoA_ISMS_E-Voting.pdf) | Statement of applicability (SOA) of the certification ISO/IEC 27001:2022 for Post CH Communication Ltd.| 
