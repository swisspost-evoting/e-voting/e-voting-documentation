---
layout: article
title: Security Whitepaper of the Swiss Post Voting System
author: © 2024 Swiss Post Ltd.
date: "2024-03-05"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...


# Security Whitepaper of the Swiss Post Voting System

## Introduction
Switzerland has a longstanding tradition of direct democracy, allowing Swiss citizens to vote approximately four times a year on elections and referendums. However, in recent years, voter turnout has hovered below 40 percent.

The vast majority of voters in Switzerland fill out their paper ballots at home and send them back to the municipality by post, usually days or weeks before the election date. Therefore, remote online voting (referred to as e-voting in this document) would give voters some advantages. First, it would eliminate the possibility of an invalid ballot when inadvertently filling out the ballot incorrectly. Second, it would improve accessibility for people with disabilities. Third, it would ensure the timely casting of votes (especially for Swiss citizens living abroad).

In the past, multiple cantons offered e-voting to a part of their electorate. Many voters would welcome the option to vote online - provided the e-voting system protects the integrity and privacy of their vote.

State-of-the-art e-voting systems alleviate the practical concerns of mail-in voting and, at the same time, provide a high level of security. Above all, they must have the following three properties:

- Individual verifiability: allow voters to be certain that the system correctly registered their vote
- Universal verifiability: allow an auditor to check that the election outcome corresponds to the registered votes
- Vote secrecy: do not reveal a voter’s vote to anyone

Based on these principles, the Federal Chancellery defined stringent requirements for e-voting systems. The Ordinance on Electronic Voting (OEV) and its technical annex (OEV annex) set out these requirements.

Swiss democracy deserves an e-voting system with excellent security properties. In response to the release of an earlier version of the documentation and the source code of the e-voting system, multiple researchers published attacks, highlighted vulnerabilities and suggested improvements. Swiss Post is thankful to all security researchers for their contributions and the opportunity to improve the system’s security guarantees. We also enjoy engaging with academic experts and the hacker community to maximize public scrutiny of the e-voting system.

This security whitepaper describes the security features and mechanisms of the e-voting system in order to achieve compliance with the Ordinance on Electronic Voting. The whitepaper includes detailed explanations of the security features such as authentication, access control, data encryption and intrusion detection. It also discusses any vulnerabilities or threats that the system may be susceptible to, as well as any mitigations that are in place to address these issues. A list of key documents is available in the annex. 

## Scope
### Business Context

The  architecture document [[3](#references)] outlines the Swiss Post Voting System's business context in chapter 3.

### Solution Strategy

The  architecture document [[3](#references)] outlines the Swiss Post Voting System's solution strategy in chapter 4.

## Cryptographic Protocol

We publish multiple documents explaining the Swiss Post Voting system's cryptographic protocol,
demonstrating that we fulfil the legal requirements for electronic voting in Switzerland (Federal Chancellery Ordinance on Electronic Voting - OEV [[6](#references)]):

- Computational proof [[2](#references)]
- Symbolic proof [[11](#references)]
- System specification [[1](#references)]
- Verifier specification [[12](#references)]
- Crypto primitives specification [[13](#references)]

## Data Protection
Data protection describes the use of various tools and techniques to safeguard digital information from unauthorized access, modification, theft, or destruction. 

### Core Principles
By design, the architecture of the Swiss Post E-Voting solution ensures data protection, following three core principles:

- **No personal data in the Swiss Post environment** \
All personal information (mainly found in the electoral register) remains in the cantons. It can be handled by Swiss Post software components, but only within the infrastructure of the cantons.


- **Encryption** \
All sensitive data (mainly the ballots) in the Swiss Post infrastructure is encrypted. Moreover, the private key to decrypt this information is held by the cantons.


- **Deletion of vote data** \
As with voting on paper, once the result of the vote is beyond dispute, the data is deleted from the cantonal and Swiss Post infrastructure.

### Organization
Below are some of organizational measures stated regarding data protection. 

- **Information Security and Data Protection (ISDP) concept** \
Information security is the practice of protecting information from unauthorized access, use, disclosure, disruption, modification, or destruction. It involves implementing various security measures to safeguard digital and physical data against threats, such as hackers, malware, and natural disasters. Data protection security, on the other hand, is the practice of safeguarding personal information that is stored or processed by an organization. This includes information such as names, addresses, social security numbers, and financial information. Data protection security is necessary to comply with regulations and to maintain the trust of customers and partners. Both information security and data protection security are essential to ensure the confidentiality, integrity, and availability of data. Confidentiality ensures that only authorized individuals have access to data. Integrity ensures that data is accurate, complete, and reliable. Availability ensures that data is accessible to authorized individuals when needed.


- **Separation of duty** \
Separation of duty is an important principle and is applied in the entire e-voting area. The responsibilities are divided organizationally in such a way that no one can carry out a task themselves. This means, for example, that a software developer cannot write source code, test it and then put it into production. The separation of duties is a principle in which different tasks and responsibilities related to a specific process or system are divided among multiple individuals or groups. The goal of this principle is to minimize the risk of fraud, errors and other undesirable events by ensuring that no single person has complete control over a critical function or task. The separation of duties is particularly important in industries that handle sensitive information or financial transactions, such as banking, healthcare and government. In these industries, it is common to require multiple levels of approval or verification before sensitive information is accessed or transactions are executed


- **Access and permissions concepts** \
An access concept refers to the way in which users are granted permissions to perform certain actions or operations within a system or application. Access control is the process of managing access to resources and ensuring that users only have access to the resources they are authorized to access. In computer security, access control is the selective restriction of access to a resource or information. Access control mechanisms can be used to implement various security policies, such as mandatory access control, discretionary access control, role-based access control, and attribute-based access control. Permissions, on the other hand, are rules that define the level of access that a user or group of users has to a particular resource or information. Permissions can be granted or denied to users or groups, depending on their roles or levels of authority. In general, access control and permissions are used to protect sensitive data, prevent unauthorized access, and maintain the integrity of a system or application. Access control and permissions are critical components of any security architecture, and their implementation is essential for ensuring the security of an organization's assets.


- **4-6 eyes principle** \
The four-eye principle, also known as the two-person rule, is a management and security principle that requires at least two individuals to authorize a particular action, decision, or transaction before it is carried out. This principle is commonly used in organizations to ensure that critical or sensitive activities are reviewed by more than one person, thus reducing the risk of errors, fraud, or other types of misconduct. For changes to the cryptographic part of the system we apply a six-eyes principle.


## Public Scrutiny
Transparency and public scrutiny are necessary in order to gain the trust of voters and the authorities when it comes to electronic voting. Public scrutiny of an IT system has two aims:

- Independent experts can critically examine the system and report potential vulnerabilities to the system developer.
- Establishing trust

Scrutiny refers to the process of analyzing and examining specification, software code or applications to ensure that they meet certain standards or criteria. Scrutiny can be done manually by human reviewers or through the use of automated tools that scan the code for issues or potential vulnerabilities. The goal of scrutiny is to identify any problems or weaknesses in the system that could lead to errors, crashes, or security breaches.

### Swiss Post E-Voting Community Programme
Since the beginning of 2021, Swiss Post has been disclosing its new e-voting system in stages, as part of a community programme. The source code and key documents are continuously, iteratively and openly published. This iterative approach is central: we regularly update the published artefacts and continuously improve our publication, based on feedback and learning from the e-voting community and IT experts. 

- **Source code publication** \
The source code of the e-voting system is published on GITLab [[7](#references)] and publicly accessible. No registration with acceptance of terms and conditions is required. We took a collaborative approach of defining a code of conduct with experts from the community. The code of conduct governs access to the e-voting material within the framework of the Swiss Post e-voting community programme

- **System documentation publication** \
Besides the source code, all relevant documents on the system are fully published on GitLab [[7](#references)]: protocol, specification, architecture, operation and infrastructure as well as the software development process. 

Below, we explain the most important aspects of our community programme in greater depth:

- **Key parts of the code under an open source licence** \
Key parts of the code are published under an open source licence: key cryptographic algorithms, known as crypto-primitives, are available in a library provided by Swiss Post. The Swiss Post verifier, an open verifier software to verify the Swiss Post Voting Protocol, is also available under an open source licence.
We have chosen the permissive open-source licence (Apache 2) to facilitate and encourage further development and reuse by third parties. This allows Swiss Post to gain from the experience of the reuse of this code.
- **Transparent development** \
Transparent development is Swiss Post’s approach to software development for its evoting service. The transparent development approach supports disclosure on the following points:
    - Swiss Post permanently discloses software releases.
    - Software increments are published between releases and are made available on a designated branch. An increment is a defined feature or /task designed to make it easier to understand the changes in the source code.
    - The commits history for the software increments and releases are published. For all files, the commit history can be examined either per release or per increment.
    - The contributions from the community (e.g. pull requests) are reviewed, and if accepted, integrated into the e-voting source code. This means that changes or improvements from the community can be included in the source code.

- **Publication of a compilable system** \
The building of the whole e-voting system has been made much easier and uses standard tools only. The maven build can be called in a standard environment with only one command.

- **Opportunities to simulate contests** \
We provide researchers with a docker environment to run the whole e-voting system and simulate election events on their own infrastructure – i.e. they can run a ballot with productive data and also carry out attacks (including adjustments to the source code).

- **Permanent bug bounty programme**
    - In order to continuously improve the security of its digital products, Information Security at Swiss Post operates a Group-wide bug bounty programme and e-voting is also part of it.
    - For the bug bounty, we defined a grid combining for the different scopes, an evaluation based on the industry standard common vulnerability scoring system (CVSS), plus specific scenarios taking into consideration the e-voting’s specificities.
    - The public programme covers three aspects:
        - Static tests (search for errors in the disclosed documentation or source code)
        - Dynamic tests (search for errors by analysing the executable system in a private infrastructure)
        - Internet tests (attacks on the provider's infrastructure, fixed term annual test)
    - We see the bug bounty programme as a programme within the community programme. This means that researchers do not have to register for the bug bounty programme in order to be able to scrutinize the system if they do not wish to. All system artefacts and documents which are disclosed can be accessed on GitLab without signing up to the bug bounty programme.
    - Our aim is to provide the highest possible level of transparency. All accepted findings of the bug bounty programme are also published on our Gitlab [[7](#references)].

- **Communication**
    - Communication tailored to the target audience is important and needs to be addressed on an ongoing basis. For the expert audience, we publish a blog for each milestone (disclosed objects) and send an e-mail with information.
    - In order to explain our system and approach in depth, we conduct expert webinars on a regular basis. 
    - We have built a dedicated community website[[8](#references)] where access to all information and to all disclosed system items can be found. The platform is updated step-by-step and is written in a language that non-experts can also understand. 

### Penetration Tests and Audits
These tests are carried out by mandated companies:

- **Penetration tests** \
With penetration tests, the system is attacked by independent companies and then checked for vulnerabilities. The tests are carried out with every new system release.

- **Audits** \
Audits are carried out by independent companies, universities and technical colleges to check the source code and the documentation.

## Software Security - Secure Development Software Lifecycle
Software security refers to the process of designing, developing, testing and deploying software applications that are resistant to attacks and protect the data they handle. Software security involves a range of practices such as secure coding, threat modelling, code review, penetration testing and vulnerability scanning. It also includes the use of security tools and technologies such as firewalls, intrusion detection and prevention systems and encryption. A secure software development lifecycle (SDLC) is a structured approach to building software applications that are designed with security in mind from the very beginning of the development process. The SDLC encompasses all phases of software development, from planning and design to testing and deployment, and involves the integration of security practices and controls throughout each stage of the process. The secure SDLC is designed to ensure that software applications are developed in a secure and resilient manner, with security integrated throughout the development process. By adopting a secure SDLC approach, organizations can reduce the risk of security breaches and data loss, protect their critical assets and enhance their overall security posture.

The following points show some of the security measures applied:

- **Software Assurance Maturity Model, OWASP-SAMM** \
Swiss Post uses the security framework “Software Assurance Maturity Model, SAMM” for secure software development lifecycle from the foundation “Open Web Application Security Project, OWASP”. SAMM enables Swiss Post to analyse and improve the software security of ongoing developments. It measures and assesses security practices across five business functions in the software lifecycle. The assessment evaluates the maturity of security aspects and provides best practices and guidelines. 

- **4-6 eyes principle and separation of duty** \
These principles are applied throughout the software development. See chapter [Organization](#organization).

- **Tool chain** \
Software tools are used for safety and quality assurance and various stages of the processes. The entire tool chain is described in the Software Development Process[[4](#references)].

- **In-house software development in Switzerland** \
The software is developed exclusively by Swiss Post in Switzerland.

- **Threat modelling**  \
Threat modelling refers to a structured approach of identifying and prioritizing potential threats to a system. The Federal Chancellery’s Ordinance defines the threat model and the desired security objectives. The system must provide verifiability and privacy even in the presence of a malicious voting client, voting server and three out of four malicious control components. Our threat modelling technique is currently based on the threat model that the Ordinance prescribes. The further implementation of the STRIDE methodology in the software development process started in Q4 2022. Accordingly, Swiss Post focuses on threat analysis in the event of further developments or vulnerability remediation. Threat analysis is based on threat modelling, in which all threats are analysed and presented on a visualized model of the software or partial component. The development department of the e-voting system works according to agile methods with sprints. A sprint a is an achievement of an intermediate goal within a defined time frame, usually two weeks. Threat modelling is carried out in each sprint for the next sprint work, which may involve the further development of components, sub-components or new components. Threat modelling is performed using the STRIDE method during the e-voting software development cycle. The STRIDE method works through five different phases, with the actual STRIDE method taking place effectively in identifying threats.
The underlying goal is to establish a reference threat model for the Swiss Post Voting System.

- **Security Champion** \
The Security Champion Program is used for communication, knowledge sharing and collaboration between the security and development team. A Security Champion is a defined role. As part of this, a member of the development team acquires knowledge of security issues and is able to implement it as an authority in development. This member also serves as an interface between the development and security experts. In addition, as a member of the development team, the Security Champion is closer to the ongoing development lifecycle, allowing optimization of security measures (e.g. code review) at an early stage in continuous development.

## Network Security
Network security is the practice of protecting computer networks and their associated systems, devices, and data from unauthorized access, use, disclosure, modification, or destruction. It involves a combination of technologies, processes, and policies designed to secure networks from a wide range of threats, such as viruses, malware, phishing attacks, hacking, and data breaches. The main goal of network security is to ensure the confidentiality, integrity, and availability of network resources and data. Confidentiality refers to the protection of sensitive information from unauthorized disclosure or access, such as personal or financial data. Integrity ensures that data is accurate, complete, and uncorrupted, and has not been tampered with. Availability means that network resources and services are accessible to authorized users and are not disrupted or disabled by external attacks or system failures.

### Deployment View
The voting system is a distributed system deployed in three main network zones.

- Swiss Post’s internal network
- Cantonal network
- Internet

**Swiss Post**

The control components execute sensitive, intensive mathematical operations and are deployed on separate bare-metal machines. Each machine is bootstrapped with a different hardened operating system (OS) to mitigate against OS-level exploits.
The Voting server, Voter Portal, and message broker components are deployed to a Kubernetes platform to maximize flexibility and scalability. Infrastructure as code is used for the deployment.
The control components and resources in Kubernetes are deployed via Ansible plain YAML.

**Canton**
The following components are deployed on hardened bare metal hardware on the canton’s infrastructure: Secure Data Manager (SDM) instances and Verifier.
All sensitive operations are carried out on the offline SDM with no network interfaces, and it transfers data to the connected online SDM via data storage devices (typically USB sticks).

See the “e-Voting Architecture Document”[[3](#references)] chapter 7 for more information.

### Security 
**Encrypted connections**

All network connections are encrypted.


**Voting Portal hashes / security advices**

Security advice for the use of e-voting is available on GitLab [[9](#references)].  Voters can carry out various checks on the device used for voting to ensure their vote has not been manipulated.

## Operation Security
Software Operation Security (OpSec) refers to the measures and practices put in place to ensure the secure and reliable operation of software systems. OpSec involves implementing controls and procedures to protect against both intentional and unintentional security threats that may arise during the operation of software. Some common OpSec measures include access controls, data encryption, intrusion detection, and incident response plans. Access controls ensure that only authorized users can access the software and its data. Data encryption protects sensitive information by converting it into a format that can only be read with the proper decryption key. Intrusion detection systems monitor the software for any attempts to breach its security and alert the appropriate parties if any suspicious activity is detected. Incident response plans provide guidelines and procedures for responding to security incidents, including identifying and containing the incident, assessing its impact, and taking steps to prevent similar incidents in the future. OpSec is critical for maintaining the security and integrity of software systems, particularly in industries such as finance, healthcare, and government, where the consequences of a security breach can be severe. By implementing strong OpSec practices, organizations can reduce the risk of security incidents and protect their sensitive data and assets.

- **Trusted build and deployment** [[10](#references)] \
A "trusted build" refers to a software build process that can be verified to produce an executable or library that is free from any malicious code or security vulnerabilities. A trusted build process involves using secure development practices, such as code reviews, static code analysis, and vulnerability scanning, to ensure that the resulting binary is safe to deploy. A "trusted deployment," on the other hand, refers to the process of deploying software in a secure and controlled manner. This involves using secure protocols and encryption to protect data in transit and at rest, as well as implementing access controls and other security measures to prevent unauthorized access. In summary, both trusted builds and trusted deployments are important aspects of software development that help ensure the security and reliability of software systems. The Trusted Build and Trusted Deployment is used for each production software release.  Trusted Build of the Swiss Post Voting System
Direct Trust Certificates
In the PKI (Public Key Infrastructure), there are various models for trusting the public key of an owner. One of the models is called Direct Trust. Only the owner of the private key is trusted. The public key is handed over directly or checked with the help of the owner using the fingerprint. The required public keys are personally handed over and verified in a Direct Trust Ceremony between the representatives of the cantons and Swiss Post. The certificate and key management of the system components of the Swiss Post Voting System and in particular the verification of the digital signatures is based on Direct Trust. 

- **Security scans** \
Components are scanned with automated security testing tools. The tools Fortify and Sonar (for SAST) and Xray (for SCA) are used for security testing. DAST tests are covered by the manual or half -automated execution of penetration tests for major releases. The software Xray is to detect and to track violations in the components. New vulnerabilities are tracked in JIRA Tickets. 

- **Staff review - vetting process** \
All Swiss Post employees are subject to a recruitment vetting process by the HR unit. Among other things, the following criteria of a Swiss Post employee are checked on the Debt collection register (credit information), Confirmation of employment history and empty times and Authentication of education certificates, diplomas, professional licenses. In addition to general requirements, e-voting employees have other requirements that they must fulfil, such as, Criminal records extract (criminal offences, prison sentences and Non-disclosure agreement (NDA). Depending on the department and function within Swiss Post, further contracts or information about the employee's private life may become necessary. This is to protect the public, as Swiss Post has various sensitive data in its possession (e.g. address data of private individuals, used for mail and parcel carriers). All employees must also comply with Swiss Post’s compliance regulations, which cover various areas relating to ethics, culture, data protection, corruption, customer relations and interactions with other people. 

## Infrastructure Security
Software infrastructure security refers to the protection of the underlying software systems and components that support an organization's IT infrastructure. It involves a range of measures aimed at ensuring the confidentiality, integrity, and availability of critical software components such as operating systems, databases, middle-ware, and network protocols. Effective software infrastructure security is essential for protecting against a wide range of cyber threats, including malware, ransomware, data breaches, and other types of cyber attacks. Overall, software infrastructure security is a critical aspect of IT security and requires a proactive and holistic approach to ensure that an organization's systems and data remain secure.

The Infrastructure Whitepaper [[5](#references)] describes the various applied security measures in detail.

<a id="references"></a>
## References
- [1] [System specification](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/System_Specification.pdf). 
- [2] [Protocol of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf).
- [3] [E-Voting Architecture](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/SwissPost_Voting_System_architecture_document.pdf).
- [4] [Software development process](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md).
- [5] [Infrastructure whitepaper](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md).
- [6] [The Federal Chancellery’s Ordinance on Electronic Voting](https://www.fedlex.admin.ch/eli/cc/2022/336/en).
- [7] [E-Voting Documentation Landing Page](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/ABOUT.md).
- [8] [E-Voting Swiss Post Community Website](https://evoting-community.post.ch).
- [9] [E-Voting Security Advices Website](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Security-advices/en). 
- [10] [Trusted Build of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Trusted-Build/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md?ref_type=heads).
- [11] [Symbolic models](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Symbolic-models).
- [12] [Verifier specification](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/Verifier_Specification.pdf).
- [13] [Crypto primitives specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf).