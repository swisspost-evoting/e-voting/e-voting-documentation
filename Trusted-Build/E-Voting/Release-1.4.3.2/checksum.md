
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **voter-portal-1.4.3.2.zip**  |  1.4.3.2  |  ef3330066c8a3c3691a2f24ee3c390ea28add0d0ee0ccfd7afa2ffb1c9ecadd2 |
|    --> index.html  |  1.4.3.2  |  1c1dd71bb07461ca3cb82406d02214e204fb6d5a70fe1741c1639097f51aa831 |
|    --> crypto.ov-api.js  |  1.4.3.2  |  bd32848a0e07e2dccef465d47d4e295ed079be1dbee43f652bec1294fadede48 |
|    --> crypto.ov-api.js  |  1.4.3.2  |  sha384-9alQr57CZH3awteoDnjYkHFhdgE7OQlCh+RWiTSx39sM989KfgIvmYiaB/QEmQ8J |
|    --> crypto.ov-worker.js  |  1.4.3.2  |  e7f9fbc177cd4dce2d014633410b4c377b904b967c07073d0ddd674fcf470f5e |
|    --> crypto.ov-worker.js  |  1.4.3.2  |  sha384-U1Z9zf7YVDoC87bmfWT0l5dCPbiveoKkAwu2/uueqAzsvG4FWmiHkXtNcFCwIj4j |
|    --> main.js  |  1.4.3.2  |  7b1baa8d37d560371a35b9ea41adeaab1a7ee78dff0db283930f257af7451111 |
|    --> main.js  |  1.4.3.2  |  sha384-ZdZYowJf6LskiFMxr/vVvPN2WokgMlMvoNOGlOd+Q8iQzBPBo3/5p5VgDNy4M7in |
|    --> polyfills.js  |  1.4.3.2  |  cc870ae271d9fc412e1ca30b5dd14926bbc4012150cf2989b334df7c02556ed6 |
|    --> polyfills.js  |  1.4.3.2  |  sha384-5FgKmq12RDumrub2BWyG7nG16Uhr3/uq/g58n/as3ABNJ449XfcgJAPk5bALy6r6 |
|    --> runtime.js  |  1.4.3.2  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.3.2  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.3.2.zip**  |  1.4.3.2  |  242deaca515eb40ce1e68c24a0ade1d55d1b382fed9a3c6cc4b38b3e569d8ad2 |
| **voting-server-1.4.3.2-runnable.jar**  |  1.4.3.2  |  302ca2cd696d9bcc974bcd9fb7cfbac5189835ba8b62827d7c83efa7c8362c9e |
| **file-cryptor-1.4.3.2-runnable.jar**  |  1.4.3.2  |  6d6dc85e7c6f81b3c22e0203b632cb9e1499e33be25e5d427c3c5f6534c51aab |
| **direct-trust-tool-1.4.3.2.zip**  |  1.4.3.2  |  242e7db89678a2f7198ffe696b57cd211a78d3e68111bf6015be7f6c8011b547 |
| **xml-signature-1.4.3.2.jar**  |  1.4.3.2  |  065a52cfe13d3eeace3c3e04b6d932995f79832a40779e384a11bbcb9b804eeb |
| **control-component-runnable.jar**  |  1.4.3.2  |  864fcea78b6e0cd4a649187249d077cd88394cc5d0517e2ef717370088b1d837 |