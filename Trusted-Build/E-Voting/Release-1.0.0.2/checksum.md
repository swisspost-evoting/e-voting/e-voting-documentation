| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-components-runnable.jar**  |  1.0.0.2  |  4bb65ce0dcd62214e439396f5a0e059fd15f3fa9fed81ed70091a85001cc49a6 |
| **secure-data-manager-package-1.0.0.2.zip**  |  1.0.0.2  |  092d0f4b6ebc702393deb049e50a9030eec0512b7b507fe6090690c3d2d9b0da |
| **voter-portal-1.0.0.2.zip**  |  1.0.0.2  |  fb9dfe7bbcd577eadd454d260faa218ed182e6ed461da39f235f1c1f07c4e800 |
|   ** ¦-> crypto.ov-api.min.js**  |  1.0.0.2  |  48c9422b819d13449cb141d2d9e158aed1e1fd8d3cfb5e3aaa10365b8b2e07ae |
|   ** ¦-> main.js**  |  1.0.0.2  |  ed68ed3720717ed7f74c4c0207604a97d1301302f16673a92e8127814324c79b |
|   ** ¦-> polyfills.js**  |  1.0.0.2  |  de7c8cf23c24b44b2d46d402cb5e56c60991b876d0a8f244f75b7c3281718a4c |
|   ** ¦-> runtime.js**  |  1.0.0.2  |  cfb9e12b148b5e2aa0cb02f16a262fd2aefb270af0e08f5ab36707476c5d4e39 |
| **ag-ws-rest.war**  |  1.0.0.2  |  8c60697d49b82edea95e7feef6c1b2faa599ce2ea8a643da3466fa4755ebe5fa |
| **au-ws-rest.war**  |  1.0.0.2  |  8e12deab96651e9e9481072be7d6b66a180083121e72a36e3dd2fd94237d9957 |
| **cr-ws-rest.war**  |  1.0.0.2  |  925fa7ef6551899e2ac1a3beb4b7f0d791dda03bb3ba27efa632ad5c78e389c5 |
| **ei-ws-rest.war**  |  1.0.0.2  |  85f0bec4089f7d63645e92d997b1e10902d3160b7dfa80f43f3d988e6765ec61 |
| **ea-ws-rest.war**  |  1.0.0.2  |  b2bed5ea0d5cb51e1460d02b538e74433d2d89246f6729b4cf2ac87d63eb9fa8 |
| **message-broker-orchestrator-1.0.0.2.jar**  |  1.0.0.2  |  8b5280a488413efa69b6d438f2169cf96be3c6e1c1c85af4ba6ef43f4774a890 |
| **vv-ws-rest.war**  |  1.0.0.2  |  5c79159b731cd037eebbe99655f51529603285ec1218b5f27e51e6a1f8584ce3 |
| **vm-ws-rest.war**  |  1.0.0.2  |  15051cf6b141c2469483bbee49e7cfb15405ad3e3dc41ce8882c1018bef5c9d6 |
| **vw-ws-rest.war**  |  1.0.0.2  |  94cfa37f817bd2a0c54275ffe2b2f4fc45eea3e97b76a87180ee3c113e30b668 |