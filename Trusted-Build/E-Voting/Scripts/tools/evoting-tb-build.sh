#!/bin/bash

#
# (c) Copyright 2024 Swiss Post Ltd.
#

POSTFIX=""

BRANCH="master"
OUTPUT_DIR="evoting"
TOOLS_DIR="/tools/"
DATA_DIR="/data/"

usage() {
  printf "build.sh [-s <mvnsettingsFile>] [-f] [-b <branchName>] [-e <evoting-git-url>] [-c <crypto-primitives-url>] [-o <outputDirectoryName>] [-t <toolsDir>] [-d <dataDirectory>] [-n <npmrcFile> [-h]\n"
  printf "Options:\n"
  printf "f   Flatten builded executables in outputDirectory. Feature not activated by default\n"
  printf "b   Branch to checkout. % character will be replace with module name (evoting | crypto-primitives | crypto-primitives-domain). Default is ${BRANCH}\n"
  printf "o   outputDirectoryName where builded executables will be put. Default is ${OUTPUT_DIR}\n"
  printf "t   directory where the tools are located (with iaik files). Default is ${TOOLS_DIR}\n"
  printf "d   directory where the output will be put. Default is ${DATA_DIR}\n"
  printf "n   .npmrc file\n"
  exit 0
}

while getopts ":s:fb:o:ht:d:n:" arg; do
  case $arg in
    s) MVN_SETTINGS="-s ${OPTARG}"
      ;;
    f) POSTFIX="-${HOSTNAME}"
	     FLATTEN="-f";
      ;;
    b) BRANCH=${OPTARG}
      ;;
    o) OUTPUT_DIR=${OPTARG}
      ;;
    t) TOOLS_DIR=${OPTARG}
      ;;
    d) DATA_DIR=${OPTARG}
      ;;
    n) NPMRC_FILE=${OPTARG}
      cp ${NPMRC_FILE} ~/.npmrc
      ;;
    h) usage
      ;;
    *) # Display help.
      echo "wrong parameters"
      usage
      ;;
  esac
done

cp -r evoting building
cd building

export EVOTING_HOME=$(pwd)

cd crypto-primitives
if ! mvn ${MVN_SETTINGS} clean install -DskipTests --no-transfer-progress; then
  exit 10
fi

cd ..
cd crypto-primitives-ts
if ! mvn ${MVN_SETTINGS} clean install -DskipTests --no-transfer-progress; then
  exit 10
fi

cd ..
cd e-voting-libraries
if ! mvn ${MVN_SETTINGS} clean install -DskipTests --no-transfer-progress; then
  exit 10
fi

cd ..

if ! MAVEN_OPTS="-Xms1g -Xmx6g" mvn ${MVN_SETTINGS} clean install -DskipTests --no-transfer-progress; then
  exit 10
fi

cd ${TOOLS_DIR}
./evoting-tb-copy-artefacts.sh -s ${EVOTING_HOME} -d ${DATA_DIR}/${OUTPUT_DIR}${POSTFIX} ${FLATTEN}
