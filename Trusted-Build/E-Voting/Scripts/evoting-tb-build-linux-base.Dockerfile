#
# (c) Copyright 2024 Swiss Post Ltd.
#

FROM eurolinux/centos-stream-9

USER root

RUN yum install glibc.i686 git bzip2 unzip xz openssl -y

COPY ./certs/appl_proxy_ca /etc/pki/ca-trust/source/anchors/appl_proxy_ca.crt
RUN update-ca-trust
ARG JAVA_URL=https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.4%2B7/OpenJDK21U-jdk_x64_linux_hotspot_21.0.4_7.tar.gz
ARG MAVEN_URL=https://archive.apache.org/dist/maven/maven-3/3.9.9/binaries/apache-maven-3.9.9-bin.tar.gz
ARG NODE_URL=https://nodejs.org/dist/v18.20.4/node-v18.20.4-linux-x64.tar.xz
ARG CHROMIUM_URL=https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
ARG WINE_BINARIES_CENTOS9_URL=https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/package_files/48045366/download

#JAVA
RUN mkdir -p /opt/customtools/java && curl -ksSL -o jdk.tar.gz $JAVA_URL && tar xvzf jdk.tar.gz -C /opt/customtools/java && rm jdk.tar.gz
ENV JAVA_HOME="/opt/customtools/java/jdk-21.0.4+7/"

#UPDATE CERTIFICATES
COPY ./certificates /certificates
RUN for c in `find /certificates -type f -name *.pem`; \
       do $JAVA_HOME/bin/keytool -importcert -alias $(basename -s .pem $c) -storepass changeit -keystore $JAVA_HOME/lib/security/cacerts -noprompt -trustcacerts -file ${c}; \
    done

#MAVEN
RUN mkdir -p /opt/customtools/maven && curl -sSL -o maven.tar.gz $MAVEN_URL && tar xvzf maven.tar.gz -C /opt/customtools/maven && rm maven.tar.gz
ENV MAVEN_HOME="/opt/customtools/maven/apache-maven-3.9.9/"

#NODE
RUN mkdir -p /opt/customtools/node && curl -sSL -o node.tar.xz $NODE_URL && tar -xf node.tar.xz -C /opt/customtools/node && rm node.tar.xz
ENV NODE_HOME="/opt/customtools/node/node-v18.20.4-linux-x64/"

#CHROMIUM
RUN curl -sSL -o google-chrome.rpm $CHROMIUM_URL && yum install google-chrome.rpm -y && rm google-chrome.rpm

#WINE
RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then cd /usr && curl -ksSL -o wine-binaries.tar.gz $WINE_BINARIES_CENTOS9_URL \
&& tar xvzf ./wine-binaries.tar.gz && rm wine-binaries.tar.gz; fi

ENV PATH="${JAVA_HOME}bin/:${MAVEN_HOME}bin/:${NODE_HOME}bin:${PATH}"

RUN useradd -m baseuser

#COPY BUILD ENV AND SET RIGHT RIGHTS
COPY ./settings.xml /home/baseuser
COPY ./tools /home/baseuser/tools
COPY ./npmrc  /home/baseuser/.npmrc
RUN chown -R baseuser:baseuser /home/baseuser/
RUN chmod -R +x /home/baseuser/tools/

USER baseuser
WORKDIR /home/baseuser

RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then wineboot; fi
RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then wineserver -d; fi

CMD ["sh", "-c", "tools/evoting-tb-build.sh -t /home/baseuser/tools/ -s /home/baseuser/settings.xml -d /home/baseuser/export"]
