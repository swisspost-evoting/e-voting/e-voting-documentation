
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-component-runnable.jar**  |  1.2.3.0  |  72a947804eff446a75427e1ada4fab448fbc8b17ed01efbe4e71957801cee39a |
| **secure-data-manager-package-1.2.3.0.zip**  |  1.2.3.0  |  4ff22a02dd12f6c258ec4a33aa7e3cdd69f0edab4924ea11e28737297ba87871 |
| **xml-signature-1.2.3.0.jar**  |  1.2.3.0  |  f9fe5e9d9b95bf47ca5894cb3d7d47a317f765f113024ce625bf522075ec3573 |
| **voter-portal-1.2.3.0.zip**  |  1.2.3.0  |  9cf743bc977ba0efc1f5abaa54bcba5c48d4fe53b630560a74b511a821bb5919 |
|    --> crypto.ov-api.min.js  |  1.2.3.0  |  d01621e980c6661f54c5937124bb98a237f9245e0efea5877dbce021ee8a7186 |
|    --> crypto.ov-api.min.js  |  1.2.3.0  |  sha384-LNpOLQxQDyfXuie3N/w5l14A9p1QT9sy0KlEVmQbetvEYN936NOs9j+lMWLiHXFl |
|    --> main.js  |  1.2.3.0  |  c0592a70de15ccd47ceaf008f8ed7f3504d9bbf1d2abf98e6b53d70fd58f3a65 |
|    --> main.js  |  1.2.3.0  |  sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0 |
|    --> polyfills.js  |  1.2.3.0  |  4b448effec426a2c384bd53d1c469bef63618a2fa9eab4a851ecb0e230cae675 |
|    --> polyfills.js  |  1.2.3.0  |  sha384-D5FYZRCy3QcIC4rAKIL6cKa/tZUk4a3yANQVXiMyS4vygoidBFR2ZdVIvBuaWA2T |
|    --> runtime.js  |  1.2.3.0  |  28e7daae6a51b866ffcecbc88fa0c0c51918bd2cee047f0279a18ea979d95dd8 |
|    --> runtime.js  |  1.2.3.0  |  sha384-dlxcMTURbY1Q0kEMB97cLvsa9nDmXfFMjrdjaw/M8L0JQ0HbsmIuJhln++YxFaNI |
| **ag-ws-rest.war**  |  1.2.3.0  |  6dd47af9f896af0e444ab1b929dbcb8bd3a5b1e1136c38ed07911987940f6b8d |
| **au-ws-rest.war**  |  1.2.3.0  |  b30913f2a9744e8aa1e468f4ffe64861f39352116a423f6689c39248b77b5c47 |
| **cr-ws-rest.war**  |  1.2.3.0  |  9b53e36b26d434ca1dd47d3b696e15863ad05e28ff3321c59ddb744a92dd1f22 |
| **ei-ws-rest.war**  |  1.2.3.0  |  00d5f1d801ae894ac486fb39df18fbe31f3510038ef233bdee63bc7259e73003 |
| **ea-ws-rest.war**  |  1.2.3.0  |  d4700ee4332e0d473986d3651b5f9c661e3fbd9790946f1a8a973df2fe1e9371 |
| **message-broker-orchestrator-1.2.3.0.jar**  |  1.2.3.0  |  6982901d164a6e8ab38a99139477984643397ce423e0f7dd79ae0f26caca3a56 |
| **vv-ws-rest.war**  |  1.2.3.0  |  f535ea79c1b10ba01172d9142814b03328cddf79c209b95fd9558c3afd7bb322 |
| **vm-ws-rest.war**  |  1.2.3.0  |  965db3f494b80408093cecb6c57f1e73db17dd9e042901e07490cb8e481a93b2 |
| **vw-ws-rest.war**  |  1.2.3.0  |  d6aef13c7b48a1817753b53de2481aff60892a603a58545e8f0dd40ff9b90047 |