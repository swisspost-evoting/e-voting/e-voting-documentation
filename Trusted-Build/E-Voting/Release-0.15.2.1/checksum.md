
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-components.jar**  |  0.15.2.1  |  e03f66194e06a7c40803982a122311d38cbccb3565b7cb32d883d1352e7f1bea |
| **secure-data-manager-package-0.15.2.1.zip**  |  0.15.2.1  |  ef57509b0be81344232cdcccc9d73cc009e757913d43efbcc153c2baeed9a743 |
| **voter-portal-0.15.2.1.zip**  |  0.15.2.1  |  93d8bb12c98818a22f3eea0a81d5617bde684d86d344d03d8a94c87914094f2e |
|   ** ¦-> voter-portal.js**  |  0.15.2.1  |  d59f1076459ebf315e6f42cf690a49420eca527a0d06a71c8a3b07eb7ef45e02 |
|   ** ¦-> voter-portal-templates.js**  |  0.15.2.1  |  4009fb3b9186ccb8842fe5c4c46740b2a0f9e11789a1b95c567b9f6e4067b092 |
|   ** ¦-> voter-portal-vendors.js**  |  0.15.2.1  |  1f85dacaa008fcfe4e50e0f44d5f036b7f3e45d2c47ae9fe6f7e86c1dd8bccfe |
|   ** ¦-> ov-api.min.js**  |  0.15.2.1  |  feab2bc92e4bbb6d6a87047cd56891a5d208f98d134914ffc0b5f4c9577f0e0b |
| **ag-ws-rest.war**  |  0.15.2.1  |  7e9ab4eb6687cb6589cd2f3ecf25cb3c1f7f3fa8f246ba0a6d5008523931deb7 |
| **au-ws-rest.war**  |  0.15.2.1  |  aed7718235b9aa15a41c10ac7066bdb6ee83b00ace67ed5b814a4e2579f2bed4 |
| **cr-ws-rest.war**  |  0.15.2.1  |  cd68bb4a8ec508bb01f1ee892879da9f31a081910d7437cefa049820019d5bd8 |
| **ei-ws-rest.war**  |  0.15.2.1  |  e60fd6b90af0e7bf5ca355590c31959f892efabdbe7c1c3e29a005541c64e8f0 |
| **ea-ws-rest.war**  |  0.15.2.1  |  91c193a25f7374756a49b0415d9c4a9c0ee880313448852bf7d6a09a129eb707 |
| **message-broker-orchestrator-0.15.2.1.jar**  |  0.15.2.1  |  ef3da5dd8d7f5ae7649a48fb9242cd9d055607775369ed6c88e18a3c1964f5c6 |
| **or-ws-rest.war**  |  0.15.2.1  |  e6aed4fbe15a5303340a20d85be5a97cbfb19096ebe40c2307528eefce4bf508 |
| **vv-ws-rest.war**  |  0.15.2.1  |  e61b8100fc34e926faeb7206670bd79df2689310681dcc8ede5665f14b3c657d |
| **vm-ws-rest.war**  |  0.15.2.1  |  6650d982526af4c47ce3dfb27c3b96bf5695d1a539bd4960624c7d17b47e79a1 |
| **vw-ws-rest.war**  |  0.15.2.1  |  f906ff677354197cee66cb7e20f7a7703f3c59c93dc9806dbe09c7e35f6fecfd |