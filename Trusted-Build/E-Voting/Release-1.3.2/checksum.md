| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **secure-data-manager-package-1.3.2.1.zip**  |  1.3.2.1  |  6e41f46e086ea3dfa8808f616b522c867c9b80ada1d1bdae05908d14e5db448b |
| **config-cryptographic-parameters-tool-1.3.2.1.jar**  |  1.3.2.1  |  40e4458d055041150d2b261a07caabd91d81e0313938bb4803bcf398db4daa50 |
| **xml-signature-1.3.2.1.jar**  |  1.3.2.1  |  2aa885ed2722b53df5356b533b51cb447c26bf2c539bde48618d66fc40795ad3 |
| **control-component-runnable.jar**  |  1.3.2.4  |  a3d1c230ed62bab284cb873266f59c53b37d5e667950746153884d35704bf020 |
| **voter-portal-1.3.2.3.zip**  |  1.3.2.3  |  ecfc0d1053c37840186c1958e55f26005cef4040edfd8af870427eb7b50f45b0 |
|    --> crypto.ov-api.js  |  1.3.2.3  |  2faf9fae8d6bf75cc1ca06aa9e4f5f2ae4f220420e9d047d884b3cac7bdb26a3 |
|    --> crypto.ov-api.js  |  1.3.2.3  |  sha384-7G9+mPIpCYXzspoKVxrbB9gypA8LQN3AcdpKeGkl0JF/fMDXMpwPEuJuy43BX6Eb |
|    --> crypto.ov-worker.js  |  1.3.2.3  |  a93ace783af039704355f598406a032bc10e7511b0bf377e0c57c8042d46d214 |
|    --> crypto.ov-worker.js  |  1.3.2.3  |  sha384-9rQmV63AI0wyZ609/QwVoGxlkp4G8sT9qVn4PAybkowXbn0wrToHdL0cVVhoIxs9 |
|    --> main.js  |  1.3.2.3  |  3376b2ac07ce03c0f23b2abf05e483e251b144445eac064db7c6762a9a59a4e3 |
|    --> main.js  |  1.3.2.3  |  sha384-ANycYNE+9QxDMDLU2bNogXH89gVJ5n8wg05hIK2nshSidx2yQ2svQIyAEQ/YzyxW |
|    --> polyfills.js  |  1.3.2.3  |  34dfb6e7c7123409169c1b505914dcf3a93b020cddf6d920af248aba9325b99a |
|    --> polyfills.js  |  1.3.2.3  |  sha384-ADkau9TRfRJgjPSietYkJtX6HXQO0rHfv3Grxl4mrWOXJStCoIp3NYVehF+jV2tb |
|    --> runtime.js  |  1.3.2.3  |  12aabd163c442aae3e55119869e24e35654310fb3de1ced5aaf68f6400956ef1 |
|    --> runtime.js  |  1.3.2.3  |  sha384-dl5N3uRQoV0Gvcn8R0Iwi5XWok4Hru2MaccA0GUVXpz+8KUybTBAJ88CJCSpQGiR |
| **voting-server-1.3.2.3-runnable.jar**  |  1.3.2.3  |  3dcf16182ae751cc744ce4e73cd6ccf9dca72318ce5763c6f40f79e0e9a782d8 |