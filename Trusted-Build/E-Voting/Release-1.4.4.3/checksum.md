
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-component-runnable.jar**  |  1.4.4.3  |  5d0fd1d2a2f17560237642750abbc2629b397de7ac854774caf71ced59bbf44c |
| **voter-portal-1.4.4.3.zip**  |  1.4.4.3  |  c62cfcc1740aea0de17b8e592b6cf137a6ffd1536a8796070d0c312b0f7413d1 |
|    --> index.html  |  1.4.4.3  |  b0edcd6681b9bec5d70df07ad81c368da8bea49370e41bfe803e2e2537511f7e |
|    --> crypto.ov-api.js  |  1.4.4.3  |  bd32848a0e07e2dccef465d47d4e295ed079be1dbee43f652bec1294fadede48 |
|    --> crypto.ov-api.js  |  1.4.4.3  |  sha384-9alQr57CZH3awteoDnjYkHFhdgE7OQlCh+RWiTSx39sM989KfgIvmYiaB/QEmQ8J |
|    --> crypto.ov-worker.js  |  1.4.4.3  |  bc34108f5bbbd52b04fe433e38ab073be0620669f5e5fd976d1719340f576486 |
|    --> crypto.ov-worker.js  |  1.4.4.3  |  sha384-N0dd7b05OOepa4coJzcxBUhIBIjqWoYrbwH3gV8RPrK4wdKtxR9n5sCU7PzjLRyG |
|    --> main.js  |  1.4.4.3  |  aca3429d0db2d3822d9de1798e1dac872571f2c2ec0c1478fdfd3c58aabaea32 |
|    --> main.js  |  1.4.4.3  |  sha384-SDqdj88SsMSua5v+xcyhWR4lIW/QiUkukp8Q/9E4msUmSPo98xhyvHOE/PRYy6+W |
|    --> polyfills.js  |  1.4.4.3  |  4c6def3377663b9a437cb43a981b4c36edf5cbb78b956fd7ebbe1754d362860f |
|    --> polyfills.js  |  1.4.4.3  |  sha384-ZIVSoFeZ4Rl8BJTsjulnwRKxIPbb9acPMCKPB1hC9gPCGaFd7jusR1B90wXapEKz |
|    --> runtime.js  |  1.4.4.3  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.4.3  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.4.3.zip**  |  1.4.4.3  |  b4e8b54d6f870f5deb9b38122714c3c5b7e2ba76bb149148c0cdd0658ccccebb |
| **voting-server-1.4.4.3-runnable.jar**  |  1.4.4.3  |  465cfc489a49504d4ec84eead1e8fd0c63c850db32e059718431c7765a12a021 |
| **file-cryptor-1.4.4.3-runnable.jar**  |  1.4.4.3  |  9695a5818e1e7d6c9d75218a0753ff34479a382a8dc6fc8cf0c8434e7604b4e1 |
| **direct-trust-tool-1.4.4.3.zip**  |  1.4.4.3  |  2386fff45e66df14eb894ee91303b566ff8abb2174d20e2d28d6cb4c60522cce |
| **xml-signature-1.4.4.3.jar**  |  1.4.4.3  |  72b43477d63cd64ac47381d95300b2c7ab7916ae6448d7aff2959b7028b7fde0 |