
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **voter-portal-1.4.3.1.zip**  |  1.4.3.1  |  84e4a35adf5642d74a4a5777295889d6ea06546b79bc9aec875baa2a9080e118 |
|    --> index.html  |  1.4.3.1  |  8c0e04bbd05f4dacd2e7c33bb3530648309a7b2120f9af81d68b799e47c48c38 |
|    --> crypto.ov-api.js  |  1.4.3.1  |  bd32848a0e07e2dccef465d47d4e295ed079be1dbee43f652bec1294fadede48 |
|    --> crypto.ov-api.js  |  1.4.3.1  |  sha384-9alQr57CZH3awteoDnjYkHFhdgE7OQlCh+RWiTSx39sM989KfgIvmYiaB/QEmQ8J |
|    --> crypto.ov-worker.js  |  1.4.3.1  |  02afbdd5beecdc2e5329c1df72443fa69f443fb8eda2762fa2e67dceb3633c2d |
|    --> crypto.ov-worker.js  |  1.4.3.1  |  sha384-4v/AWGLOeuiktB5MpfVLlvJ+mWTSYH+UutYcS2GRm6UvyPtxeuf1vuYmqvraLblL |
|    --> main.js  |  1.4.3.1  |  7b1baa8d37d560371a35b9ea41adeaab1a7ee78dff0db283930f257af7451111 |
|    --> main.js  |  1.4.3.1  |  sha384-ZdZYowJf6LskiFMxr/vVvPN2WokgMlMvoNOGlOd+Q8iQzBPBo3/5p5VgDNy4M7in |
|    --> polyfills.js  |  1.4.3.1  |  cc870ae271d9fc412e1ca30b5dd14926bbc4012150cf2989b334df7c02556ed6 |
|    --> polyfills.js  |  1.4.3.1  |  sha384-5FgKmq12RDumrub2BWyG7nG16Uhr3/uq/g58n/as3ABNJ449XfcgJAPk5bALy6r6 |
|    --> runtime.js  |  1.4.3.1  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.3.1  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.3.1.zip**  |  1.4.3.1  |  49ecc43612dd970c6c9e9ca23265752e04a4dc254a636ebdb1c7380a2b52fe8e |
| **voting-server-1.4.3.1-runnable.jar**  |  1.4.3.1  |  710d3684a872f5d42b40cde956beac6de52311eecea90708b79141453f36ad07 |
| **file-cryptor-1.4.3.1-runnable.jar**  |  1.4.3.1  |  a769c3cfc0eff4cbdd54858eb1035dd5b8d20bdb24808c3b9548505c48c7b512 |
| **direct-trust-tool-1.4.3.1.zip**  |  1.4.3.1  |  a280a22188febedd2ac0f669f8618d259235550c8c01e3bdd6a39cc06f47fcb2 |
| **xml-signature-1.4.3.1.jar**  |  1.4.3.1  |  702f34b9aacaf4be3efbf246c5562742b2c456833dfd510a72c931e1863c7244 |
| **control-component-runnable.jar**  |  1.4.3.1  |  29dcee5b8ddc161ada15b7c178a902d0b2089056758ca3caf6d426f3c6aad2ad |