
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **direct-trust-tool-1.4.2.1.zip**  |  1.4.2.1  |  1eb91efb4a0d77fee0ffd970c27c5ada719bdbc243a74035260a3a635cbfafbf |
| **xml-signature-1.4.2.1.jar**  |  1.4.2.1  |  ef31b466d77e230f5dc5c956a2da0788adacc49d65ca442cfdcc637774c2af4e |
| **file-cryptor-1.4.2.1-runnable.jar**  |  1.4.2.1  |  34959779e2d50c56532b1a09c31b0a540b9b1fab3672793c35d89a556c164a58 |
| **control-component-runnable.jar**  |  1.4.2.1  |  fa02a3cfdb5439f6ed8fedbe654ba356e72454946b1bfaad8634c81b83080628 |
| **voter-portal-1.4.2.1.zip**  |  1.4.2.1  |  4e8d9a095b77bb1985b0a05f4d9b4a8b13223c3ae126e14b9713aef3780d9d8e |
|    --> index.html  |  1.4.2.1  |  17d8d53953e8aecb97d7f2c760fe0d3aa1036399e2a60114cc3d6d772ead9be4 |
|    --> crypto.ov-api.js  |  1.4.2.1  |  df476703b73981eb6da974ab7b8b210d9f4b3b19b078bae39cf4c2d36e80a5f1 |
|    --> crypto.ov-api.js  |  1.4.2.1  |  sha384-XGDGDSw7ujI8tcLttVm7GkJoWjeBt9Pbzci6w7rETw354wcKKpk344uUv+803PUI |
|    --> crypto.ov-worker.js  |  1.4.2.1  |  3ea5988e3a96b2a77cf26d0a362f259e758d468420ab99e425b0d567a6c999d7 |
|    --> crypto.ov-worker.js  |  1.4.2.1  |  sha384-8GBxVhb5fgN46wLXlUcj5uN//U1DPEXFoLRoHEAvQyGT3jlxe8gvWrNneiVNbApp |
|    --> main.js  |  1.4.2.1  |  12d4b1a7f4ded25297133ee5c5974d3245758c12f3399ecfc6e4fd09aee52d25 |
|    --> main.js  |  1.4.2.1  |  sha384-QXlPR71EF1ynZF3CtEqvP2clHf6DRQJQeUxlUyQSqiWB6RGLgkxM3Z/N16OWDSwc |
|    --> polyfills.js  |  1.4.2.1  |  cc870ae271d9fc412e1ca30b5dd14926bbc4012150cf2989b334df7c02556ed6 |
|    --> polyfills.js  |  1.4.2.1  |  sha384-5FgKmq12RDumrub2BWyG7nG16Uhr3/uq/g58n/as3ABNJ449XfcgJAPk5bALy6r6 |
|    --> runtime.js  |  1.4.2.1  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.2.1  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.2.1.zip**  |  1.4.2.1  |  a88603de79874a601187bb3e4cf1230c656e37147733df1caf48d1a758871f80 |
| **voting-server-1.4.2.1-runnable.jar**  |  1.4.2.1  |  8080b0497430d077aceea812796d2b5f33a5fe6b3cd3bd31f1899c8cd0e34a74 |