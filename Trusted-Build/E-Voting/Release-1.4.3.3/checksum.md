
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-component-runnable.jar**  |  1.4.3.3  |  7e027febb36b1dbd58ada36bcbeddb3839a36289e06824b35c45b2b995a4dbff |
| **voter-portal-1.4.3.3.zip**  |  1.4.3.3  |  8a7e68d5777525fdd90735430cf621f58768826d45c86dc8834efad8e280728d |
|    --> index.html  |  1.4.3.3  |  45ea31153fc7c8760a71fb2bbf513eb716f2b761ac98c0835a057db13c21595a |
|    --> crypto.ov-api.js  |  1.4.3.3  |  bd32848a0e07e2dccef465d47d4e295ed079be1dbee43f652bec1294fadede48 |
|    --> crypto.ov-api.js  |  1.4.3.3  |  sha384-9alQr57CZH3awteoDnjYkHFhdgE7OQlCh+RWiTSx39sM989KfgIvmYiaB/QEmQ8J |
|    --> crypto.ov-worker.js  |  1.4.3.3  |  e7f9fbc177cd4dce2d014633410b4c377b904b967c07073d0ddd674fcf470f5e |
|    --> crypto.ov-worker.js  |  1.4.3.3  |  sha384-U1Z9zf7YVDoC87bmfWT0l5dCPbiveoKkAwu2/uueqAzsvG4FWmiHkXtNcFCwIj4j |
|    --> main.js  |  1.4.3.3  |  7b1baa8d37d560371a35b9ea41adeaab1a7ee78dff0db283930f257af7451111 |
|    --> main.js  |  1.4.3.3  |  sha384-ZdZYowJf6LskiFMxr/vVvPN2WokgMlMvoNOGlOd+Q8iQzBPBo3/5p5VgDNy4M7in |
|    --> polyfills.js  |  1.4.3.3  |  cc870ae271d9fc412e1ca30b5dd14926bbc4012150cf2989b334df7c02556ed6 |
|    --> polyfills.js  |  1.4.3.3  |  sha384-5FgKmq12RDumrub2BWyG7nG16Uhr3/uq/g58n/as3ABNJ449XfcgJAPk5bALy6r6 |
|    --> runtime.js  |  1.4.3.3  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.3.3  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.3.3.zip**  |  1.4.3.3  |  8a3e0dee8fbbffecd67ef0a4202d7a27ef06594d2d5e5bda0b6dde8352096125 |
| **voting-server-1.4.3.3-runnable.jar**  |  1.4.3.3  |  31f4b768e7a970312b41dfa1997bbd73795ab861d64c03fc370e1e1e4f89b9f3 |
| **xml-signature-1.4.3.3.jar**  |  1.4.3.3  |  ecc71f57fd59474d43d8622843723e1fd8ec0e42a57d0295deabb6c242e102ee |
| **file-cryptor-1.4.3.3-runnable.jar**  |  1.4.3.3  |  98b4a74a1bce07f8144a1e6b9f699a2afe30680f284769aa96a8d4c7626f7405 |
| **direct-trust-tool-1.4.3.3.zip**  |  1.4.3.3  |  9d7397d3d904ea82e8a9c5ebc8250a109dbb05b8eb694a72a2184d174d44b048 |