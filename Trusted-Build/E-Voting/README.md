# E-Voting Trusted Build - Scripts

This folder contains all the relevant information needed to perform your own trusted build and compare your hash results with the once from Swiss Post.

Related documentation can be found [here](../Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md).

| Folder | Content | Type of file |
|:-------|:--------|:-------------|
|[Release-x.x.x.x] | Includes hash values, protocols and log files of the trusted build run of the release. | .md, .html, .txt, .pdf |
|[Scripts](Scripts)| Contains all used script files, of the trusted build run, used in the Kubernetes Jenkins environment of Swiss Post. | scripts |
