
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-component-runnable.jar**  |  1.3.1.1  |  dfba6ed45a616598d81ec7d065a92a1f5c3915042903da116ac4e1cf07f68f02 |
| **secure-data-manager-package-1.3.1.1.zip**  |  1.3.1.1  |  25fe7e7c5d164f69770e482fa8ef121132b498dd93e0f94f5bb5382e5f63d6cd |
| **config-cryptographic-parameters-tool-1.3.1.1.jar**  |  1.3.1.1  |  8f5f5c544c0c1d182495581696f591bc61eb238b4da519b1c68158a54dd4961a |
| **xml-signature-1.3.1.1.jar**  |  1.3.1.1  |  b3dcb96975536e0088280a82ce9c5024dd31d2385bf15f964fdf2c930628df93 |
| **voter-portal-1.3.1.1.zip**  |  1.3.1.1  |  e26aebad0ac537dd051309c22d0b33d28cbd4046526ec23420a3639dae6ebbff |
|    --> crypto.ov-api.min.js  |  1.3.1.1  |  0365b5d97f1a4d26077084b9b2f0922b739def8d4d55324619744fe92713fc20 |
|    --> crypto.ov-api.min.js  |  1.3.1.1  |  sha384-NU6xdOrZ140CmzAZ6VAe7rqcgvN8xlMWJaWCeoBkeOQtVKm5oi/p+zXxZeAxa0Qd |
|    --> main.js  |  1.3.1.1  |  a3d695c126bd23d46148d1588b4183f012de7e6776ded1df50998019024f0fc2 |
|    --> main.js  |  1.3.1.1  |  sha384-ffNcXrt1Wj9U03n9BV+C62F/1HfoaSusAwLj0wnbwEPbSeDEtkAQAacSDfKoMwQw |
|    --> polyfills.js  |  1.3.1.1  |  4b448effec426a2c384bd53d1c469bef63618a2fa9eab4a851ecb0e230cae675 |
|    --> polyfills.js  |  1.3.1.1  |  sha384-D5FYZRCy3QcIC4rAKIL6cKa/tZUk4a3yANQVXiMyS4vygoidBFR2ZdVIvBuaWA2T |
|    --> runtime.js  |  1.3.1.1  |  28e7daae6a51b866ffcecbc88fa0c0c51918bd2cee047f0279a18ea979d95dd8 |
|    --> runtime.js  |  1.3.1.1  |  sha384-dlxcMTURbY1Q0kEMB97cLvsa9nDmXfFMjrdjaw/M8L0JQ0HbsmIuJhln++YxFaNI |
| **voting-server-1.3.1.1-runnable.jar**  |  1.3.1.1  |  ad48cd113055216bf2ac5a6a804d83303ef7f27bd7f096797deb96da2a97d57a |