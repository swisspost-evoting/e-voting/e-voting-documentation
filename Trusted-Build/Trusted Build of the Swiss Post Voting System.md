---
layout: article
title: Trusted Build of the Swiss Post Voting System
author: © 2023 Swiss Post Ltd.
date: "2023-06-15"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Trusted Build of the Swiss Post Voting System

## Introduction

The Trusted Build is a reliable and verifiable software compilation to ensure that the executable release is built from reviewed components. Procedural and organizational measures are implemented to meet the requirements of the [Federal Chancellery Ordinance on Electronic Voting (OEV)](https://www.fedlex.admin.ch/eli/cc/2022/336/en). Section "24.3 Reliable and verifiable compilation and deployment” sets out the requirements. This document provides an overview of the Trusted Build implementation, shows the process and contains information about the risk assessment regarding the software build.

The Swiss Post Voting System enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various software components and the system architecture are explained in the [system specification and system architecture documents](../System).

## Trusted Build Implementation

The chapter explains the implementation of the trusted software build. The requirements of the Federal Chancellery Ordinance on Electronic Voting (OEV, 1 July 2022) are simplified for this description. There must be a reliable and verifiable software compilation and proof that the source code in the production environments is the same as the publicly available source code. The following diagram provides an overview and the details of the implementation are stated below.

![Diagram of Trusted Build implementation ](../.gitlab/media/trusted_build/image2.png)

**Reproducible-build**

The reliable and verifiable software compilation evidence uses the reproducible build approach. Each software build creates the same artefact hash values. The involved participants, such as the software development and operation department, the independent observers and the community, can compare and verify the hash values. 

More information on reproducible builds is available here: https://reproducible-builds.org/.

**Publishing the source code**

Publishing the source code of the Swiss Post Voting System is an upstream process. There is no code resynchronization from the source code repository GitLab to Swiss Post internal source code repositories, GIT. That is a security measure regarding manipulations.

**Third-party libraries**

The Swiss Post software development organization relies on Artifactory as a proxy for third-party libraries, which are whitelisted. Using special forensic and test software, existing third-party dependencies are continuously checked for known vulnerabilities and threats.

**Comparing artefact hash values**

For the Swiss Post Voting system, two source code repository environments are operated. There is an internal source code repository environment called GIT and a public named GitLab. The internal code is not accessible to external users; GitLab is used for the source code publication and is accessible to external users.

To verify that the source code is the same, the artefact hash values from the source code repository GIT are compared against the artefact hash values of GitLab and vice-versa.

The comparison must show that the values are identical.

**Comparing Source Code Files - Diff**

One additional measure is a source code file diff compare. The files in the source code repository “GIT” are compared against the source code repository “GitLab” and vice-versa.

The comparison must show that the values are identical.

**Two departments - separation of Duty**

The separation of duty is a security measure. It means that more than one responsible person is required to complete the tasks. For the Trusted Build process, the separation of duty is implemented with two distinct departments, the software development department and the software operation department.

**Independent observers**

The software build is executed by the independent observers individually based on Swiss Post software release information. To ensure that the deployed source code is the one used for production, all parties should receive the same artefact hash values. For this purpose, the created artefact hash values are compared with Swiss Post’s created artefact hash values.

**Acceptance protocol**

An acceptance protocol is created and published for each software release. The protocol contains information about the software release and the results from the comparison of artefact hash values.

**Publication software build scripts**

The operation department uses Jenkins scripts to execute the Trusted Software build. For transparency reasons, the scripts are published on GitLab and available for public scrutiny. More information is provided in the document below in the chapter “Publication of the build scripts”.

## Versions of the Reproducible Build

The build results depend on the operating system. The software artefact hash values are not the same on Windows and Linux-Mac-Unix operating systems due to the different file systems used. 

By default, Swiss Post published the values for the operating system Linux-Max-Unix.

## Source Code Publication

The source code of the Swiss Post Voting System is publicly available as part of a community programme. The disclosure facilitates an in-depth examination and dialogue amongst specialists and our experts. It will enable us to gradually improve the system, by considering findings. Further information on the community program can be found here <https://evoting-community.post.ch/>. This section explains how the source code is synchronized across the different source code repositories for publication.
The software development is only done on the Dev-Branch on Git. The source code merges to the Master Branch are performed after two approvals. The four-eyes principle is applied as a security measure.  The current productive software versions and releases are on the Master Branch. Before the source code is published, the source code is synchronized to the Pre-Publication Branch. The repository serves to anonymize the source code and personal data is removed as a security measure. The graphic illustrates the high-level procedure for the source code publication.

![Overview of source code publication](../.gitlab/media/trusted_build/image3.png)

Once the source code is anonymized on the "Pre-Publication Branch", it will be synchronized to the public source code repository on GitLab. The repository contains the published source code for external access (community).


## Trusted Build Process

This chapter describes the Trusted Build process. The process is executed by two different departments, Software Development and Software Operation. The graphic below illustrates these steps.

![Trusted Build process](../.gitlab/media/trusted_build/image4.png)

The steps are described below.

**Steps in unattended build operated by Software Development department**

Step 1 - Performing the build

- Performing the software build on Git using dedicated scripts on the Jenkins platform
- The artefacts are created for the different operating systems (Linux/Mac and Windows).
- The step is part of the Continuous Integration and Continuous Delivery, CI/CD. More information on CI/CD is available in the document "Software development process of the Swiss Post Voting System".

Step 2 - Building artefact hash values

- During the software build, the artefact hash values are created.
- The hash values are used for the comparison during the observed software build.
- The hash values are compared in step 10 "Compare artefacts hash values".

Step 3 - Anonymize

- The step is part of the preparation for the source code publication.
- Personal information in the source is removed.

Step 4 - Deploy

- Deployment to the different integration environments (Swiss Post and cantons).

Step 5 - Testing and release

- Test management tests the build according to the [Test Concept of the Swiss Post Voting System](../Testing).
- The build is validated by the test management department and by the cantons

*Decision*

- Depending on tests and validation of the build.

If an error occurs

- The build is not released. An investigation and correction are required before the process can be started again.

If the process step is passed

- The build is released for the production system environments and publication.

Steps 6 - Synchronize

- The source code is synchronized across the source code repositories from Git to GitLab.

**Steps in observed build operated by Software Operation department**

The process steps are supervised by independent observers (observed reproducible build). Independent observers are from external companies and supervise the process of validating the results.

Step 7 - Compare source code

- To ensure that the source code is the same for the observed software build, the source code files between Git and GitLab are compared by this command:
  `diff -x .git -W200 -y -q -r --no-dereference <<PATH TO SOURCE (GitLab)>> <<PATH TO CHECK (Git)>>`
- The internal repository Git is compared with the publication repository GitLab.
- The results are documented in the acceptance protocol.

*Decision*

If an error occurs or the comparison fails

- The build is not released. An investigation and correction are required before the process can be started again.

If there is no error and the comparison is successful

- The process continues

Step 8 - Performing the build

- Performing the software build for each operating system (Linux/Mac and Windows).
- The build is performed on GitLab.
- The results are documented in the acceptance protocol.

Step 9 - Building artefacts hash values

- Building the artefact hash values for the comparison.

Step 10 - Compare artefacts hash values

- The artefact hash values are compared to ensure the source code is the same. The list of the controlled artefacts is set out in chapter 6 “Artefact list”.
- The internal repository Git is compared against the publication repository GitLab.
- The hash values must be identical.
- The results are documented in the acceptance protocol.

*Decision*

If an error occurs or the comparison fails

- The build is not released. An investigation and correction are required before the process can be started again.

If there is no error and the comparison is successful

- The build is released for production.

Step 11- Publication artefacts hash values

- Publish the artefact hash values on GitLab and other channels (cantons) as a security measure.

Step 12- Publication acceptance protocol

- Publish the acceptance protocol on GitLab.

**Additional** **information**

Only the build manager has access to the Jenkins Master Service during the build process in step 1 “Performing the build”. Access permissions are controlled via an Active Directory (AD) group. The build manager is added to the AD group by a third person on the day of the Trusted Build execution and is automatically removed from the AD group after the Trusted Build expires.

The build process is carried out on a shielded Jenkins environment within the Kubernetes cluster (see Infrastructure white paper) of the Swiss Post IT organization. Through strict Splunk monitoring, at all build stages, every single step is monitored. These Splunk logs are stored in a read-only area and can be viewed at any time, but can no longer be changed.


## Trusted Build Process Independent Observers

This chapter describes the Trusted Build process, which is carried out by independent observers. The graphic below illustrates these steps.

![Trusted Build process Independent Observers](../.gitlab/media/trusted_build/image6.png)


**Process steps**

Independent observers execute the process steps. The independent observers are from the cantons and external companies.

Step 1- Performing the build

- The build is performed with the source code on GitLab.
- The artefacts are created for the operating systems (Linux/Mac).

Step 2 - Building artefacts hash values

- Building the artefact hash values for the comparison

Step 3 - Compare artefacts hash values

- The artefact hash values are compared to ensure the source code is the same. The list of the controlled artefacts is set out in chapter 6 “Artefact list”.
- The created artefact hash values are compared with the publication repository GitLab.
- The hash values must be identical.
- The results are documented in the acceptance protocol.

*Decision*

If an error occurs or the comparison fails

- An investigation and correction are required before the process can be started again.

If there is no error and the comparison is successful

- The build is released by the independent observers

The following chapter provides an overview of the software artefacts.

## Artefact List

The published artefacts of the Swiss Post Voting System are listed below

### Voter Portal

The role of the Voter Portal is described [here](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/tree/master/voter-portal).

Artefacts:

- voter-portal-[VERSION].zip

Content:

- index.html
- crypto.ov-api.js
- crypto.ov-worker.js
- main.js
- polyfills.js
- runtime.js

### Voting Server

The role of the Voting Server is described [here](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/tree/master/voting-server).

Artefacts:

- voting-server-*-runnable.jar

### Control Components

The role of the Control Components is described [here](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/tree/master/control-components).

Artefacts:

- control-component-runnable.jar

### Secure Data Manager (SDM)

The role of the Secure Data Manger is described [here](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/tree/master/secure-data-manager).

Artefacts:

- secure-data-manager-package-[VERSION].zip

More information on the artefacts is available here: [Swiss Post Voting System Architecture Document](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/SwissPost_Voting_System_architecture_document.pdf)

## Trusted Deployment

This chapter describes the Trusted Deployment of the Voting system. The Trusted Deployment concerns reliable and verifiable deployment with appropriate security measures. A software release is deployed on different system environments such as test or production. The software artefact hash values can then be compared and verified. This verification is done by comparing the hash values obtained by the independent observers (public scrutiny). Identical hash values confirm that the same source code is used. The graphic below illustrates the implementation.

![Overview delivery chain - Deployment](../.gitlab/media/trusted_build/image5.png)

Overview Trusted Deployment

Legend

- SDM = Secure Data Manager
- CC =  Control Components
- GIT / GitLab = Source Code Repository


The following points explain the graphic above

**Build artefacts**

First are the artefacts built for a software release as described in chapter 6, "Trusted Build Process". These artefacts are built from the source code repository "GIT" and are represented with the box "Artefacts". 

**Build Docker images containing artefacts**

Second, Docker images are created containing the artefacts. The images are used in the Kubernetes system environment. The artefacts are stored in the Artefacts Repository and are verified by the Trusted Build procedure. The Docker image allows the software to be run within the image. Instead of distributing all the artefacts individually in different system environments,  only the image is deployed.

**Artefacts Repository**

The artefacts such as the Secure Data Manager SDM, the Control Components CC and Docker Images are stored and deployed in Artifactory.

**Deployment**

Once the artefacts are stored in Artifactory, the deployment to the system environment "development" follows. For the deployment of the stored artefacts, an Ansible script is used. This script is used to build the Docker Images and also deploy the images in different system environments. Tests are executed according to the test concept and procedures for a release. Once the build is released, the same activities are applied for the deployment in other system environments.

**Reproducible Build - compare file hashes against system environments**

To ensure, that the same source code is used and running as published, the artefacts and file hashes are checked. The conducted checks are stated in the graphic and further described below in "Key elements of the Trusted deployment".

**Observed deployment**

The deployment is observed and logged by independent observers. 

**Compare Artefact hash values against different system environments**

To ensure that the same source code is used and running as published, the artefacts and file hashes are checked. 

**Publication of the evidence - Acceptance Protocol**
The evidence stated in the acceptance protocol is publicly available on GitLab.

Further information about the test procedure and change management is available in the documents “[Test Concept of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Testing)” and “[Operation Whitepaper of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Operations)”.

## Acceptance protocol

The acceptance protocol contains the evidence that a Trusted Build for a given software release took place. All hash values of the artefacts and the acceptance protocol are always published on the publication repository [GitLab](../Trusted-Build) as well as on other channels of Swiss Post and the cantons. Publication on multiple channels is one of the security measures in place under the segregation of duties. The storage location of the artefacts, the hash values and the acceptance protocol will be disclosed once released.

## Publication of the build scripts

The scripts used by the operation department to build the software are published on GitLab for transparency reason and are split into four parts.

**evoting-tb-jenkins.groovy**: script  used by Jenkins and set up the Build Job for cloning, comparing and building the Docker Build Image. This Jenkins job is intended to run exclusively in a Jenkins environment set up in a Kubernetes cluster. If you want to run this script in a normal Jenkins environment, some parts have to be modified.

**evoting-tb-build-linux-base.Dockerfile**: This script is the Docker Build script and contains all the parts to create a Linux CentOS Docker Maven build image.

**evoting-tb-build.sh** and **evoting-tb-copy-artefacte.sh**: these 2 shell scripts trigger the Maven build process and compile the artefacts upon successful compilation and determine their hash value.


## Risks and Mitigations

This chapter shows the risk assessment. The table below states the risks and mitigations.

| **No.** | **Risk**                                                     | **Mitigation / Controls**                                    |
| ------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1       | Compromised source code is used. The source code may have been manipulated in the internal source code repository GIT and publication source code repository GitLab | 1. From Git, only after a review of the pull request by a group of people from different organizational units (development & product) is the approval given to publish this code on Gitlab and accept it as the basis for the Trusted Build. The publication on Gitlab takes place after acceptance of this pull request through an automated Jenkins job. Only this job has the right to perform commits on the external Gitlab. |
|         |                                                              | 2. Before the Trusted Build, the source code files between the code published on Gitlab and the code available on the internal Git are compared. If not all files between Gitlab and internal Git match, the build is aborted. A successful attack should therefore be able to manipulate both the sources in Gitlab and on the internal Git. |
| 2       | The Trusted Build is created in a non-secure environment. This can lead to the Trusted Build being manipulated. | 1. The Trusted Build takes place on the production Kubernetes environment that is used for the cantonal ballots. The security measures to protect this environment are described in the [Infrastructure whitepaper of the Swiss Post voting system](Infrastructure whitepaper of the Swiss Post voting system.md). |
|         |                                                              | 2. Due to the possibility that the Trusted Build can be carried out in different environments, the result of the checksums of the artefacts must always be the same. If this is not the case, it can be assumed that the environment used has been changed by manipulation. |
| 3       | Use of manipulated or compressed libraries in the Trusted Build process. | Swiss Post relies on *Artifactory* as a proxy for third-party dependencies and implements a whitelisting process that restricts which dependencies can be used. The use of a new dependency requires an architectural decision. Existing third-party dependencies are checked for vulnerabilities and threats by special forensic and test software. Further information on the tools and processes used can be found in the  [Software Development Process](/Product/Software development process of the Swiss Post voting system.md) document. |
| 4       | The generated artefacts / container images are compromised. This can lead to compromised artefacts / container images being used by the cantons. | Hash over the artefacts / container images are generated and published (SHA sums), so the cantons can check the integrity of the artefacts / containers.  Swiss Post, for its part, also checks the SHA sums as part of the build/deployment process.  The voting participants are also offered the option of also checking the SHA sums. |
| 5       | Artifactory in which the created artefacts / container images are stored is not secure. This can lead to the generated artefacts / container images being compromised. | Before the artefacts are stored in Artifactory, the hashes of these artefacts are logged as evidence of the Trusted Build. These hashes correspond to the hashes that an independent researcher receives when he or she performs a build him or herself (reproducible build). This makes it possible to verify that these hashes are correct for the published code. Before deployment, the artefacts that originate from Artifactory are checked against these hashes. Manipulation during storage in Artifactory can thus be detected. |
| 6       | Risk that the validated infrastructure environment in which the Trusted Build is carried out does not match that which has been subject to prior public inspection and independent verification. | The environments are checked internally by the Swiss Post and by the independent observers before the Trusted Build is executed |

Table of risks and mitigations

## Glossary

The glossary is listed in the table below.

| **Designation**                                              | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Git                                                          | The source code management of the Swiss Post IT Organization |
| [GitLab](https://gitlab.com/swisspost-evoting)               | is a web application for version management. It is used for the public repository of Swiss Post for software projects based on Git, with additional software development functions added later. |
| [Jenkins](https://www.jenkins.io/)                           | is a leading open source automation server, offering hundreds of plugins that support the creation, deployment and automation of any project. |
| [Kubernetes](https://en.wikipedia.org/wiki/Kubernetes)       | Software for managing resources in computer networks         |
| [Artifactory](https://jfrog.com/artifactory/)                | Software for managing software [artefacts](https://en.wikipedia.org/wiki/Artifact_(software_development). |
| [Cluster](https://en.wikipedia.org/wiki/Data_cluster)        | Technology for combining several individual computers into a network |
| [Splunk](https://en.wikipedia.org/wiki/Splunk)               | is a [logging](https://en.wikipedia.org/wiki/Log_file), [monitoring](https://en.wikipedia.org/wiki/Monitoring) and [reporting platform](https://en.wikipedia.org/wiki/Reporting) |
| [Container images](https://en.wikipedia.org/wiki/OS-level_virtualization) | is [free software](https://en.wikipedia.org/wiki/Free_software) for isolating applications using container virtualization. |
| [Version management](https://en.wikipedia.org/wiki/Version_control) | is a system used to record changes to documents or files.    |
| [Source code / Source codes](https://en.wikipedia.org/wiki/Source_code) | In [computer science](https://en.wikipedia.org/wiki/Computer_science) the text of a [computer program](https://en.wikipedia.org/wiki/Computer_programm) written in a [programming language that](https://en.wikipedia.org/wiki/Programming_language)can be read by humans. |
| [Cloning](https://git-scm.com/docs/git-clone/en)             | Imports a repository into a new directory of a whole source code branch. |
| [Pull request](https://de.wikipedia.org/wiki/Pull_Request)[/ merge request](https://en.wikipedia.org/wiki/Distributed_version_control) | In version management, a pull request or merge request is a (web-based) workflow for making [source code changes in](https://en.wikipedia.org/wiki/Source_code)software projects. |
| [Branch / Branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell) | means that you branch off from the main line of development and continue your work without tinkering with the main line. |
| [Artefacts](https://en.wikipedia.org/wiki/Artifact_(software_development)) | is one of the many types of tangible by-products that arise during the development of software. |
| Trusted Build                                                | is a reliable and verifiable software compilation to ensure that the executable release is built from reviewed components. |
| Reproducible Build                                           | A build is reproducible if given the same source code, build environment and build instructions, any party can recreate bit-by-bit identical copies of all specified artefacts. The relevant attributes of the build environment, the build instructions and the source code as well as the expected reproducible artifacts are defined by the authors or distributors. The artefacts of a build are the parts of the build results that are the desired primary output. Source <https://reproducible-builds.org/docs/definition/> |
| Hash (SHA-256, SHA-512)                                      | SHA stands for the secure hash algorithm.                        |