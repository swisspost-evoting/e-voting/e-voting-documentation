#!groovy

/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */


String DIR_SOURCE = "data-integration-service"
String DIR_CHECK = "check"
String DIR_RESOURCES = "resources"
String DIR_TARGET = "target"

pipeline {
    agent any

    parameters {

        credentials(name: 'credential_artifactory', description: 'Artifactory credential', credentialType: "Username with password")
        string(name: 'search_url', description: 'Artifact search URL for hash values compare', defaultValue: '')
        string(name: 'docker_image', description: 'Docker Image for building', defaultValue: '')
        string(name: 'url_docu', description: 'Repository for public documentation', defaultValue: '')
        string(name: 'branch_docu', description: 'Branch for public documentation', defaultValue: 'develop')

        string(name: 'folder_to_remove', defaultValue: 'jenkins', description: 'Path of the folders to remove in both repositories before doing the check, separated by blank space')
        string(name: 'path_to_artefacts', defaultValue: 'export', description: 'Export path of the artefacts files created by the docker image')

        credentials(name: 'credential_id_check', description: 'GITIT credential', credentialType: "Username with password")
        string(name: 'url_check', description: 'GITIT << data-integration-service master >> repository from which get the content to comparing the source', defaultValue: '')
        text(name: 'url_repro_list', defaultValue: 'data-integration-service, \ncrypto-primitives, \ne-voting-libraries, ', description: 'List of repros and tag version in form of << data-integration-service,1.2.0.0 >> or << crypto-primitives,1.1.0.0 >>')

        credentials(name: 'credential_id_source', description: 'GITLAB credential', credentialType: "Username with password")
        string(name: 'url_source', description: 'GITLAB << data-integration-service >> repository from which the build will be done', defaultValue: 'https://gitlab.com/swisspost-evoting/')

    }

    options {
        ansiColor('xterm')
        timestamps()
    }

    environment {
        NO_RELEASE        = getReleaseTag("${params.url_repro_list}")
        TAG_DOCU          = 'feature/trusted_build_dis_${NO_RELEASE}'

        GIT_IT            = credentials("${params.credential_id_check}")
        PWDARTIFACTORY    = credentials("${params.credential_artifactory}")

        WORKDIR           = "${env.WORKSPACE}/dis"
        PATH_SOURCE       = "${WORKDIR}/${DIR_SOURCE}"
        PATH_CHECK        = "${WORKDIR}/${DIR_CHECK}"
        PATH_DOCU         = "${WORKDIR}/docu"
        PATH_DOCU_TB_REL  = "${PATH_DOCU}/Trusted-Build/DIS/Release-${NO_RELEASE}"
        PATH_BUILD_SCRIPTS= "${PATH_DOCU}/Trusted-Build/DIS/Scripts"
        PATH_BUILD_TOOLS  = "${PATH_BUILD_SCRIPTS}/tools"
        PATH_RESOURCES    = "${WORKDIR}/${DIR_RESOURCES}"
        EXPORT_ARCTEFACTE = "${PATH_RESOURCES}/${params.path_to_artefacts}"
    }

    stages {

        stage('Initialization and Workspace cleanup') {
            steps {
                logInfo("Initialization and Workspace cleanup")
                sh 'rm -Rf ${WORKDIR} || true \
                && mkdir -p ${PATH_SOURCE}    \
                && mkdir -p ${PATH_CHECK}     \
                && mkdir -p ${PATH_RESOURCES} \
                && mkdir -p ${PATH_DOCU}      \
                && mkdir -p ${EXPORT_ARCTEFACTE}'

                printJobParameters()
                checkCredentials()
            }
        }

        stage('STEP 7.0 Clone GITIT repository source') {
            steps {
                logInfo("STEP 7.0 Clone GITIT repository source")
                dir("${PATH_CHECK}") {
                    loadRepositories(params.credential_id_check, params.url_check, params.url_repro_list, '')
                    removeUnwantedFiles("${PATH_CHECK}")
                }
            }
        }

        stage('STEP 7.1 Clone GitLab repository source') {
            steps {
                logInfo("STEP 7.1 Clone GitLab repository source")
                dir("${PATH_SOURCE}") {
                    loadRepositories(params.credential_id_source, params.url_source, params.url_repro_list, 'e-voting')
                    removeUnwantedFiles("${PATH_SOURCE}")
                }
            }
        }

        stage('STEP 7.2 Compare GITLAB <=> GITIT') {
            steps {
                logInfo("STEP 7.2 Compare GITLAB <=> GITIT")
                dir("${WORKDIR}") {
                    sh "diff -x .git -W200 -y -q -r --no-dereference ${PATH_SOURCE} ${PATH_CHECK}"
                }
            }
        }

        stage('STEP 8.0 Init builder') {
            steps {
                logInfo("STEP 8.0 Init Maven Docker builder")
                dir("${PATH_DOCU}") { loadRepository(params.credential_id_check, 'clone -qb', params.url_docu, params.branch_docu, ".", "${TAG_DOCU}") }

            }
        }

        stage('STEP 9.0 Run builder') {
            steps {
                logInfo("STEP 9.0 Run Maven Docker builder")
                dir("${WORKDIR}") {
                   podTemplate(yaml: '''
                        apiVersion: v1
                        kind: Pod
                        spec:
                          volumes:
                          - name: "jenkins-home"
                            persistentVolumeClaim:
                              claimName: "jenkins-tb-pvc"
                          - name: "jenkins-config-tb"
                            configMap:
                              name: "jenkins-config"
                          containers:
                          - name: docker
                            image: docker:dind
                            args:
                            - --bip=192.168.200.1/24
                            volumeMounts:
                            - mountPath: "/var/jenkins_home"
                              name: "jenkins-home"
                            - mountPath: "/appl_proxy_ca"
                              name: "jenkins-config-tb"
                              subPath: "appl_proxy_ca"
                            - mountPath: "/maven_settings"
                              name: "jenkins-config-tb"
                              subPath: "maven_settings"
                            - mountPath: "/sed.txt"
                              name: "jenkins-config-tb"
                              subPath: "sed_txt"
                            - mountPath: "/config.json"
                              name: "jenkins-config-tb"
                              subPath: "docker_config_json"
                            securityContext:
                              privileged: true
                            env:
                              - name: DOCKER_TLS_CERTDIR
                                value: ""
                              - name: "https_proxy"
                                valueFrom:
                                  secretKeyRef:
                                    key: "proxy"
                                    name: "proxy-trusted-build"
                                    optional: false
                              - name: "http_proxy"
                                valueFrom:
                                  secretKeyRef:
                                    key: "proxy"
                                    name: "proxy-trusted-build"
                                    optional: false
                              - name: "no_proxy"
                                valueFrom:
                                  secretKeyRef:
                                    key: "no_proxy"
                                    name: "proxy-trusted-build"
                                    optional: false
                              - name: HTTPS_PROXY
                                value: "$(http_proxy)"
                              - name: HTTP_PROXY
                                value: "$(http_proxy)"
                              - name: NO_PROXY
                                value: "$(no_proxy)"
                    ''') {
                         node(POD_LABEL) {
                            container('docker') {
                               sh("""cp /appl_proxy_ca /etc/ssl/certs/ca-certificates.crt \
                                   && cp /sed.txt $WORKDIR/ && mkdir -p ~/.docker/ && cp /config.json ~/.docker/ && cd $WORKDIR \
                                   && docker run -v $PATH_SOURCE:/home/baseuser/data-integration-service --user baseuser --name dis-builder -t $DOCKER_IMAGE \
                                   && docker container cp dis-builder:/home/baseuser/export/dis $EXPORT_ARCTEFACTE \
                                   && chown -R 1000:1000 $EXPORT_ARCTEFACTE """)
                            }
                         }
                    }
                }

            }
        }

        stage('STEP 10.0 Compare artefact hash values') {
            when { expression { return fileExists ("${EXPORT_ARCTEFACTE}/dis/hashes_256.txt") } }
            steps {
                logInfo("STEP 10.0 Compare artefact hash values")
                dir("${PATH_DOCU}") {
                    compareArtefacteHashes("${params.search_url}", "${EXPORT_ARCTEFACTE}/dis/hashes_256.txt", "${PATH_DOCU_TB_REL}", "${NO_RELEASE}", "" )
                }

            }
        }

        stage('STEP 11 Publish artefact and hash values to GitIT') {
            when { expression { return fileExists ("${PATH_DOCU_TB_REL}/checksum.md") } }
            steps {
                logInfo("STEP 11 Publish hash values and build report to GitIT")
                dir("${PATH_DOCU_TB_REL}") {
                    sh "cat ${JENKINS_HOME}/jobs/${JOB_NAME}/builds/${BUILD_NUMBER}/log >> log.txt"
                    ansiLog2Html("${PATH_DOCU_TB_REL}","log.txt", "${WORKDIR}/sed.txt")
					sh "cat ${EXPORT_ARCTEFACTE}/dis/hashes_256.txt >> hashes.txt"
                }
                dir("${PATH_DOCU}") {
                    publishArtefacte("${PATH_DOCU}", "${TAG_DOCU}", "${NO_RELEASE}", "${PATH_DOCU_TB_REL}" )
                }
            }
        }


    }
}

def loadColors() {
    BLACK='\u001b[0;30m'
    RED='\u001b[0;31m'
    GREEN='\u001b[0;32m'
    YELLOW='\u001b[0;33m'
    BLUE='\u001b[0;34m'
    MAGENTA='\u001b[0;35m'
    CYAN='\u001b[0;36m'
    CYAN_L='\u001b[36m'
    WHITE='\u001b[0;37m'

    BLACK_BL='\u001b[1;30m'
    RED_BL='\u001b[1;31m'
    GREEN_BL='\u001b[1;32m'
    YELLOW_BL='\u001b[1;33m'
    BLUE_BL='\u001b[1;34m'
    MAGENTA_BL='\u001b[1;35m'
    CYAN_BL='\u001b[1;36m'
    WHITE_BL='\u001b[1;37m'

    NC='\u001b[0m'
    NCB='\u001b[m'
    NCL='\u001b[0;1m'
    NC_L='\u001b[1m'

}

def loadColorsDict()  {
    LT_GREEN='color:green'
    LT_RED='color:red'
    LT_BLUE='color:blue'
    LT_CYAN='color: #00CDCD;'
    BL_GREEN='color:green;font-weight:bold;'
    BL_RED='color:red;font-weight:bold;'
    BL_BLUE='color:blue;font-weight:bold;'
    BL_YELLOW='color: #CDCD00; font-weight:bold;'
    BL_CYAN='color: #00CDCD; font-weight:bold;'
}

def ansiLog2Html(String path, String logFile, String sedTxt) {

    loadColors()
    loadColorsDict()

    def HTML_TEMPLATE = "<!DOCTYPE html><html><head></head><body style='font-family:arial;font-size:1rem;color:#333f48'><p>%1</p></body></html>"
    def SPAN_TEMPLATE = '<span style="%1">'

    def fileName = "${path}/${logFile}"
    def htmlFile = "${path}/log.html"

    if (fileExists(sedTxt)) {
        def lines = ''
        lines = readFile sedTxt
        sh """sed ${lines} ${fileName}"""
    }

    htext = readFile fileName
    htext = htext.replace("[20", '<p style="font-weight:normal; color:#333f48">[20')
    htext = htext.replace("\n", '</p>')
    htext = htext.replace("${NCB}", '</span>')
    htext = htext.replace("${NCL}", '</span>')
    htext = htext.replace("${NC_L}", '</span>')
    htext = htext.replace("${NC}", '</span>')
    htext = htext.replace("${RED}", SPAN_TEMPLATE.replace('%1',"${LT_RED}" ) )
    htext = htext.replace("${BLUE}", SPAN_TEMPLATE.replace('%1',"${LT_BLUE}" ) )
    htext = htext.replace("${GREEN}", SPAN_TEMPLATE.replace('%1',"${LT_GREEN}" ) )
    htext = htext.replace("${CYAN}", SPAN_TEMPLATE.replace('%1',"${LT_CYAN}" ) )
    htext = htext.replace("${CYAN_L}", SPAN_TEMPLATE.replace('%1',"${LT_CYAN}" ) )
    htext = htext.replace("${CYAN_BL}", SPAN_TEMPLATE.replace('%1',"${BL_CYAN}" ) )
    htext = htext.replace("${RED_BL}", SPAN_TEMPLATE.replace('%1',"${BL_RED}" ) )
    htext = htext.replace("${BLUE_BL}", SPAN_TEMPLATE.replace('%1',"${BL_BLUE}" ) )
    htext = htext.replace("${GREEN_BL}", SPAN_TEMPLATE.replace('%1',"${BL_GREEN}" ) )
    htext = htext.replace("${YELLOW_BL}", SPAN_TEMPLATE.replace('%1',"${BL_YELLOW}" ) )

    writeFile file: htmlFile, text: HTML_TEMPLATE.replace('%1', htext)
}


def logError(message) {
    loadColors()
    sh """set +xe; echo -e "[ ${RED}ERROR${NC} ] $message" """
}

def logPassed(message) {
    loadColors()
    sh """set +xe; echo -e "[ ${GREEN}PASSED${NC} ] $message" """
}

def logInfo(message) {
    loadColors()
    sh """set +xe; echo -e "[ ${BLUE}INFO${NC} ] $message" """
}

String getReleaseTag(String release_list) {
    def list = []
    def values = []
    list = release_list.split('\n')
    values = list[0].split(',')
    String tag = values[1].trim()
    if ( tag.length() == 0 ) {
        error ("No release tag found.")
    }
    logInfo("Release Tag is set to ${tag}")
    return tag
}

void printJobParameters() {
    params.each { key, value -> echo "${key} : ${value}" }
}

void checkCredentials() {
    if (!("${GIT_IT_USR}" ==~ /^[\w-.]+$/)) error("Username for git check contains forbidden charts")
    if (!("${GIT_IT_PSW}" ==~ /^[\w-.]+$/)) error("Token for git check contains forbidden charts")
}

String getUrlWithCredential(String url, String user, String password) {
    String hostName = (url =~ /(?<=http[s]?:\/\/)[^\/]+/)[0]
    return url.replaceFirst("^https://${hostName}", "https://${user}:${password}@${hostName}")
}

void loadRepository(String _credential, String _action, String _url, String _tag, String _path, String _newBranch) {
    if ( _url.length() > 0 ) {
        if (_credential != null && _credential.length() > 0 ) {
            withCredentials([gitUsernamePassword(credentialsId: _credential,
                gitToolName: 'git-tool')]) {
                sh """git $_action $_tag --single-branch $_url $_path"""
            }
        }
        else {
            sh """git $_action $_tag --single-branch $_url $_path"""
        }
    }
    if (_newBranch.length() > 0) {
        sh("""git branch $_newBranch && git checkout $_newBranch""")
    }
}

void loadRepositories(String credential, String base_url, String repositories, String sub_repository) {
    repositories.split('\n').each {
        def values = []
        values = it.split(",")
        String name = values[0].trim()
        String tag = name + '-' + values[1].trim()
        String path = name
        String url = base_url + name + '.git'

            // This is only for GitLab, because the names are different to the internal git
        if ( sub_repository != '' ) {
            if ( path == 'data-integration-service' || path == 'e-voting-libraries') {
                url = base_url + sub_repository + '/' + name + '.git'
            } else {
                url = base_url +  'crypto-primitives/' + name + '.git'
            }
        }
            // Only dis must be checked out to the main folder, all other are sub folders of data-integration-service
        if ( path == 'data-integration-service') {
            path = '.'
        }

        loadRepository(credential, 'clone -qb', url, tag, path, "")
    }

}

void removeUnwantedFiles(String path) {
    String files = "${params.folder_to_remove}".trim()
    if (files) {
        files.split(' ').each {
            sh "for c in `find ${path} -type d -name ${it.trim()}`; do rm -rf " + '${c}; done'
        }
    }
}

void appendFile(String fileName, String line) {
    def current = ""
    if (fileExists(fileName)) {
        current = readFile fileName
    }
    writeFile file: fileName, text: current + "\n" + line
}

void createMDFile(String fileName) {
    if (fileExists(fileName)) {
        sh """rm ${fileName}"""
    }
        // Write an file which is needed as md file.
    appendFile(fileName, "| Artefact     | Version    | Checksum    |")
    appendFile(fileName, "| --------|---------|---------|")
}

void compareArtefacteHashes(String searchUrl, String hashfile, String docuFolder,  String release_no, String excludes) {

    loadColors()

    String fileName = ""
    String hashValue = ""
    def compareValue = ""
    def url = ""
    def lines = []
    def fitems = ""
    def mdFile = "${docuFolder}/checksum.md"

    def folder = new File( "${docuFolder}" )
        // If it doesn't exist
    if( !folder.exists() ) {
            // Create all folders up-to
        folder.mkdirs()
    }

    if (fileExists(hashfile)) {
        def hFile = new File("${hashfile}")

            // If the file exists but the length will be zero, throw an error
        if (hFile.length() > 0 ) {
            lines = hFile.readLines()
            createMDFile(mdFile)
        } else {
            error ("No builded artefact found.")
        }

    } else {
        error ("No builded artefact found.")
    }


    lines.each {
        def checkValues = it.split('  ')
        hashValue    = checkValues[0].trim()
        String aName = checkValues[1].trim().substring(2)

        def artefactName = aName.split("/")
        fileName = artefactName[artefactName.length-1]

        if (fileName != excludes) {

            logInfo("Check hash value::${BLUE}$hashValue${NC}, for artifact::${BLUE}$fileName${NC}")
            fitems = 'items.find( { "repo":"libs-release-evoting-local", "sha256" : "'+hashValue+'" }).include("name","repo","path","sha256")'

        withCredentials([[$class : 'UsernamePasswordMultiBinding',
        credentialsId   : "${params.credential_artifactory}",
        usernameVariable: 'PWDARTIFACTORY_USR',
        passwordVariable: 'PWDARTIFACTORY_PSW']]) {
            url = """-u$PWDARTIFACTORY_USR:$PWDARTIFACTORY_PSW -X POST ${searchUrl} -H 'Content-Type: text/plain' -d '${fitems}'"""
            compareValue = sh(script:"""set +xe; curl $url""", returnStdout: true).trim() as String
        }
            def jObject = readJSON text: compareValue;

            if (jObject.range.total == 0 ) {
                logError("The Artefact << ${RED}${fileName}${NC} >> could not be found in artifactory via the checksum ${hashValue}")
                error ("Error: The Artefact checksum of '${fileName}' is not as expected << ${hashValue}>> get <<${jObject.results[0].sha256}>>.")
            } else {
                logPassed("Identical checksums \n${GREEN}${hashValue}${NC} \n${GREEN}${jObject.results[0].sha256}${NC}")
                appendFile(mdFile, "| **${fileName}**  |  ${release_no}  |  ${hashValue} |")
            }

        }

    }

 }

void publishArtefacte(String pathDocu, String branch, String releaseTag, String newDocuDir) {
    logInfo("Adding new branch:: ${branch} to repository.")
    withCredentials([gitUsernamePassword(credentialsId: "${params.credential_id_check}",
            gitToolName: 'git')]) {
            sh 'git config --global user.email "infopevoting@post.ch" && git config --global user.name "jenkinsevoting"'
            sh "cd ${pathDocu} && git add -A && git commit -m 'DIS Trusted Build release ${releaseTag}' && git push origin ${branch} && git status "

    }
}
