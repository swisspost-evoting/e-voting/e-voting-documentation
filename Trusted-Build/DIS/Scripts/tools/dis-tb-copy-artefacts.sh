#!/bin/bash


# 
# (c) Copyright 2024 Swiss Post Ltd.
# 


set -o errexit

FIND_FILE_SELECTION=" -not -path "*/.m2/*" -not -path ".*/archive-tmp/*" -a
(
-name "data-integration-service-*.zip"
)"

COPY_OPTION="--parents"

die() {
  echo "$(date) [$$] ERROR $*"
  usage
  exit "$1"
}

usage() {
  echo "Usage copy-executables.sh -s <src-dir> -d <dest-dir> [-f]"
  echo "s : source directory"
  echo "d : destination directory"
  echo "f : flatten flag. if set all executables will be copied on destination root directory"
}

load_argument() {
  while getopts :s:d:f option; do
      case "${option}"
      in
        f) COPY_OPTION="" ;;
        s) SOURCE_DIR=${OPTARG} ;;
        d) DESTINATION_DIR=${OPTARG} ;;
    *)
      echo "Unknown option ${OPTARG}. Process is ending..."
      die 1
      ;;
      esac
    done

  if [[ -z "${SOURCE_DIR}" ]]; then
    die 1 's parameter is mandatory'
  fi
  if [[ -z "${DESTINATION_DIR}" ]]; then
    die 1 'd parameter is mandatory'
  fi
}

absolute_directory() {
  if [[ $1 == /* ]]; then
    echo "$1"
  else
    echo "$( cd "$(dirname "${BASH_SOURCE[0]}")/$1" ; pwd -P )"
  fi
}

copy_executables() {
  mkdir -p ${DESTINATION_DIR}
  SOURCE_ABSOLUTE_DIR=$(absolute_directory ${SOURCE_DIR})
  DESTINATION_ABSOLUTE_DIR=$(absolute_directory ${DESTINATION_DIR})

  FILENAME="${DESTINATION_ABSOLUTE_DIR}/hashes_256.txt"
  true > ${FILENAME}

  echo "Copying executables from : ${SOURCE_ABSOLUTE_DIR} to : ${DESTINATION_ABSOLUTE_DIR}"
  # shellcheck disable=SC2091
  $(cd "${SOURCE_ABSOLUTE_DIR}" ; find . \( $FIND_FILE_SELECTION \) -exec cp ${COPY_OPTION} {} ${DESTINATION_ABSOLUTE_DIR} \; -exec sha256sum {} >> ${FILENAME} \;)
}

load_argument "$@"
copy_executables
