# DIS Trusted Build - Scripts 

| Folder | Content | Type of file |
| :------- | :---- | :---- |
| | Includes the used docker file for kubernetes/jenkins to setup the build enviroments | Dockerfile |
|[jenkins](jenkins)| Includes the jenkins pipeline script | groovy |
|[tools](tools)| Includes the bash file for maven and to create the hash values text file | Shell scripts |