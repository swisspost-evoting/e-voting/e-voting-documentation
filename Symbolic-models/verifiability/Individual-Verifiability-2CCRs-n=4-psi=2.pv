(* Properties evaluated in this file: Individual Verifiability. *)
(* This specification models 2 CCRs (Return code control components), 2CCMs (Mixing control components), 4 voting options and voters selecting 2 voting options *)
(* Trust assumptions: *)
(* CCR1 and Alice are trustworthy *)
(* CCR2 and the voting server and the voting client and CCM1 and CCM2 are untrustworthy *)
(* RESULT *)
(* Individual Verifiability property is verified *)
(* i.e. if a voter ends her role (HappyUser(.)) then a ballot containing her intended vote(s)
has been inserted and confirmed by the honest CCR. *)


(* 1. Objects and Types *)
type agent_name. fun t_agent_name(agent_name) : bitstring [data,typeConverter]. (* The type for the voters' names. *)
type vc_id. fun t_vc_id(vc_id) : bitstring [data,typeConverter]. (* The type for the voting card IDs, vcid. *)
type password. fun t_password(password) : bitstring [data,typeConverter]. (* The type for user passwords. *)
type num. fun t_num(num) : bitstring [data,typeConverter]. (* The type for natural numbers. *)
free c : channel. (* Public Channel for communication with the adversary # public *)
free Ch1 : channel. (* Channel between Alice and her (dishonest) voting client # public *)
free SC_CCRChannel : channel. (* Communication between the Setup Component (SC) and the honest CCR # (public) *)
free AliceSCChannel : channel. (* Channel in which Alice (an honest voter) sends her pseudonymous identity to the Setup Component (SC) # (public) *)
free UserChannel : channel. (* Channel in which the Setup Component receives Alice's pseudonymous identity idA # (public) *)
fun v(num) : bitstring [data]. (* The function mapping the voter's selections to voting options # public , invertible *)
free ja1,ja2,ja3,ja4 : num. (* Voting choices # public *)

(* 2 CCRs *)
free CCRId1,CCRId2: num.

fun H1(bitstring) : bitstring. (* Model the hash functions RecursiveHash(.) and  HashAndSquare(.) in the system specification # public , noninvertible *)

(* 2. Adversary capabilities and functions *)

(* Encryption scheme -- ElGamal/ElGamal with multiple encryptions -- Key pairs generated through Gen_e are implicit. *)
type private_ekey. fun t_private_ekey(private_ekey): bitstring [data,typeConverter]. (* The type for private decryption keys + type converter. *)
type public_ekey. fun t_public_ekey(public_ekey) : bitstring [data,typeConverter]. (* The type for public encryption keys + type converter. *)
fun ske(vc_id) : private_ekey [private]. (* The private key associated to a vcid # private *)
fun pube(private_ekey) : public_ekey. (* The function to rebuild a public key from the private # public , noninvertible *)
letfun pke(vcid:vc_id) = pube(ske(vcid)). (* The public encryption key associated to a vcid # public , invertible *)
free kCCR1 : private_ekey [private]. (* Secret key for choice return codes' KDF computations of in the honest CCR *)
free kCCR2: private_ekey. (* Secret key choice return codes'for KDF computations in the dishonest CCR *)
free kCCR1' : private_ekey [private]. (* Secret key for vote cast return codes' KDF computations in the honest CCR *)
free kCCR2': private_ekey. (* Secret key for vote cast return codes' KDF computations in the dishonest CCR *)
free setupSecretKey: private_ekey [private]. (* Secret key used by the Setup Component during the setup process *)
free ELSk1:private_ekey. (* Election decryption key share held by the (dishonest) CCM1 *)
free ELSk2: private_ekey. (* Election decryption key share held by the dishonest CCM2 *)

fun Enc_c1( num) : bitstring. (* The c1 part of the asymmetric encryption function with explicit random number. *)
fun Enc_c2(public_ekey,bitstring,num) : bitstring. (* The c2 part of the asymmetric encryption function with explicit random number. *)
letfun Enc(Pk:public_ekey,M:bitstring,R:num) = (Enc_c1(R),Enc_c2(Pk,M,R)).
reduc forall Sk:private_ekey, M:bitstring, R:num; Dec(Sk,(Enc_c1(R),Enc_c2(pube(Sk),M,R))) = M. (* Decryption *)
reduc forall Pk:public_ekey, M:bitstring, R:num; VerifE(Pk,(Enc_c1(R),Enc_c2(Pk,M,R))) = true. (* Checks key *)
reduc forall vcid:vc_id; Get_Id(pube(ske(vcid))) = vcid. (* Extract vcid - even if this computation is not possible with ElGamal encryptions, it strengthens the attacker capabilities in the model *)

(* Signature used to model messages exchanged between the Setup Component and the CCRs  - see the section on channel security in the system specification*)
type private_skey. fun t_private_skey(private_skey): bitstring [data,typeConverter]. (* The type for private signature keys + type converter. *)
type public_skey. fun t_public_skey(public_skey) : bitstring [data,typeConverter]. (* The type for public signature keys + type converter. *)
fun pubs(private_skey) : public_skey. (* The function to rebuild a public key from the private # public , noninvertible *)
free ABsk : private_skey [private]. (* The private signature key used by the Setup Component - ABsk refers to the administration board secret key*)
fun sign(private_skey,bitstring) : bitstring.
reduc forall Sk:private_skey, M:bitstring; checkSign(pubs(Sk),sign(Sk,M)) = M.

(* Modeling of the voting phase non-interactive zero-knowledge Proofs. *)
(* 'Plaintext Equality Proof' are abstracted inside the model *)
(* Consequently, the c1~ and c2~, which are intermediate values, are not needed anymore and thus abstracted away. *)
fun pCC(private_ekey,bitstring) : bitstring. (* The function to generate partial Choice Return Codes (kID,vi) = vi^kId # public , noninvertible *)
fun tild(private_ekey,bitstring): bitstring. (* To exponentiate ciphertexts generated by the function Enc # public , noninvertible *)
(* Note that pCC() and tild() both model an exponentiation. Two symbols are defined for readability purposes. *)

fun ZKP(public_ekey, public_ekey, bitstring, bitstring, num, private_ekey) : bitstring. (* Modeling of the proof generation *)
reduc forall ELpk:public_ekey, KVCId: public_ekey, J1:num,J2:num, R:num, vcid:vc_id;
    VerifP(ELpk, pube(ske(vcid)), vcid, (Enc_c1(R),Enc_c2(ELpk,(v(J1),v(J2)),R)),(pCC(ske(vcid), v(J1)),pCC(ske(vcid), v(J2))), ZKP(ELpk, pube(ske(vcid)), (Enc_c1(R),Enc_c2(ELpk,(v(J1),v(J2)),R)), (pCC(ske(vcid),v(J1)),pCC(ske(vcid),v(J2))), R, ske(vcid))) = true.

(* Proofs of correct exponentiation produced by the CCRs (needed for generating the Vote Cast Retuurn code VCC) *)
(* ZKPexp((G,m), x, (G^x,m^x), vcid) *)
(* verifyZKPexp((G,m), (G^x,m^x), pi, vcid) *)
const G:bitstring. (* The group generator *)
fun ZKPexp(bitstring,private_ekey,bitstring,vc_id):bitstring.
reduc forall kId:private_ekey, x:bitstring,vcid:vc_id;
  let pi = ZKPexp((G,x),kId,(tild(kId,G),tild(kId,x)),vcid) in
  verifyZKPexp((G,x),(tild(kId,G),tild(kId,x)),pi,vcid) = true.

(* Next follows a series of functions that model modular multiplications that take place at several parts of the protocol *)
(* Different functions names imply that they are applied to different input Types *)
fun mergepk(public_ekey,public_ekey): public_ekey.
fun mergeExpo(bitstring,bitstring):bitstring.

reduc forall sk1: private_ekey, sk2:private_ekey, M: bitstring, R:num; DecMerge(sk1, sk2, (Enc_c1(R), Enc_c2(mergepk(pube(sk1), pube(sk2)), M, R))) = M. (* Reduction rule to model that an attacker is able to decrypt as soon as he knows both parts of the secret key *)

(* Models commutativity of exponentiation and decryption *)
reduc forall sk:private_ekey, kVCId:private_ekey, kId1:private_ekey, kId2:private_ekey, R:num, M:bitstring;
Dec_after_exp(
  sk,
  tild(kId1,(Enc_c1(R), Enc_c2(pube(sk), M, R)))
) =
tild(kId1, M).

(* Symmetric encryption scheme -- AES-GCM *)
const empty_data:bitstring.
type symmetric_ekey. fun t_symmetric_ekey(symmetric_ekey): bitstring [data,typeConverter]. (* The type for symmetric encryption keys. *)
fun Enc_s(symmetric_ekey,bitstring,bitstring) : bitstring. (* Key, Message, Nonce, Authenticated data *)
reduc forall SKey:symmetric_ekey, M:bitstring, Nonce:num, Data:bitstring; Dec_s(SKey,Enc_s(SKey,M,Data),Data) = M. (* Key, Ciphertext, Associated data *)

(* Key and IDs derivation scheme *)
fun deltaId(agent_name) : vc_id. (* The vcid is created for honest voters only by the honest SetupComponent, # private *)
fun deltaKey(password) : symmetric_ekey. (* The delta function, for symmetric encryption keys for keystore # public *)
fun deltaPassword(vc_id): password [private]. (* The delta function to assign passwords (the start voting key SVK) to voters # private *)
fun deltaKeyAgent(private_ekey,vc_id) : private_ekey. (* Key Derivation Function (KDF) used by the CCRs to create kId1, kId2,.... # public *)
fun delta(bitstring) : symmetric_ekey. (* The mask generation function (see the algorithm "KeyDerivationFunction" in the system specification ) # public , noninvertible *)
fun honest(agent_name) : agent_name [private]. (* MODELING : a function to separate honest and dishonest agents (only in Properties) *)

fun rand(bitstring,bitstring):num. (* to sample random nonces when Setup Component encrypts voting options in the Setup phase *)
fun rand2num(bitstring,agent_name): num. (* rand2 and rand2num are used to sample random nonces that are unique per agent *)

(* 3. Initialization sequence (done offline in this modeling). *)
fun bck(agent_name) : bitstring [private]. (* The Ballot Casting Key 'BCK^id' of an agent id # private *)
fun CC(vc_id,bitstring) : bitstring [private]. (* The short choice return code 'CC_{id,k}' of an agent id + voting option k # private *)
fun VCC(vc_id) : bitstring [private]. (* The short vote cast return code 'VCC_id' of an agent id # private *)
fun CCM_table(bitstring) : bitstring [private]. (* The Codes Mapping Table contained CC codes -- written only by the Setup Component (private) but opened by everyone using readCC(). *)
fun VCM_table(bitstring) : bitstring [private]. (* The Codes Mapping Table contained VCC codes -- written only by the Setup Component (private) but opened by everyone using readVCC(). *)
reduc forall VCid:vc_id, J:num, pC:bitstring ;
    readCC( H1(H1((VCid, pC))) , CCM_table(Enc_s( delta( H1((VCid, pC)) ) , CC(VCid,v(J)), empty_data))) =
      Enc_s( delta( H1((VCid, pC)) ) , CC(VCid,v(J)), empty_data ).
reduc forall VCid:vc_id, CMId:bitstring, vCCId: bitstring; (* Enc_s(VCC,VCC) *)
    readVCC( H1(H1((VCid, CMId))) , VCM_table(Enc_s( delta( H1 ((VCid, CMId)) ) , (vCCId, VCC( VCid)), empty_data))) =
      Enc_s( delta( H1((VCid, CMId)) ), (vCCId, VCC(VCid)), empty_data).

(* Registration data for any voter id -- AliceData(svk) produced by the Setup Component for Alice -- opened with a reduction *)
type extended_auth. fun t_extended_auth(extended_auth):bitstring [typeConverter].
fun EA(agent_name):extended_auth [data]. (* This function is inversible because the extended_auth data is a low entropy value *)

fun AliceData(agent_name) : bitstring [private].
reduc forall id_Alice:agent_name, J1:num,J2:num;
    GetAliceData(AliceData(id_Alice),J1,J2) = (id_Alice, EA(id_Alice), deltaId(id_Alice), deltaPassword(deltaId(id_Alice)), bck(id_Alice), VCC(deltaId(id_Alice)), CC(deltaId(id_Alice),v(J1)),CC(deltaId(id_Alice),v(J2))).
(* Note: The code CC(id,v(J)) is the Choice Return Code of the intended vote of Alice. *)

(* Registration data for the Voting Server -- ServData(pke,sks) produced by the Setup Component -- opened with a reduction *)
fun ServData(public_ekey) : bitstring [private].
reduc forall ELpk:public_ekey, id_Voter:agent_name;
  GetServData(ServData(ELpk),id_Voter) = (ELpk, Enc_s(deltaKey(deltaPassword(deltaId(id_Voter))),t_private_ekey(ske(deltaId(id_Voter))),t_public_ekey(ELpk))). (* The election public key is part of the associated data *)

(* 4. Algebraic properties and List of Events *)
event PreConfirmedUser(agent_name,num,num). (* Issued by the voter just before she enters her ballot casting key BCK in the device *)
event HappyUser(bitstring, num,num). (* Issued by the voter when he/she terminates successfully. *)
event SetupComponentCompleted(vc_id).
event InsertBBCCR(vc_id, bitstring). (* Issued by the CCR when it adds something into the ballot box (BB). *)
event BallotConfirmationProcessed(vc_id, bitstring, bitstring). (* Issued by the CCR when it confirms a ballot in the ballot box (BB). *)
event BallotPreConfirmationProcessed(vc_id, bitstring, bitstring). (* Issued by the CCR when it commits to the confirmation of a ballot, i.e. outputs hlVCC_j *)

(* 5. Methods and Agents processes *)

(* CreateVote(ELpk,VCid,Vopt,VCidpk,VCidsk,pkCCRs,R,R'). Voting Client creates the ballot. Algorithm CreateVote in protocol SendVote.
In the symbolic model, the voting client sends the unencrypted partial choice return codes (E2) to the voting server. *)
(* NOTE- This function is not used in the model since the voting client is dishonest and able to forge the ballot. However, we keep it for the sake of readability. Indeed, it defines what is a ballot in this ProVerif model. *)
letfun CreateVote(ELpk:public_ekey,VCid:vc_id,J1:num,KVCId:public_ekey,kVCId:private_ekey) =
    let V = (v(J1)) in new R:num;
    let E1 = Enc(ELpk, V, R) in
    let E2 = (pCC(kVCId,v(J1))) in
    let P = ZKP(ELpk,KVCId,E1,E2, R, kVCId) in
    (E1,E2, tild(kVCId,E1), KVCId, P).

(* ProcessVoteCheck(ELpk,VCid,B,pkCCRs) -- corresponds to the algorithm VerifyBallotCCR of the protocol SendVote *)
letfun ProcessVoteCheck(ELpk:public_ekey,VCid:vc_id,B:bitstring) =
    let (xE1:bitstring, xE2:bitstring, xKVCId: public_ekey, xP:bitstring) = B in
    let Ok1 = VerifP(ELpk,xKVCId, VCid, xE1, xE2, xP) in
    true.

(* Typing of the messages *)
(* These are public tags that are added on top of messages, to indicate from which step they originate. *)
(* They do not affect the executability nor the semantics of the protocol since they are
public, i.e. known by the attacker who can also change them. *)
free mAC0, mAC1, mAC2, mCA1, mCA2 : bitstring.
free ElementMapping, KeyMapping, EncVotingOptions, ExpoVotingOptions, EncBCK, ExpoBCK, VCIdTag, CMTag, VCCidTag, vCCIdTag, E2ExpoTag: bitstring.

table User(agent_name). (* Store the name of the voters *)
event Corrupted(agent_name). (* Identify the malicious voters *)

(* Alice -- The client process *)
let Alice(Ch1:channel,InitData:bitstring,J1:num,J2:num) =
    (* Checks that all voting options are different *)
    if J2 = J1 then 0 else (* No honest voter can use twice the same option *)
    (* Retrieves registration data obtained from the Setup Component -- Set of initial data given to Alice by the Setup Component. *)
    let (id_Alice: agent_name, EAid:extended_auth, VCid: vc_id, SVKid:password, BCKid:bitstring, VCCid:bitstring, CCid1:bitstring,CCid2:bitstring) = GetAliceData(InitData,J1,J2) in
    !out(AliceSCChannel, id_Alice) |

    (* Authentication part *)
    out(Ch1, (mAC0,(VCid,SVKid,EAid)));
    (* Voting part -- The voting process followed by agent Alice *)
    out(Ch1, ( mAC1,(J1,J2)));
    in( Ch1, (=mCA1,(CC_Received1:bitstring,CC_Received2:bitstring)));
    if (CC_Received1=CCid1 && CC_Received2=CCid2) || (CC_Received1=CCid2 && CC_Received2=CCid1) then (* Compares the short Choice Return Codes. *)
    (
    event PreConfirmedUser(id_Alice, J1,J2);
    out(Ch1, ( mAC2,BCKid));
    in( Ch1, (=mCA2,=VCCid)); (* Alice checks the Vote Cast Return Code *)
    event HappyUser(InitData, J1,J2);
    0
    ).


(* `CCRExpoVotingOptions` and `CCRExpoBCK` correspond to algorithm GenEncLongCodeShares of the protocol SetupVoting *)
(* They allow an honest CCR to compute the exponentiation of the received message, to his key. *)
(* It is important to note that the KDF is partially abstracted too.
A CCR is created with two private keys: kCCR1 (resp. kCCR2) to exponentiate c_pCC and
kCCR1' (resp. kCCR2') to exponentiate c_CK *)
(* More precisely: deltaKeyAgent(kCCRi,VCId) = DeriveKey(k'_i, vcid) and
deltaKeyAgent(kCCRi',VCId) = DeriveKey(k'_i, vcid || "confirm") in algorithm GenEncLongCodeShares in the system specification. *)
(* In addition to the exponentiations, proofs of correct exponentiations are produced
when exponentiating c_pCC and c_CK. *)
(* Finally, this function is executed by the Setup Component process, which abstracts
the communications between the Setup Component and the honest CCR.
However, even if the model assumes that the attacker cannot interfere on this channel,
the outputs of the functions are revealed to the attacker to model a read-only channel. *)
letfun CCRExpoVotingOptions(c_PCC:bitstring, VCId:vc_id) =
  let (c_PCC_1:bitstring,c_PCC_2:bitstring,c_PCC_3:bitstring,c_PCC_4:bitstring) = c_PCC in
  let k_id = deltaKeyAgent(kCCR1,VCId) in
  let c_expPCC_1 = (tild(k_id, c_PCC_1)) in
  let c_expPCC_2 = (tild(k_id, c_PCC_2)) in
  let c_expPCC_3 = (tild(k_id, c_PCC_3)) in
  let c_expPCC_4 = (tild(k_id, c_PCC_4)) in

  let zkp_Exp_1 = ZKPexp( (G,c_PCC_1), k_id, (tild(k_id,G),tild(k_id,c_PCC_1)), VCId ) in
  let zkp_Exp_2 = ZKPexp( (G,c_PCC_2), k_id, (tild(k_id,G),tild(k_id,c_PCC_2)), VCId ) in
  let zkp_Exp_3 = ZKPexp( (G,c_PCC_3), k_id, (tild(k_id,G),tild(k_id,c_PCC_3)), VCId ) in
  let zkp_Exp_4 = ZKPexp( (G,c_PCC_4), k_id, (tild(k_id,G),tild(k_id,c_PCC_4)), VCId ) in
  (tild(k_id,G),
    (c_expPCC_1,c_expPCC_2,c_expPCC_3,c_expPCC_4),
    (zkp_Exp_1,zkp_Exp_2,zkp_Exp_3,zkp_Exp_4)).

letfun CCRExpoBCK(c_CK:bitstring,VCId:vc_id) =
  let k'_id = deltaKeyAgent(kCCR1',VCId) in
  let c_expCK = tild(k'_id, c_CK) in
  (tild(k'_id,G),
    c_expCK,
    ZKPexp((G,c_CK),k'_id,(tild(k'_id,G),c_expCK),VCId)).

letfun decrypt_c_expPCC(sk:private_ekey,c_expPCC:bitstring) =
  let (c_expPCC_1:bitstring, c_expPCC_2:bitstring, c_expPCC_3:bitstring, c_expPCC_4:bitstring) = c_expPCC in
  let c_expPCC_1_dec = Dec_after_exp(sk, c_expPCC_1) in
  let c_expPCC_2_dec = Dec_after_exp(sk, c_expPCC_2) in
  let c_expPCC_3_dec = Dec_after_exp(sk, c_expPCC_3) in
  let c_expPCC_4_dec = Dec_after_exp(sk, c_expPCC_4) in
  (c_expPCC_1_dec,c_expPCC_2_dec,c_expPCC_3_dec,c_expPCC_4_dec).

letfun verify_zkp_c_expPCC(K_id:bitstring,vcid:vc_id,c_PCC:bitstring,c_expPCC:bitstring,x_zkpExp:bitstring) =
  let (c_PCC_1:bitstring, c_PCC_2:bitstring, c_PCC_3:bitstring, c_PCC_4:bitstring) = c_PCC in
  let (c_expPCC_1:bitstring, c_expPCC_2:bitstring, c_expPCC_3:bitstring, c_expPCC_4:bitstring) = c_expPCC in
  let (x_zkpExp_1:bitstring, x_zkpExp_2:bitstring, x_zkpExp_3:bitstring, x_zkpExp_4:bitstring) = x_zkpExp in
  if verifyZKPexp((G,c_PCC_1), (K_id,c_expPCC_1), x_zkpExp_1, vcid)
    && verifyZKPexp((G,c_PCC_2), (K_id,c_expPCC_2), x_zkpExp_2, vcid)
    && verifyZKPexp((G,c_PCC_3), (K_id,c_expPCC_3), x_zkpExp_3, vcid)
    && verifyZKPexp((G,c_PCC_4), (K_id,c_expPCC_4), x_zkpExp_4, vcid) then
    true.


table L_lVCC(bitstring). (* The sequence of hhlVCC_id is securely sent
to the CCRs by the honest Setup Component. We model this communication by a table
that contains all the elements. Note that these elements are made public before
they are inserted in the table to let the attacker know them. *)

(* In order to model that the CCR accepts at most one ballot per voter, we consider the following restriction. Only traces satisfying this restriction will be considered by ProVerif. *)
restriction vcid:vc_id, B1,B2:bitstring; event(InsertBBCCR(vcid,B1)) && event(InsertBBCCR(vcid,B2)) ==> B1 = B2.

(* The Choice Return Codes Control Components - CCR_1, CCR_2 - one of which is untrustworthy *)
let CCR(i:num,k:private_ekey, k':private_ekey, SC_CCRChannel:channel, ABpk:public_skey) =
    (** setup phase - setupCCR **)

    (* generate the private/public key pairs *)
    in(SC_CCRChannel, (=VCIdTag, vcid: vc_id));

    (* Compute k_{j,id}/K_{j,id} w.r.t. the control components' keys in the system specification  *)
    let kId = deltaKeyAgent(k,vcid) in
    out(c, tild(kId,G)) ; (* Compute pube(kId) but it uses tild(.,G) for sake of consistency, in particular in proofs of correct exponentiations *)

    (* Compute k'_{j,id}/K'_{j,id} w.r.t. the control components' keys in the system specification  *)
    let kId' = deltaKeyAgent(k',vcid) in
    out(c, tild(kId',G)) ; (* Compute pube(kId') but it uses tild(.,G) for sake of consistency, in particular in proofs of correct exponentiations *)

    (* createCC *)
    (* exponentiate E2 *)

    let ELpk = mergepk(pube(ELSk1), pube(ELSk2)) in

    (* message of the setup process; others are abstracted in the Setup Component process *)
    in(c, lpCCinit:bitstring);
    let (LpCC1:bitstring,LpCC2:bitstring,LpCC3:bitstring,LpCC4:bitstring) = checkSign(ABpk,lpCCinit) in

    (** voting phase **)

    (* The CCR receives a ballot *)
    in(c, B:bitstring);
    let (E1:bitstring,E2:bitstring, KVCId:public_ekey, P:bitstring) = B in

    if ProcessVoteCheck(ELpk, vcid, B) = true then
      (* the PartialDecryptPCCj and DecryptPCCj algorithms are not described since E2 is sent as a plaintext *)
      let (pCC1:bitstring,pCC2:bitstring) = E2 in (* E2 is not encrypted and constrains the pCC as a tuple *)
      let (hpCC1:bitstring,hpCC2:bitstring) = (H1(pCC1), H1(pCC2)) in
      let (lpCC1:bitstring, lpCC2:bitstring) = (H1((vcid,hpCC1)), H1((vcid,hpCC2))) in

      if (lpCC1 = LpCC1 || lpCC1 = LpCC2 || lpCC1 = LpCC3 || lpCC1 = LpCC4)
         && (lpCC2 = LpCC1 || lpCC2 = LpCC2 || lpCC2 = LpCC3 || lpCC2 = LpCC4) then (

        event InsertBBCCR(vcid, B); (* The honest CCR stores in its internal log that it has proceeded the pair (vcid,B) *)
        out(c, (E2ExpoTag, i, (tild(kId,hpCC1),tild(kId,hpCC2)))); (* output of lCCj *)

        (* Process Confirm *)
        in(c, (=CMTag, pCM:bitstring));
        (* W.l.o.g. in what follows, the names of variables are as if the process is executed by CCR1 and waits for data from CCR2 *)
        let lVCC_id_1 = tild(kId', H1(pCM)) in
        let hlVCC_id_1 = H1((vcid,lVCC_id_1)) in
        event BallotPreConfirmationProcessed(vcid, B, hlVCC_id_1);
        out(c, hlVCC_id_1);  (* output of the hlVCC_j,id term *)
        in(c, (hlVCC_id_2:bitstring));  (* input hlVCC_id_j of other CCRs, that is, CCR2 since there are only 2 CCRs in the model *)
        let hhlVCC_id = H1((hlVCC_id_1,hlVCC_id_2)) in
        get L_lVCC(=hhlVCC_id) in (
          event BallotConfirmationProcessed(vcid, B, hhlVCC_id); (* The honest CCR stores in its internal log that it has contributed to the confirmation of the pair (vcid,B) using the hhlVCC "commitment" *)
          out(c, lVCC_id_1)
        ) else 0
      ) else 0
    .

(* Process to model a malicious CCR who is able to claim any ballot as confirmed
as soon as he is able to produce a hhlVCC that occurs in the sequence L_lVCC provided
by the Setup Component *)
let dishonest_CCR_log() =
  in(c, vcid:vc_id);
  in(c, B:bitstring);
  in(c, hlVCC_id_1:bitstring);
  in(c, hlVCC_id_2:bitstring);
  get L_lVCC(=H1((hlVCC_id_1,hlVCC_id_2))) in
    event BallotConfirmationProcessed(vcid, B, H1((hlVCC_id_1,hlVCC_id_2)))
  else 0.


(* Setup Component - trustworthy *)
let SetupComponent(SC_CCRChannel:channel, J1:num,J2:num,J3:num,J4:num) =
    (* the Setup Component reveals the public part of its signature key, i.e. ABpk *)
    out(c, pubs(ABsk));

    (* setup phase *)
    new r: bitstring;
    new r': bitstring;
    new R2:bitstring;

    get User(id_Voter:agent_name) in (* Setup Component receives the voter's pseudonym id *)
    let VCId:vc_id = deltaId(id_Voter) in
    out(SC_CCRChannel, (VCIdTag, VCId));
    let svk:password = deltaPassword(VCId) in

    (* generate the keys *)
    let kVCId: private_ekey = ske(VCId) in
    out(c, pube(kVCId));
    (* encrypt set of voting options *)
    let (r1:num,r2:num,r3:num,r4:num) = (rand(r, v(J1)),rand(r, v(J2)),rand(r, v(J3)),rand(r, v(J4))) in
    let hpCC1 = H1( pCC(kVCId,v(J1))) in
    let hpCC2 = H1( pCC(kVCId,v(J2))) in
    let hpCC3 = H1( pCC(kVCId,v(J3))) in
    let hpCC4 = H1( pCC(kVCId,v(J4))) in
    let c_PCC:bitstring = (Enc(pube(setupSecretKey), hpCC1, r1) ,Enc(pube(setupSecretKey), hpCC2, r2) ,Enc(pube(setupSecretKey), hpCC3, r3) ,Enc(pube(setupSecretKey), hpCC4, r4) ) in

    (* compute the encrypted ballot casting key (BCK) *)
    let BCKId = bck(id_Voter) in
    let hBCKId = H1(bck(id_Voter)) in
    let R = rand2num(r', id_Voter) in
    let c_CK = Enc(pube(setupSecretKey), H1( tild(kVCId,hBCKId) ) , R) in
    let lpCC1 = H1((VCId, hpCC1)) in
    let lpCC2 = H1((VCId, hpCC2)) in
    let lpCC3 = H1((VCId, hpCC3)) in
    let lpCC4 = H1((VCId, hpCC4)) in

    out(c, (EncVotingOptions, c_PCC));
    out(c, (EncBCK, c_CK));
    out(c, sign(ABsk, (lpCC1,lpCC2,lpCC3,lpCC4))); (* for this communication, we model the signature used by the Setup Component when sending messages to the CCRs *)

    (* wait for the exponentiations from all CCRs *)
    let (K_id_1:bitstring,c_expPCC_1:bitstring,y_zkp_Exp_1:bitstring) = CCRExpoVotingOptions(c_PCC,VCId) in (* abstraction of the 'input' of the exponentiation computed by CCR1 (honest) *)
    out(c, c_expPCC_1); (* reveals that this message should have been sent by CCR1 and thus may be known by the attacker spying on the channel *)
    in(c, (=ExpoVotingOptions, =CCRId2, (K_id_2:bitstring,c_expPCC_2:bitstring,y_zkp_Exp_2:bitstring))); (* input of the exponentiation computed by CCR2 (dishonest) *)

    (* Unlike the specification, the SetupComponent decrypts before merging (i.e. computing the product of) the c_expPCC. This is functionally equivalent. *)
    if verify_zkp_c_expPCC(K_id_1,VCId,c_PCC,c_expPCC_1,y_zkp_Exp_1)
      && verify_zkp_c_expPCC(K_id_2,VCId,c_PCC,c_expPCC_2,y_zkp_Exp_2) then
    let (c_expPCC_1_1_dec:bitstring,c_expPCC_1_2_dec:bitstring,c_expPCC_1_3_dec:bitstring,c_expPCC_1_4_dec:bitstring) = decrypt_c_expPCC(setupSecretKey,c_expPCC_1) in
    let (c_expPCC_2_1_dec:bitstring,c_expPCC_2_2_dec:bitstring,c_expPCC_2_3_dec:bitstring,c_expPCC_2_4_dec:bitstring) = decrypt_c_expPCC(setupSecretKey,c_expPCC_2) in

    (* Combine exponentiated ciphertexts of the long Choice Return Codes shares received from the CCRs and decrypt the result. Corresponds to algorithms CombineEncLongCodeShares and GenCMTable of the protocol SetupVoting *)
    let pCId1: bitstring = mergeExpo(c_expPCC_1_1_dec, c_expPCC_2_1_dec) in
    let pCId2: bitstring = mergeExpo(c_expPCC_1_2_dec, c_expPCC_2_2_dec) in
    let pCId3: bitstring = mergeExpo(c_expPCC_1_3_dec, c_expPCC_2_3_dec) in
    let pCId4: bitstring = mergeExpo(c_expPCC_1_4_dec, c_expPCC_2_4_dec) in

    (* Combine exponentiated ciphertexts of the long Vote Cast Return Code shares received from CCRs. Corresponds to algorithm CombineEncLongCodeShares of the protocol SetupVoting *)
    let (K'_id_1:bitstring,c_expCK_1:bitstring,x_zkpExp_1:bitstring) = CCRExpoBCK(c_CK,VCId) in (* abstraction of the 'input' of the exponentiation computed by CCR1 (honest) *)
    out(c, c_expCK_1); (* reveals that this message should have been sent by CCR1 and thus may be known by the attacker spying on the channel *)
    in(c, (=ExpoBCK, =CCRId2,(K'_id_2:bitstring,c_expCK_2:bitstring,x_zkpExp_2:bitstring))); (* input of the exponentiation computed by CCR2 (dishonest) *)

    if verifyZKPexp((G,c_CK),(K'_id_1,c_expCK_1),x_zkpExp_1,VCId) (* check the correct exponentiation proofs produced by the CCRs *)
      && verifyZKPexp((G,c_CK),(K'_id_2,c_expCK_2),x_zkpExp_2,VCId) then
    let lVCC_id_1 = Dec_after_exp(setupSecretKey, c_expCK_1) in
    let hlVCC_id_1 = H1((VCId,lVCC_id_1)) in
    let lVCC_id_2 = Dec_after_exp(setupSecretKey, c_expCK_2) in
    let hlVCC_id_2 = H1((VCId,lVCC_id_2)) in
    let lvCC_id = mergeExpo(lVCC_id_1,lVCC_id_2) in
    let hhlVCC_id = H1((hlVCC_id_1,hlVCC_id_2)) in
    out(c,hhlVCC_id); (* reveal it to the attacker before sending it (i.e. insert in the table) to the CCRs *)
    insert L_lVCC(hhlVCC_id);

    let CMId = mergeExpo(lVCC_id_1, lVCC_id_2) in

    (* Main steps of algorithm GenCMTable in the system specification  *)
    let (lCCId1:symmetric_ekey,lCCId2:symmetric_ekey,lCCId3:symmetric_ekey,lCCId4:symmetric_ekey) = (delta( H1((VCId, pCId1)) ) ,delta( H1((VCId, pCId2)) ) ,delta( H1((VCId, pCId3)) ) ,delta( H1((VCId, pCId4)) ) ) in
    let CCMtable = (CCM_table(Enc_s(lCCId1, CC(VCId,v(J1)), empty_data)) ,CCM_table(Enc_s(lCCId2, CC(VCId,v(J2)), empty_data)) ,CCM_table(Enc_s(lCCId3, CC(VCId,v(J3)), empty_data)) ,CCM_table(Enc_s(lCCId4, CC(VCId,v(J4)), empty_data)) ) in

    let lvCCId:symmetric_ekey = delta( H1((VCId, CMId)) ) in
    let vCCId = VCC(VCId) in
    let VCMtable = VCM_table(Enc_s(lvCCId, (vCCId, VCC(VCId)), empty_data)) in

    (* output of the table *)
    let CCkeys = ( H1(H1((VCId, pCId1))), H1(H1((VCId, pCId2))), H1(H1((VCId, pCId3))), H1(H1((VCId, pCId4))) ) in
    let VCCkey = H1(H1((VCId, CMId))) in
    out(c, (KeyMapping, (CCkeys, VCCkey)));
    out (c, (ElementMapping, (CCMtable, VCMtable)));
    event SetupComponentCompleted(VCId);
    0.


(* 6. Second Phase -- The Tally after all votes are collected. *)

(* No Tally for individual verifiability properties, only for universal verifiability. *)

(* 7. Security properties *)

(* Sanity check *)
(* All the events occuring in premise of a query (and a few more) must be reachable *)
(* ProVerif must not return 'true' i.e. it must return 'false' or at least 'cannot be proved' (because over-approximations) *)
query vcid:vc_id; event(SetupComponentCompleted(vcid)).
query id_alice:agent_name; event(HappyUser(AliceData(id_alice), ja1,ja2)).
query id_Alice:agent_name, B:bitstring, hhlVCC_id:bitstring;
  event(BallotConfirmationProcessed(deltaId(id_Alice), B, hhlVCC_id)).

(* Individual Verifiability Property : if the honest voter is happy then a ballot corresponding to the voter's intention must have been inserted and confirmed in the ballot-box *)
(* Consequently, if the voting choices do not match the voter's intentions, then the voting server cheated. *)
query Id:agent_name, ELpk:public_ekey,
    J1:num,J2:num,j1:num, j2:num,
    E21:bitstring, R1:num, P1,hhlVCC_id:bitstring, VCid:vc_id;
    event(HappyUser(AliceData(honest(Id)), J1,J2)) ==>
    VCid = deltaId(honest(Id))
    && event(InsertBBCCR(VCid, ((Enc_c1(R1),Enc_c2(ELpk,(v(j1),v(j2)),R1)),E21,pube(ske(VCid)),P1)))
    && event(BallotConfirmationProcessed(VCid, ((Enc_c1(R1),Enc_c2(ELpk,(v(j1),v(j2)),R1)),E21,pube(ske(VCid)),P1),hhlVCC_id))
      && ((j1 = J1 && j2 = J2) || (j1 = J2 && j2 = J1)).

(* Extra property: if a ballot occurs as "confirmed" in a well-formed log of a CCR
(including malicious CCR), which is modeled by event BallotConfirmationProcessed, then it corresponds to a ballot sent by a legitimate voter.
In other words, a malicious CCR cannot add dummy ballots in its well-formed log to
abuse the system and make it raise an error.
*)
query id_Alice,id_Alice':agent_name, ELpk:public_ekey,
    J1:num,J2:num,j1:num, j2:num,
    E21:bitstring, R1:num, P1,B:bitstring,
    hhlVCC_id,hlVCC_id_1,hlVCC_id_2,lVCC_id_1,lVCC_id_2:bitstring;
event(BallotConfirmationProcessed(deltaId(id_Alice'), B, hhlVCC_id))
==>
hhlVCC_id = H1((hlVCC_id_1, hlVCC_id_2)) &&
hlVCC_id_1 = H1((deltaId(id_Alice),lVCC_id_1)) &&
hlVCC_id_2 = H1((deltaId(id_Alice),lVCC_id_2)) &&
(
  event(Corrupted(id_Alice))
  || (
    event(BallotPreConfirmationProcessed(deltaId(id_Alice), ((Enc_c1(R1),Enc_c2(ELpk,(v(J1),v(J2)),R1)),E21,pube(ske(deltaId(id_Alice))),P1),hlVCC_id_1))
    && event(PreConfirmedUser(id_Alice,j1,j2))
    && ((j1 = J1 && j2 = J2) || (j1 = J2 && j2 = J1))
  )
).

(* 8. Main process -- initiates the election *)

process
  (* Public output from Setup(..) -- Gives the election's parameters to the adversary. *)
  out(c, (pube(ELSk1),pube(ELSk2))); (* public CCMs' encryption keys *)
  let ELpk = mergepk(pube(ELSk1), pube(ELSk2)) in out(c, ELpk); (* public election key *)
  out(c, pube(setupSecretKey)); (* public Setup Component's encryption key *)
  out (c, (pube(kCCR1),pube(kCCR2))); (* public CCRs' keys *)

  (* The voting server is dishonest *)
  out(c, ServData(ELpk));

  (* Dishonest voter(s) : As many as possible for verifiability. *)
  !(new id:agent_name; event Corrupted(id); insert User(id);  out(c,id); out(c,AliceData(id))) |

  (* Roles for honest voter(s) *)
  !(
  new idH:agent_name;
  insert User(honest(idH));

  (* Gives the honest voter's public data to the adversary *)
  out(c, deltaId(honest(idH))) |

  in(c, (v1:num,v2:num));
  if (v1 = ja1 || v1 = ja2 || v1 = ja3 || v1 = ja4)
     && (v2 = ja1 || v2 = ja2 || v2 = ja3 || v2 = ja4) then
    Alice( Ch1, AliceData(honest(idH)),v1, v2) |
    out(c, ske(deltaId(honest(idH)))) (* the voting device is corrupted *)
  )

  (* The Setup Component is honest *)
  | !SetupComponent(SC_CCRChannel, ja1,ja2,ja3,ja4)

  (* CCR1 is honest *)
  | !CCR(CCRId1, kCCR1, kCCR1', SC_CCRChannel, pubs(ABsk))
  (* note that the material of CCR2 (dishonest) is public, i.e. kCCR2 and kCCR2' are already known by the attacker *)
  | !dishonest_CCR_log()
