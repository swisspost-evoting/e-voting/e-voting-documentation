# Changelog for the Symbolic Models of the Swiss Post Voting System

## Changes in version 1.3.0

Version 1.3.0 aligns the symbolic model to the improvements of the e-voting 1.4 release and models explicitly inconsistent view of control components when mixing and decrypting confirmed votes.

* [Privacy] Added process CCR and set private its corresponding key kCCR’(j,vcid) (i.e. kc_{j,id} in the system specification).
* [Privacy] Added process CCR_consensus to model that a CCR may be forced to include some ballots in list of confirmed ballots during the agreement/consensus protocol (Algorithms SendVoteAgreement and ConfirmVoteAgreement of the system specification).
* [Privacy] Modified process CCM as described above: modeled communications corresponding to the sequence diagram of the MixOnline protocol and modified the input of the mix/dec process. The symbolic proof now models that the CCM does not necessarily tally 3 ballots.
* [Privacy] Expand the proofs of correct mix and decryption to mixes of 1 and 2 ballots.
* [Privacy] Defined in merge_exp only two keys since there are only two CCs in this model.
* [Verifiability] let hpCC = H1(E2) in --> let hpCC = H1(pCC1) (* there E2 = pCC1 since there is no encryption, simplification to ease understanding *)
* [Verifiability] out(c, (E2ExpoTag, i, (tild(kId,H1(pCC1))))); --> out(c, (E2ExpoTag, i, (tild(kId,hpCC)))); (* renaming to ease understanding - strict syntactic equality *)
* Updated to ProVerif 2.05

## Changes in version 1.2.1

Version 1.2.1 contains two small fixes in the voter authentication protocol.

* Added the authentication nonce in alignment to the system specification 1.3.1
* Fixed the name of the fixed string "derAuth" to "dAuth"

## Changes in version 1.2

Version 1.2 of the symbolic models incorporates feedback from the Federal Chancellery's mandated experts.
We want to thank Saša Radomirović, Ioanna Boureanu, and Steve Schneider
from the University of Surrey for their high-quality, constructive remarks.

* Updated the symbolic models of verifiability and privacy to incorporate the new voter authentication protocol.
* Improved the alignment between symbolic models and system specification.

---

## Changes in version 1.1

Version 1.1 of the symbolic models incorporates some of the feedback from the Federal Chancellery's mandated experts (see above)

* Fixed a problem where the symbolic models could not reach the end of the voting process.
* Provided additional explanation on the modeling choices regarding the tally control component and the electoral board key pair and hashes.
* Added a section about the modeling of the auditors and their technical aid (verifier).
* Justified the abstraction of voter authentication from the symbolic models.
* Provided additional explanations on the omission of write-in votes from the symbolic models.
* Provided operational details on ensuring channel security and the corresponding handling of certificates.
* Aligned the explanation about premature results with the computational proof.

---

## Changes in version 1.0

* Implemented the improved cryptographic protocol where the control components determine each voter's confirmation status.
* Added the configuration phase's exponentiation proofs.
* Added the authenticated encryption scheme.
* Improved the generation and registration of voters.