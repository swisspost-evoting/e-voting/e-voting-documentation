# Apple Safari

## Mode de navigation privée dans Safari


1. Cliquez sur **Fichier** pour ouvrir les **Options**.
2. Cliquez sur **Nouvelle fenêtre privée**.
3. Continuez à travailler dans la nouvelle **fenêtre privée**, que vous reconnaissez à la marque **privée**.

**Screenshot Étape: 1 + 2**

![Screenshot Étape: 1 + 2](../img/rules/safari/fr/p1.png)
<br>

**Screenshot Étape: 3**

![Screenshot Étape: 3](../img/rules/safari/fr/p2.png)