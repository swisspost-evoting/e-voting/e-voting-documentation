# Instructions relatives au contrôle des fichiers JavaScript du portail de vote électronique dans le navigateur

## Contrôle de la valeur hash des fichiers JavaScript dans le navigateur

- Pour savoir si les fichiers JavaScript utilisés par le portail de vote électronique officiel n’ont pas été altérés, vous pouvez contrôler les valeurs hash correspondantes. Veuillez noter que seul le JavaScript peut être vérifié automatiquement, mais pas l'intégrité du fichier HTML.
- Si vous procédez à cette vérification avant le vote électronique, vous pouvez exclure toute manipulation ou une falsification (remplacement) des fichiers JavaScript.
- La description suivante se base sur le fichier JavaScript **main.js**, mais elle peut être aussi utilisée pour d’autres fichiers JavaScript.
- Veuillez vous assurer qu'aucun autre fichier JavaScript n'est chargé en dehors des fichiers JavaScript mentionnés dans le protocole **(5 fichiers JavaScript)**.

### Procédure de calcul automatique des valeurs hash par le navigateur (SHA384-Hash)

1. Recherchez dans les protocoles publiés par les cantons et les experts le **SHA384-Hash** des fichiers JavaScript **main.js**. Vous trouverez le lien vers les protocoles sur le portail de vote dans la rubrique **Trusted Build**.

2. Faites un clic droit sur le portail de vote pour ouvrir le code source de la page HTML.
- À la fin du fichier index.html, vous trouverez les balises ``<script>`` correspondantes avec les valeurs hash comme attribut integrity. L’attribut integrity indique au navigateur de vérifier que la valeur hash dans la balise integrity correspond à celle du fichier Javascript.

Example:

```<script src="main.js" type="module" crossorigin="anonymous" integrity="sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0"></script>```

3. Comparez la **valeur hash** affichée dans la balise integrity avec celle indiquée dans les protocoles publiés. Veuillez noter que conformément à la [recommandation W3C](https://www.w3.org/TR/SRI/), le tag `Integrity` affiche la valeur hash SHA-384 au [format Base64](https://www.rfc-editor.org/rfc/rfc4648#section-4), tandis que le SHA-256 calculé manuellement s’affiche habituellement au [format Base16](https://www.rfc-editor.org/rfc/rfc4648#section-8). En outre, vous pouvez vérifier les valeurs hash affichées sur le portail de vote.

**Si les deux chaînes sont identiques, le fichier JavaScript n’a pas été modifié.**

