# Conseils de sécurité

Les électeurs peuvent veiller eux-mêmes au bon déroulement du vote électronique en suivant la procédure conformément aux instructions. Cela implique notamment de comparer les codes de vérification générés avant le vote avec les codes reçus dans le matériel de vote physique. Pour transmettre le vote, il faut saisir le code de confirmation figurant dans le matériel de vote dans l’appareil utilisé. Le code de finalisation, qui s’affiche après la transmission du vote, doit aussi être comparé avec celui qui figure dans le matériel de vote. Cette procédure porte le nom de vérifiabilité individuelle. Elle permet aux électeurs de vérifier si leur vote a été transmis tel quel et s’il a été correctement enregistré dans l’urne électronique.

Les électeurs peuvent en outre procéder à différents contrôles sur l’appareil utilisé afin de s’assurer que leur vote n’est pas manipulé.

Ces différentes mesures sont décrites ci-après.

## Vérification de l´empreinte numérique du certificat

Si vous souhaitez vous assurer que vous vous trouvez sur le portail de vote électronique correct et officiel, vous pouvez vérifier l´empreinte numérique du certificat. De cette manière, vous pouvez exclure que vous vous trouvez sur un site manipulé ou falsifié. Les certificats sont utilisés afin de garantir l’authenticité du serveur web contacté et de chiffrer la liaison de communication avec le serveur web. Ils permettent ainsi de distinguer les sites véritables des sites falsifiés même s’ils se ressemblent ou sont identiques au premier abord. **Veuillez noter que, pour le moment, les appareils iOS ne prennent pas en charge cette fonctionnalité.**

| [![google-chrome](../icons/google-chrome.png)](zert_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](zert_edge.md) | [![firefox](../icons/firefox.png)](zert_firefox.md) | [![apple-safari](../icons/apple-safari.png)](zert_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](zert_chrome.md)           | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)               | [Safari](zert_safari.md)                                     |       |

### Que fais-je si mon navigateur Internet affiche une empreinte numérique erronée?

Vous devriez interrompre le processus de vote et en informer le canton (équipe d’assistance).

Le fait qu’un navigateur affiche une empreinte numérique erronée signifie que le navigateur et la plate-forme de vote ne sont pas directement connectés. L’interruption de connexion n’est dans la plupart des cas pas liée à une tentative d’abus et est au contraire le résultat d’une mesure de protection. En effet, certaines entreprises procèdent par exemple à une telle interruption sur leur réseau entre les ordinateurs de leurs collaborateurs et les sites Internet afin d’éviter la transmission de données potentiellement dangereuses. Les antivirus peuvent également être configurés de cette manière.

## Effacer l’historique de votre navigateur

Si vous souhaitez vous assurer qu’aucune reconstitution de votre bulletin de vote et de son contenu ne puisse être faite sur votre appareil (ordinateur, tablette, smartphone), nous vous recommandons de vider le cache du navigateur après chaque vote. Cliquez sur le logo du navigateur que vous utilisez afin d’accéder aux instructions relatives à la suppression des historiques de navigateur.

| [![google-chrome](../icons/google-chrome.png)](del_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](del_edge.md) | [![firefox](../icons/firefox.png)](del_firefox.md) | [![apple-safari](../icons/apple-safari.png)](del_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](del_chrome.md)           | [Edge](del_edge.md)                                         | [Firefox](del_firefox.md)               | [Safari](del_safari.md)                                     |       |
| [Chrome Mobile](del_chromemobile.md)              |           [Edge Mobile](del_edgemobile.md)                                                           |                    |   [Safari Mobile](del_safarimobile.md)                                     |            |

## Contrôle des valeurs hash (contrôle élargi)

Pour s’assurer que le code HTML et JavaScript sur l’appareil de l’électeur ou de l’électrice exécute bien les opérations prévues et crypte correctement le vote, l’électeur ou l’électrice peut vérifier l’intégrité du code HTML et JavaScript utilisé. Pour ce faire, l’électrice ou l’électeur peut comparer les valeurs hash des fichiers HTML et Javascript concernés avec celles publiées dans les protocoles des cantons et des experts externes ainsi que sur le portail de vote électronique. Veuillez noter que l'intégrité du code HTML ne peut être vérifiée que manuellement.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](hash_browser_automatic.md) |
| ------------------------------------------------------------ |
| [Contrôle automatique (SHA384-Hash)](hash_browser_automatic.md)
| [Contrôle manuel (SHA256-Hash)](hash_browser_manual.md)   

## Utiliser un mode de navigation sans add-on (mode incognito ou privé)

Par mesure de précaution supplémentaire, nous vous recommandons d’utiliser un mode de navigation bloquant par défaut les add-on du navigateur. Sur Chrome ou Microsoft Edge, il s’agit du mode Incognito, sur Internet Explorer du mode InPrivate, et sur Safari du mode Navigation privée. Vous pouvez également contrôler manuellement les add-on et les extensions du navigateur, et les désactiver le cas échéant.

**Veuillez désactiver la fonction de traduction automatique de votre navigateur afin de garantir que les contenus du portail de vote électronique soient affichés correctement.**


| [![google-chrome](../icons/google-chrome.png)](incognito_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](incognito_edge.md) | [![firefox](../icons/firefox.png)](incognito_firefox.md) | [![apple-safari](../icons/apple-safari.png)](incognito_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](incognito_chrome.md)           | [Edge](incognito_edge.md)                                         | [Firefox](incognito_firefox.md)                                   | [Safari](incognito_safari.md)                                    |

## Guide d'affichage et de synchronisation de l'heure système

Pour permettre le vote, nous vous recommandons d'activer la synchronisation de l'heure sur votre appareil.

| [![Microsoft-Edge](../icons/time.png)](timecheck.md) |
| ------------------------------------------------------------ |
| [Guide d'affichage et de synchronisation de l'heure système](timecheck.md)