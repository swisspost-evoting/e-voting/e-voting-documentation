# Microsoft Edge

## Activer le mode InPrivate dans Microsoft Edge


1. Cliquez sur les **"trois points“** ”Paramètres et plus", marqués d'une flèche sur la capture d'écran.
2. Cliquez sur **"Nouvelle fenêtre InPrivate"**.
3. Continuez à travailler dans la nouvelle **fenêtre InPrivate**, que vous reconnaissez à son **fond sombre**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/p1.png)
<br>

**Screenshot Étape: 3**

![Screenshot Étape: 3](../img/rules/edge/de/p2.png)
