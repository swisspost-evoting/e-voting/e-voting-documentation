# Google Chrome

## Activer le mode "navigation privée" dans Google Chrome


1. Cliquez sur les **"trois points“** ”Personnaliser et gérer Google Chrome", indiqués ici par la flèche sur la capture d'écran.
2. Sélectionnez l'option de menu **"Nouvelle fenêtre d'incognito"**.
3. Continuez à travailler dans la nouvelle **fenêtre d'incognito**, que vous reconnaissez à son **fond sombre**.

**Screenshot Étape: 1**

![](../img/rules/chrome/de/1.png)

**Screenshot Étape: 3**

![Screenshot Étape: 3](../img/rules/chrome/de/incognito_chrome.PNG)



