# Microsoft Edge

## Supprimer l’historique du navigateur dans Microsoft Edge


1. Cliquez sur les **"trois points“** ”Paramètres et plus", marqués ici par la flèche dans la capture d'écran.
2. Choisissez l'option de menu **Paramètres**.
3. Cliquez maintenant sur **"Effacer les données de navigation“** ou sur **”Sélectionner les éléments à effacer“** dans la rubrique **”Confidentialité, recherche et services"**.
5) Dans la liste déroulante, sélectionnez au moins la période qui couvre le processus de vote électronique. Sélectionnez par exemple l'entrée **"Dernière heure"**.
6) Cochez les cases "Cookies et autres données du site web" et "Images et fichiers mis en cache" et cliquez sur l'option de menu **Effacer maintenant**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/p1.png)
