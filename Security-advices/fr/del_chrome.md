# Google Chrome

## Supprimer l’historique du navigateur dans Google Chrome



1. Cliquez sur les **"trois points“** ”Personnaliser et gérer Google Chrome", indiqués ici par la flèche sur la capture d'écran.
2. Choisissez l'option de menu **"Paramètres"**.
3. Sélectionnez l'option de menu **"Confidentialité et sécurité"**.


4. Dans ce masque, sous **"Confidentialité et sécurité“**, cliquez sur **”Effacer les données du navigateur"**.
5. Dans la liste déroulante, sélectionnez au moins la période qui couvre le processus de votre vote électronique. Sélectionnez par exemple l'entrée **"Dernière heure"**.
6. Cochez les cases **"Cookies et autres données du site web “** et **”Images et fichiers mis en cache »**.
7. Cliquez sur le bouton de menu **"Effacer les données"**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/chrome/de/1.png)

