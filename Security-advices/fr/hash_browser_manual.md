# Instructions relatives au contrôle des fichiers HTML et JavaScript du portail de vote électronique dans le navigateur

## Contrôle de la valeur hash des fichiers HTML et JavaScript dans le navigateur

- Pour savoir si les fichiers HTML et JavaScript utilisés par le portail de vote électronique officiel n’ont pas été altérés, vous pouvez contrôler les valeurs hash correspondantes.
- Si vous procédez à cette vérification avant le vote électronique, vous pouvez exclure toute manipulation ou une falsification (remplacement) des fichiers HTML et JavaScript.
- La description suivante se base sur le fichier JavaScript **main.js**, mais elle peut être aussi utilisée pour d’autres fichiers JavaScript. Pour vérifier le fichier HTML, vous pouvez afficher le code source de la page en faisant un clic droit dans le navigateur et passer directement à l'étape 5.
- Veuillez vous assurer qu'aucun autre fichier HTML et JavaScript n'est chargé en dehors des fichiers HTML et JavaScript mentionnés dans le protocole **(5 fichiers JavaScript) et un fichier index.html**.

### Procédure de calcul manuel des valeurs hash (SHA256-Hash)

1. Recherchez dans les protocoles publiés par les cantons et les experts le **SHA256-Hash** des fichiers JavaScript **main.js**. Vous trouverez le lien vers les protocoles sur le portail de vote dans la rubrique **Trusted Build**.

2. Démarrez le processus de vote et ouvrez ensuite les **outils de développement** du navigateur comme suit:
   - Appuyez sur la touche **F12** ou
   - Appuyez sur les touches **Ctrl+Shift+I** ou
   - Cliquez sur les **points de suspension**, puis sur **Outils supplémentaires et Outils de développement**

3. Cliquez sur l’onglet **Sources** et sélectionnez le fichier **main.js**:

4. Désactivez la fonction d'impression (si l'icône est bleue, cela signifie qu'elle est active)

![img](../img/rules/general/prettyprint.png)

5. Copiez le contenu du fichier sur n'importe quel site de conversion de hachage SHA256 et faites calculer le hachage **SHA256**.

6. Comparez la **valeur hash** affichée avec celle indiquée dans les protocoles publiés. Veuillez noter que le SHA-256 calculé manuellement s’affiche habituellement au [format Base16](https://www.rfc-editor.org/rfc/rfc4648#section-8), tandis que le Tag `Integrity` indique la valeur hash SHA-384 au [format Base64](https://www.rfc-editor.org/rfc/rfc4648#section-4) conformément à la [recommandation W3C](https://www.w3.org/TR/SRI/).


En outre, vous pouvez vérifier les valeurs hash affichées sur le portail de vote.

**Si les deux chaînes sont identiques, le fichier HTML ou JavaScript n’a pas été modifié.**

