# Apple Safari

## Vérification du certificat


1. Cliquez sur le **symbole du cadenas** dans la barre d'adresse du navigateur, ici marqué d'une flèche sur la capture d'écran.
2. Sélectionnez **"Afficher le certificat"**.
3. Faites défiler jusqu'au bout de la fenêtre jusqu'à ce que vous voyiez **des empreintes digitales** et comparez ce code avec celui qui figure sur votre carte de vote. Il doit être identique.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/edge_zert1.PNG)


**Remarque:** L'adresse web se compose toujours de la manière suivante: ct.evoting.ch, «ct» étant l'abréviation du canton en question.