# Microsoft Edge Mobile

## Supprimer l’historique du navigateur dans Microsoft Edge


1. Cliquez sur les **"trois lignes horizontales"** en bas à droite, marquées d'une flèche sur la capture d'écran.
2. Choisissez l'option de menu **Paramètres**.
3. Dans ce masque, cliquez sur **"Confidentialité, sécurité et services“**, puis sur **”Effacer les données du navigateur"**.
4. Choisissez dans la liste déroulante au moins la période qui couvre le processus de votre vote électronique. Sélectionnez par exemple l'entrée **"Dernière heure"**.
5. Cochez les cases "Cookies et autres données de site web" et "Images et fichiers mis en cache".
6. Cliquez sur l'option de menu **Effacer les données**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/edge_mobile1.png)