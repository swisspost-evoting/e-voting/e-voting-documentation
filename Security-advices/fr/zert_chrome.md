# Google Chrome

## Vérification du certificat


1. Cliquez sur le bouton **"Afficher les informations du site web"**, indiqué par la flèche sur la capture d'écran.
2. Cliquez maintenant sur **"Connexion sécurisée"** dans le menu ouvert.
3. Cliquez sur **"Le certificat est valide"**.
4. Vous voyez maintenant dans la nouvelle fenêtre sous l'onglet **"Généralités"** les empreintes digitales SHA-256. L'empreinte digitale du certificat est importante pour vous. Veuillez comparer cette valeur avec celle indiquée sur votre certificat de vote. Elle doit être identique.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/chrome/de/z1.png)

**Remarque:** L'adresse web se compose toujours de la manière suivante: ct.evoting.ch, «ct» étant l'abréviation du canton en question.