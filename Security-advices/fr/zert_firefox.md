# Firefox

## Vérification du certificat


1. Cliquez sur le **symbole de verrouillage** à côté de la ligne d'adresse, marqué ici par une flèche sur la capture d'écran.
2. Cliquez ensuite sur **"Connexion sécurisée"**.
3. Cliquez sur **"Autres informations"**.
4. Cliquez sur **"Afficher le certificat"** dans le menu **Sécurité**.
5. Comparez la valeur SHA-256 sous le paragraphe **Empreintes digitales** avec celle de votre carte de vote. Elle **doit être identique**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/edge_zert1.PNG)


**Remarque:** L'adresse web se compose toujours de la manière suivante: ct.evoting.ch, «ct» étant l'abréviation du canton en question.