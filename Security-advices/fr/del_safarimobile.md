# Safari Mobile

## Supprimer le cache du navigateur sous iOS


1. Entrez dans les **Réglages**de votre iPhone et sélectionnez **Safari**
2. Sélectionnez **"Effacer historique, données du site"**.
3. Confirmez en cliquant sur «Effacer».

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/safari/mobile/fr/1.png)

**Screenshot Étape: 2**

![Screenshot Étape: 2](../img/rules/safari/mobile/fr/2.png)

**Screenshot Étape: 3**

![Screenshot Étape: 3](../img/rules/safari/mobile/fr/3.png)
