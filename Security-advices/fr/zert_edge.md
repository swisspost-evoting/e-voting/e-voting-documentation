# Instructions pour vérifier l'adresse Web du portail de vote électronique

Vérification de l'empreinte numérique dans le navigateur


## Microsoft Edge

1. Cliquez sur le **symbole de verrouillage** à côté de l'URL pour afficher les informations du site web, marquées ici par la flèche sur la capture d'écran.
2. Cliquez maintenant sur **"Connexion sécurisée"** dans le menu ouvert.
3. Cliquez sur **Symbole de certificat** en haut à droite du menu.
4. Dans le nouveau menu, sous l'onglet **"Généralités"**, vous voyez maintenant les empreintes digitales SHA-256. Ce qui est important pour vous, c'est l'empreinte digitale du certificat. Veuillez comparer cette valeur avec celle indiquée sur votre certificat de vote (vous pouvez ignorer les deux points). Elle doit être identique.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/edge/de/edge_zert1.PNG)


**Remarque:** L'adresse web se compose toujours de la manière suivante: ct.evoting.ch, «ct» étant l'abréviation du canton en question.