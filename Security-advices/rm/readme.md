# Cussegls per la segirezza

Cun exequir il process tenor l'instrucziun pon las votantas ed ils votants controllar sezzas resp. sezs che la votaziun electronica sa spleghia correctamain. En spezial vali da cumparegliar ils codes da controlla ch'èn vegnids generads avant la votaziun cun ils codes da controlla ch'ellas ed els han survegnì sin ils documents da votaziun fisics. Per transmetter lur vusch ston las votantas ed ils votants scriver giu il code da conferma dals documents da votaziun fisics sin l'apparat utilisà. Suenter che la vusch è vegnida transmessa cumpara il code da finalisaziun. Era quel sto correspunder al code da finalisaziun sin ils documents da votaziun fisics. Questa procedura sa numna verifitgabladad individuala. Uschia po mintga votanta e mintga votant controllar, sche sia vusch è vegnida messa en l'urna electronica a moda correcta e senza vegnir manipulada.

Plinavant han las votantas ed ils votants differentas pussaivladads da controllar cun l'apparat utilisà che lur vusch na vegnia betg manipulada.

Sutvart èn descrittas las differentas mesiras.

## Examinar l'impronta dal certificat

Sche Vus vulais controllar che Vus sajas sin il portal dad e-voting correct ed uffizial, pudais Vus examinar l'impronta dal certificat. Uschia pudais Vus excluder dad esser sin ina pagina manipulada u falsifitgada. Ils certificats vegnan utilisads per garantir l'autenticitad dal webserver adressà e per codar la colliaziun da communicaziun cun il server. Uschia pon ins differenziar paginas-web vardaivlas da falsifitgadas, era sch'ellas vesan or a prima vista sumegliant u tuttina. **Vus plaschair da savair che dispositivs iOS actualmain n’adattan betg questa funcziun.**

| [![google-chrome](../icons/google-chrome.png)](zert_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](zert_edge.md) | [![firefox](../icons/firefox.png)](zert_firefox.md) | [![apple-safari](../icons/apple-safari.png)](zert_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](zert_chrome.md)           | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)               | [Safari](zert_safari.md)                                     |       |

### Tge fatsch jau, sche mes navigatur inditgescha in'impronta dal certificat fallada?

Vus duessas interrumper il process da votar ed infurmar. il post responsabel dal Chantun (team da support).

Sch'il navigatur inditgescha in'impronta dal certificat fallada, n'èn il navigatur e la plattafurma per vuschar betg colliads directamain. Per il solit na stat l'interrupziun betg en connex cun in abus, mabain è ina mesira da protecziun. Tschertas interpresas interrumpan per exempel en lur raits la colliaziun tranter ils computers da lur collavuraturs e las plattafurmas d'internet per filtrar datas nuschaivlas en il traffic da datas. Era programs per la protecziun cunter virus pon vegnir utilisads en questa moda ed esser configurads correspundentamain. Sche votants suspectan che lur vusch pudess vegnir legida a moda abusiva, èsi il meglier d'interrumper il process da vuschar e vuschar cun in auter computer u per correspundenza.

## Stizzar la cronologia dal navigatur

Sche Vus vulais far la segira ch'i n'è betg pussaivel da trair eventualas conclusiuns davart Vossas votaziuns ed elecziuns sin l'apparat che Vus avais utilisà (computer, tablet, smartphone), cussegliain nus a Vus da svidar il cache dal navigatur suenter mintga votaziun/elecziun. Cliccai sutvart sin il navigatur che Vus utilisais per vegnir a l'instrucziun co stizzar la cronologia dal navigatur.

| [![google-chrome](../icons/google-chrome.png)](del_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](del_edge.md) | [![firefox](../icons/firefox.png)](del_firefox.md) | [![apple-safari](../icons/apple-safari.png)](del_safari.md) |
| ---------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](del_chrome.md)           | [Edge](del_edge.md)                                         | [Firefox](del_firefox.md)               | [Safari](del_safari.md)                                     |       |
| [Chrome Mobile](del_chromemobile.md)             |           [Edge Mobile](del_edgemobile.md)                                                           |                    |   [Safari Mobile](del_safarimobile.md)                                     |            |

## Controllar las valurs hash (verificaziun extendida)

Per verifitgar ch'il code da HTML e Javascript sin l'apparat da la votanta u dal votant exequeschia l'operaziun previsa e codeschia endretg la vusch po la votanta resp. il votant controllar l'integritad dal code da HTML e Javascript utilisà. La persuna cun dretg da votar po cumparegliar las valurs hash da las datotecas da Javascript pertutgadas cun las valurs publitgadas en ils protocols publics dals chantuns e dals experts externs sco era sin il portal per la votaziun electronica. Vigilai che l'integritad dal code HTML po vegnir controllada be manualmain.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](hash_browser_automatic.md) |
| ------------------------------------------------------------ |
| [Controllar automatica (SHA384-Hash)](hash_browser_automatic.md)
| [Controllar manuala (SHA256-Hash)](hash_browser_manual.md)   

## Utilisar in modus dal navigatur senza add-ons (Incognito u il modus privat)

Sco ulteriura mesira da segirezza recumandain nus a Vus d'utilisar in modus dal navigatur che supprima sco standard add-ons. Tar Chrome e Microsoft Edge è quai il modus "Inkognito", tar l'Internet Explorer "InPrivate" e tar Safari il modus da navigar privat. Vus pudais era verifitgar ed eventualmain deactivar manualmain ils add-ons e las extensiuns dal navigatur.

**Per plaschair deactivai la funcziun da translaziun automatica da Voss navigatur per garantir che il cuntegn dal portal per la votaziun electronica vegnia mussà correctamain.**

| [![google-chrome](../icons/google-chrome.png)](incognito_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](incognito_edge.md) | [![firefox](../icons/firefox.png)](incognito_firefox.md) | [![apple-safari](../icons/apple-safari.png)](incognito_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](incognito_chrome.md)           | [Edge](incognito_edge.md)                                         | [Firefox](incognito_firefox.md)                                   | [Safari](incognito_safari.md)                                  |

## Gida per mussar e sincronisar l'ura dal system

Per pussibilitar il vot, nus recommendain d'activar la sincronisaziun da l'ura en tia apparecchiatura.

| [![Microsoft-Edge](../icons/time.png)](timecheck.md) |
| ------------------------------------------------------------ |
| [Gida per mussar e sincronisar l'ura dal system](timecheck.md)