# Firefox

## Activar il modus privat en Firefox


1. Cliccai sin las **"trais lingias orizontalas"** dal tuttafatg a dretga, marcada qua cun in pal en il monitur.
2. Cliccai sin ina nova **"fanestra privata"**.
3. Vus lavurais vinavant en la nova **fanestra privata** che Vus enconuschais **a la basa stgira**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/firefox/de/1.png)

**Screenshot Pass: 3**

![Screenshot Pass: 3](../img/rules/firefox/de/incognito_firefox.png)
<br>