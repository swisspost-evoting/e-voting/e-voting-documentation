# Google Chrome Mobile

## Stizzar la cronologia dal navigatur en Google Chrome Mobile iOS


1. Cliccar sin la **"trais puncts"* "Adattar ed administrar la crome" qua segna il monitur cun la pitga.
2. Cliccar sin il punct da menu **Protecziun da datas e segirezza**.
3. Tscherni il punct da menu **Extender las datas dal navigatur**.
4. Tscherni en la glista da Dropdown almain quella perioda che cumpiglia il proceder da Vossa votaziun electronica. Tscherni per exempel l'inscripziun **"L'ultima ura"**.
5. Tscherni las chaschettas da controlla "Cookies ed autras datas da la pagina d'internet" sco er "Maletgs e datotecas ch'èn memorisadas en moda intermediara".
6. Cliccar sin il buttun da menus **Stizzar datas**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/chrome/de/chrome_mobile_incognito.png)
