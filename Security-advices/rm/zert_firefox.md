# Firefox

## Verifitgar il certificat


1. Cliccai sin il **"sicona dal chastè"** sper la lingia d'adressat, qua vegn marcà cun la pitga en il monitur.
2. Lura cliccai sin **"Segirar la colliaziun"**.
2. Cliccai sin  **"Ulteriuras infurmaziuns"**.
3. Cliccai sin **"Mussar certificat"** sut il menu **"Segirtad"**.
4. Cumparegliai sut l'alinea **"Fingerabdruck"** la valur SHA-256, cun quella sin Voss attest da votar. **El sto esser identic**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/edge_zert1.PNG)


**Indicaziun:** L’adressa-web sa cumpona adina uschia: ct.evoting.ch, «ct» stat per l’abreviaziun dal chantun correspundent. 
