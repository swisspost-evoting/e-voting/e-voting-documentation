# Safari Mobile

## Stizzar il cache dal navigatur sut iOS


1. Avri sin Voss iPhone las **configuraziuns** e cliccai sin **Safari**
2. Cliccai sin **"Verlauf" e "Websitedaten löschen"**.
3. Confermai l'annunzia.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/safari/mobile/de/1.png)

**Screenshot Pass: 2**

![Screenshot Pass: 2](../img/rules/safari/mobile/de/2.png)

**Screenshot Pass: 3**

![Screenshot Pass: 3](../img/rules/safari/mobile/de/3.png)
