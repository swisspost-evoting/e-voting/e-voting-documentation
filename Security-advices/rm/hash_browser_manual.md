# Instrucziun per controllar las datotecas da HTML e Javascript dal portal per la votaziun electronica en il navigatur

## Controllar la valur hash da las datotecas da HTML e Javascript en il navigatur

- Sche Vus vulais controllar che las datotecas da HTML e Javascript utilisadas dal portal per la votaziun electronica uffizial na sajan betg vegnidas modifitgadas, pudais Vus verifitgar las valurs hash respectivas.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder che las datotecas da HTML e Javascript èn vegnidas manipuladas u falsifitgadas (remplazzadas).
- La descripziun suandanta è basada sin il datotec JavaScript main.js, ma po era vegnir applitgada ad auters datotec JavaScript. Per controllar il datotec HTML, pudais Vus mussar il code funtauna da la pagina cun in clic a dretg en il browser e cuntinuar directamain tar la fasa 5.
- Sche Vus garantis, ultra da las datotecas JavaScript menziunads en il protocol (**5 datotec JavaScript**) ed il datotec index.html, nagins auters datotec JavaScript u HTML vegnan chargiads.

### Proceder cun la calculaziun manuala da las valurs hash (SHA256-Hash)

1. Tschertgai en ils protocols publitgads dals chantuns e dals experts la valur **SHA256-Hash** e la datoteca da javascript **main.js**. Vus chattais las colliaziuns als protocols sin il portal da votaziun sut il titel **Trusted Build**.

2. Cumenzar il process da votaziun e suenter avrais ils **tools da svilup** dal navigatur. Quai pudais Vus far en ina da las suandantas modas:

    - smatgar **F12** sin la tastatura ubain
    - smatgar **Ctrl+Shift+I** sin la tastatura ubain
    - cliccar sin ils **trais puncts**, suenter sin Weitere Tools e la finala sin **Entwicklungstools**

3. Cliccai sin il register **Quellas** e tscherna la datoteca **main.js**

4. Deactivar la funcziun da stampa originala (sch’il simbol è blau, è el activ) 

![img](../img/rules/general/prettyprint.png)

5. Copiar il cuntegn da la datoteca sin ina pagina da convertiziun da HA256 Hash e lascha calcular **SHA256-Hash**

6. Cumparegliai **Hash-Wert** cun la valur hash che vegn mussada en ils protocols publitgads. Resguardai per plaschair che la SHA-256 calculada a maun vegn inditgada usitadamain en il format [Base16](https://www.rfc-editor.org/rfc/rfc4648#section-8), entant ch’il `integrity` Tag tenor la [recumandaziun da W3C](https://www.w3.org/TR/SRI/) inditgescha la valur da basa da la SHA-384 en il format [Base64](https://www.rfc-editor.org/rfc/rfc4648#section-4). En pli pudais Vus era verifitgar las valurs hash che vegnan inditgadas en il portal d'elecziun e da votaziun.

**Sche las duas successiuns da segns èn identicas, n'è la datoteca da Javascript betg vegnida midada.**
