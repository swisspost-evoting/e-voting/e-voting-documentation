# Instrucziun per controllar l'adressa-web dal portal dad e-voting

Examinar l'impronta digitala (fingerprint) en il navigatur

## Microsoft Edge

1. Cliccai sin il simbol dal chastè sper l'URL per mussar las infurmaziuns da la pagina d'internet ch'èn marcadas qua cun la pitga en il monitur.
2. Cliccai ussa sin il menu avert sin "colliaziun è segira".
3. Cliccai sin **Simbol dal certificat** sisum a dretga en la carta da menu.
4. Ussa vesais Vus en il nov menu sut il chavalier "Allgemeine" las improntas da SHA-256. Impurtanta per Vus è l'impronta dal certificat. Cumparegliai per plaschair questa valur cun la valur inditgada sin Voss attest da votar (puncts dubels pon Vus ignorar). El sto esser identic.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/edge_zert1.PNG)


**Indicaziun:** L’adressa-web sa cumpona adina uschia: ct.evoting.ch, «ct» stat per l’abreviaziun dal chantun correspundent. 