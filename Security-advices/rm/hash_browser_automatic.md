# Instrucziun per controllar las datotecas da Javascript dal portal per la votaziun electronica en il navigatur

## Controllar la valur hash da las datotecas da Javascript en il navigatur

- Sche Vus vulais controllar che las datotecas da Javascript utilisadas dal portal per la votaziun electronica uffizial na sajan betg vegnidas modifitgadas, pudais Vus verifitgar las valurs hash respectivas. Vigilai che be il JavaScript po vegnir controllà automaticamain, ma betg l'integritad dal datotec HTML.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder che las datotecas da Javascript èn vegnidas manipuladas u falsifitgadas (remplazzadas).
- La descripziun sutvart vala per la datoteca da Javascript **main.js**. Ins po però era duvrar ella per autras datotecas da Javascript.
- Sche Vus garantis, ultra da las datotecas JavaScript menziunadas en il protocol **(5 datotecas JavaScript)**, na vegnan betg chargiadas autras datotecas da JavaScript. 

### Proceder cun la calculaziun automatica da las valurs hash tras il navigatur (SHA384-Hash)

1. Tschertgai en ils protocols publitgads dals chantuns e dals experts la valur **SHA384-Hash** e la datoteca da javascript **main.js**. Vus chattais las colliaziuns als protocols sin il portal da votaziun sut il titel **Trusted Build**.

2. Avri cun cliccar sin la vart dretga da la mieur en il portal da votaziun il text da funtauna da la pagina HTML.
- A la fin da la datoteca index.html chattais Vus ils scripts tags relevants cun las valurs hash sco attribut "integrity". L'attribut integrity instruescha il navigatur da controllar che la valur hash da l'Integrity Tag correspundia a la valur hash da la datoteca da javascript.

Exemplaric:

```<script src="main.js" type="module" crossorigin="anonymous" integrity="sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0"></script>```

3. Cumparegliai **Hash-Wert** en l'Integrity Tag cun la valur hash che vegn mussada en ils protocols publitgads. Resguardai p.pl. che la valur da las avainas da la SHA-384 vegn inditgada tenor la [recumandaziun W3C](https://www.w3.org/TR/SRI/) dal di `integrity`-Tag en il [format base64](https://www.rfc-editor.org/rfc/rfc4648#section-4), entant che la SHA-256 calculada a maun vegn inditgada usitadamain en il format [base16](https://www.rfc-editor.org/rfc/rfc4648#section-8). En pli pudais Vus verifitgar las valurs hash che vegnan inditgadas en il portal d'elecziun e da votaziun.

**Sche las duas successiuns da segns èn identicas, n'è la datoteca da HTML u Javascript betg vegnida midada.**
