# Google Chrome

## Stizzar la cronologia dal navigatur en Google Chrome


1. Cliccar sin la **"trais puncts"* "Adattar ed administrar la crome" qua segna il monitur cun la pitga.
2. Tscherni il punct da menu **"Pretaziuns"**.
3. Tscherna il punct da menu **"Protecziun da datas e segirezza"**.


4. Cliccai ussa en questa mascra sut **"Protecziun da datas e segirezza"** sin **"Stizzar datas da navigatur"**.
5. Tscherni en la glista da drropdown almain quella perioda che cumpiglia il proceder da Vossa votaziun electronica. Tscherni per exempel l'inscripziun **"L'ultima ura"**.
6. Tscherner las chaschas da controlla **"Cookies ed autras paginas d'internet"** sco er **"Maletgs e datotecas ch'èn arcunadas tranteren"**.
7. Cliccar sin la tasta da menu **"Stizzar datas"**

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/chrome/de/1.png)
