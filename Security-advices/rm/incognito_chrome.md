# Google Chrome

## Activar il modus navigaziun privata en Google Chrome


1. Cliccar sin la **"trais puncts"** "Adattar ed administrar la crome" qua segna il monitur cun la pitga.
2. Tscherna il punct da menu **"Fanestra da l'incognito"*.
3. Vus lavurais vinavant en la nova **"fanestra d' incognito"** che Vus enconuschais vi da la cuverta stgira.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/chrome/de/1.png)

**Screenshot Pass: 3**

![Screenshot Pass: 3](../img/rules/chrome/de/incognito_chrome.PNG)



