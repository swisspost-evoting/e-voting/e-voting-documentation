# Apple Safari

## Modus da navigar privat a Safari


1. Cliccar sin il deposit** per avrir las **opziuns**.
2. Cliccai sin ** Nova fanestra privata**.
3. Vus lavurais vinavant en la nova **Fanestra** che Vus percorschais per la marcaziun **Privat**.

**Screenshot Pass: 1 + 2**

![Screenshot Pass: 1 + 2](../img/rules/safari/it/p1.png)
<br>

**Screenshot Pass: 3**

![Screenshot Pass: 3](../img/rules/safari/it/p2.png)