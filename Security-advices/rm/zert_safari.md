# Apple Safari

## Verifitgar il certificat


1. Cliccai sin il **simbol dal chastè** en il browser en la lingia d'adressa, marcai cun in pal en il maletg dal monitur.
2. Eleger **In tschertificat**.
3. Scrollar fin a la fin da la fanestra fin che Vus vesais e cumparegliais quest code cun quel sin Voss attest da votar. **El sto esser identic**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/edge_zert1.PNG)


**Indicaziun:** L’adressa-web sa cumpona adina uschia: ct.evoting.ch, «ct» stat per l’abreviaziun dal chantun correspundent. 
