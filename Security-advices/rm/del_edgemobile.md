# Microsoft Edge Mobile

## Stizzar la cronologia dal navigatur en Microsoft Edge


1. Cliccar sin la **"trais lingias orizontalas"** dal tuttafatg sut a dretga, qua cun ina pitga en il monitur.
2. Tscherni il punct da menu **Pretaziuns.**
3. Cliccai ussa en questa mascra sut **"Protecziun da datas, segirezza e servetschs"** sin **"Stizzar datas da navigatur"**.
4. Tscherni en la glista da Dropdown almain quella perioda che cumpiglia il proceder da Vossa votaziun electronica. Tscherni per exempel l'inscripziun **"L'ultima ura"**.
5. Tscherni las chaschettas da controlla "Cookies ed autras datas da la pagina d'internet" sco er "Maletgs e datotecas ch'èn memorisadas en moda intermediara".
6. Cliccar sin il punct da menu **Stizzar las datas**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/edge_mobile1.png)