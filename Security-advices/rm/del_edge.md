# Microsoft Edge

## Stizzar la cronologia dal navigatur en Microsoft Edge


1. Cliccar sin las **"trais puncts"** "Pretaisas e dapli", qua vegn marcà cun la pitga en il monitur.
2. Tscherni il punct da menu **Pretaziuns.**
3. Cliccai ussa en questa mascra sut **"Protecziun da datas, retschertga e servetschs"** sin **"Stizzar las datas da navigatur"**, respectivamain sin **"Tscherner elements che duain vegnir stizzads"**.
5. Tscherni en la glista da drropdown almain quella perioda che cumpiglia il proceder da Vossa votaziun electronica. Tscherni per exempel l'inscripziun **"L'ultima ura"**.
6. Tscherni la chascha da controlla "Cookies ed autras datas da l'internet" sco er "Miletgs e datotecas ch'èn memorisadas tranteren" e cliccai sin il punct da menu **Ussa stizzar**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/p1.png)