# Microsoft Edge

## Browserverlauf löschen in Microsoft Edge

1. Klicken Sie auf die **"drei Punkte"** "Einstellungen und mehr", hier im Screenshot mit dem Pfeil markiert.
2. Wählen Sie den Menüpunkt **Einstellungen.**
3. Klicken Sie nun in dieser Maske unter **"Datenschutz, Suche und Dienste"** auf **"Browserdaten löschen"**, beziehungsweise auf **"Zu löschende Elemente auswählen"**.
5. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
6. Wählen Sie die Kontrollkästchen "Cookies und andere Websitedaten" sowie "Zwischengespeicherte Bilder und Dateien" an und klicken Sie auf den Menüpunkt **Jetzt löschen**.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/p1.png)


