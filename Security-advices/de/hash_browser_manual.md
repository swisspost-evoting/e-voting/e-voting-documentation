# Anleitung zur manuellen Prüfung der HTML und JavaScript-Dateien des E-Voting Portals im Browser

## Prüfung des Hash-Wertes der HTML und JavaScript-Dateien im Browser

- Wenn Sie überprüfen möchten, ob sich die vom offiziellen E-Voting-Portal verwendeten HTML und JavaScript-Dateien in unverändertem Zustand befinden, können Sie die jeweiligen Hash-Werte überprüfen.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass die HTML und JavaScript-Dateien manipuliert oder gefälscht (ersetzt) worden sind.
- Die folgende Beschreibung erfolgt anhand der JavaScript-Datei **main.js**, kann aber auch für andere JavaScript-Dateien verwendet werden. Um die HTML Datei zu prüfen, können Sie im Browser mittels Rechtsklick den Seitenquelltext anzeigen und direkt mit Schritt 5 beginnen.
- Bitte stellen Sie sicher, dass ausser den im Protokoll aufgeführten JavaScript Dateien **(5 JavaScript-Dateien)** und der index.html Datei, keine anderen JavaScript und HTML Dateien geladen werden.

### Vorgehen mit manueller Berechnung der Hashwerte (SHA256-Hash)

1. Suchen Sie in den publizierten Protokollen der Kantone und der Experten den **SHA256-Hash** der JavaScript-Dateien **main.js**. Sie finden den Link zu den Protokollen auf dem Abstimmungsportal unter der Überschrift **Trusted Build**.

2. Starten Sie den Abstimmungsprozess und öffnen Sie dann die **Entwicklungstools** des Browsers auf eine der folgenden Arten:
- Drücken Sie **F12** auf der Tastatur oder
- Drücken Sie **Ctrl+Shift+I** auf der Tastatur oder
- Klicken Sie auf die **Auslassungspunkte**, dann auf **Weitere Tools** gefolgt von **Entwicklungstools**

3. Klicken Sie auf das Register **Quellen** und wählen Sie die Datei **main.js**

4. Deaktivieren Sie die Schöndruck-Funktion (wenn das Symbol blau ist, dann ist es aktiv)

![img](../img/rules/general/prettyprint.png)

5. Kopieren Sie den Inhalt der Datei auf eine beliebige SHA256-Hash-Konvertierungsseite und lassen Sie den **SHA256-Hash** berechnen

6. Vergleichen Sie den angezeigten **Hash-Wert** mit dem Hash-Wert der in den publizierten Protokollen angezeigt wird. Bitte beachten Sie, dass der manuell berechnete SHA-256 üblicherweise im [Base16-Format](https://www.rfc-editor.org/rfc/rfc4648#section-8) angezeigt wird, während der `integrity`-Tag gemäss der [W3C-Empfehlung](https://www.w3.org/TR/SRI/) den SHA-384-Hashwert im [Base64-Format](https://www.rfc-editor.org/rfc/rfc4648#section-4) anzeigt. Zusätzlich können sie auch die Hashwerte überprüfen, die auf dem Wahl- und Abstimmungsportal angezeigt sind.

**Sind die beide Zeichenfolgen identisch, ist die HTML oder JavaScript-Datei nicht verändert worden.**