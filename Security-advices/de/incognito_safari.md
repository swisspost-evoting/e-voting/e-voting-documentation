# Apple Safari

## Privater Surfmodus in Safari

1. Klicken Sie auf die **Ablage** um die **Optionen** zu öffnen.
2. Klicken Sie auf **Neues privates Fenster**.
3. Arbeiten Sie im neuen **privaten Fenster** weiter, welches Sie an der Markierung **Privat** erkennen.

**Screenshot Schritt: 1 + 2**

![Screenshot Schritt: 1 + 2](../img/rules/safari/de/p1.png)
<br>

**Screenshot Schritt: 3**

![Screenshot Schritt: 3](../img/rules/safari/de/p2.png)
