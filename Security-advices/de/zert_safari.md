# Apple Safari

## Überprüfung Zertifikat



1. Klicken Sie auf das **Schloss-Symbol** im Browser in der Adresszeile, hier im Screenshot mit einem Pfeil markiert.
2. Wählen Sie **"Zertifikat einblenden"**.
3. Scrollen Sie bis ans Ende des Fensters bis Sie **Fingerabdrücke** sehen und vergleichen Sie diesen Code mit demjenigen auf Ihrem Stimmrechtsausweis. Er muss identisch sein.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/edge_zert1.PNG)


**Hinweis:** Die Webadresse setzt sich stets wie folgt zusammen: ct.evoting.ch, wobei «ct» für das Kürzel des jeweiligen Kantons steht.