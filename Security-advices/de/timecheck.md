# Anleitung zur Anzeige und Synchronisierung der Systemzeit 


## Allgemein:
Um die Stimmabgabe zu ermöglichen, empfehlen wir die Aktivierung der Zeitsynchronisierung auf Ihrem Gerät.
Untenstehend finden Sie Kurzanleitungen für verschiedene Betriebssysteme.


## Windows:

- Klicken Sie unten rechts auf die Uhrzeit > "Datums- und Uhrzeiteinstellungen"
- Wählen Sie "Uhrzeit automatisch festlegen" und aktivieren Sie diese Option
- Falls kein Zeitserver automatisch eingetragen ist, können Sie einen beliebigen Server hinzufügen (zum Beispiel ntp.metas.ch)
- Bei Bedarf aktivieren Sie auch die Option "Zeitzone automatisch festlegen"
- Klicken Sie abschliessend auf "Jetzt synchronisieren"

Nun sollte die Uhrzeit synchronisiert sein. Bitte starten Sie anschliessend Ihren Browser neu.

 

## Mac:

- Klicken Sie oben links auf das "Mac-Symbol" > "Systemeinstellungen"
- Klicken Sie links auf "Allgemein" und wählen Sie dann "Datum & Uhrzeit"
- Aktivieren Sie die Option "Zeit und Datum automatisch einstellen" und klicken Sie anschliessend auf "Einstellen". Tragen Sie einen beliebigen Zeitserver ein (zum Beispiel ntp.metas.ch)
- Bei Bedarf aktivieren Sie die Option "Zeitzone automatisch anhand deines Standorts einstellen"

Nun sollte die Uhrzeit synchronisiert sein. Bitte starten Sie anschliessend Ihren Browser neu.