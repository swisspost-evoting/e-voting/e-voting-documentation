# Google Chrome

## Browserverlauf löschen in Google Chrome



1. Klicken Sie auf die **"drei Punkte"** "Google Chrome anpassen und verwalten", hier im Screenshot mit dem Pfeil markiert.
2. Wählen Sie den Menüpunkt **"Einstellungen"**.
3. Wählen Sie den Menüpunkt **"Datenschutz und Sicherheit"**.


4. Klicken Sie nun in dieser Maske unter **"Datenschutz und Sicherheit"** auf **"Browserdaten löschen"**.
5. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
6. Wählen Sie die Kontrollkästchen **"Cookies und andere Websitedaten"** sowie **"Zwischengespeicherte Bilder und Dateien"** aus. 
7. Klicken Sie auf die Menütaste **"Daten löschen"**.


**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/chrome/de/1.png)

