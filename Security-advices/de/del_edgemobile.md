# Edge Mobile

## Browserverlauf löschen in Microsoft Edge

1. Klicken Sie auf die **"drei horizontalen Linien"** ganz unten rechts, hier im Screenshot mit einem Pfeil markiert.
2. Wählen Sie den Menüpunkt **Einstellungen.**
3. Klicken Sie nun in dieser Maske unter **"Datenschutz, Sicherheit und Dienste"** auf **"Browserdaten löschen"**.
4. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
5. Wählen Sie die Kontrollkästchen "Cookies und andere Websitedaten" sowie "Zwischengespeicherte Bilder und Dateien" an.
6. Klicken Sie auf den Menüpunkt **Daten löschen**.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/edge_mobile1.png)




