# Firefox

## Privater-Modus aktivieren in Firefox

1. Klicken Sie auf die **drei horizontalen Linien** ganz oben rechts, hier im Screenshot mit einem Pfeil markiert.
2. Klicken Sie auf **Neues privates Fenster**.
3. Arbeiten Sie im neuen **privaten Fenster** weiter, welches Sie am **dunklen Hintergrund** erkennen.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/firefox/de/1.png)

**Screenshot Schritt: 3**

![Screenshot Schritt: 3](../img/rules/firefox/de/incognito_firefox.png)
<br>
