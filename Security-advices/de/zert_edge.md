# Anleitung zur Überprüfung der Web-Adresse des E-Voting Portals

Prüfung des digitalen Fingerabdrucks (Fingerprint) im Browser


## Microsoft Edge
1. Klicken Sie auf das **Schloss-Symbol** neben der URL um die Website-Informationen anzuzeigen, hier im Screenshot mit dem Pfeil markiert.
2. Klicken Sie nun auf dem offenen Menü auf **"Verbindung ist sicher"**.
3. Klicken Sie auf **Zertifikat-Symbol** oben rechts im Menü.
4. Nun sehen Sie im neuen Menü unter dem Reiter **"Allgemein"** die SHA-256-Fingerabdrücke. Für Sie wichtig ist der Fingerabdruck des Zertifikates. Bitte vergleichen Sie diesen Wert mit dem angegebenen Wert auf Ihrem Stimmrechtsausweis (Doppelpunkte können Sie ignorieren). Er muss identisch sein.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/edge_zert1.PNG)


**Hinweis:** Die Webadresse setzt sich stets wie folgt zusammen: ct.evoting.ch, wobei «ct» für das Kürzel des jeweiligen Kantons steht.