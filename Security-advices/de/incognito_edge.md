# Microsoft Edge

## InPrivate-Modus aktivieren in Microsoft Edge

1. Klicken Sie auf die **"drei Punkte"** "Einstellungen und mehr", hier im Screenshot mit einem Pfeil markiert.
2. Klicken Sie auf **"Neues InPrivate-Fenster"**.
3. Arbeiten Sie im neuen **InPrivate-Fenster** weiter, welches Sie am **dunklen Hintergrund** erkennen.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/p1.png)
<br>

**Screenshot Schritt: 3**

![Screenshot Schritt: 3](../img/rules/edge/de/p2.png)