# Safari Mobile

## Löschen des Browsercaches unter iOS



1. Gehen Sie auf Ihrem iPhone in die **Einstellungen** und wählen Sie **Safari**
2. Wählen Sie **"Verlauf" und "Website daten löschen"** aus.
3. Bestätigen Sie die Meldung, in dem Sie auf **"Verlauf und Daten löschen klicken".**

**Screenshot Schritt: 1**

![Screenshot Schritt: 3](../img/rules/safari/mobile/de/1.png)

**Screenshot Schritt: 2**

![Screenshot Schritt: 3](../img/rules/safari/mobile/de/2.png)

**Screenshot Schritt: 3**

![Screenshot Schritt: 3](../img/rules/safari/mobile/de/3.png)