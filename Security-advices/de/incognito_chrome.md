# Google Chrome

## Inkognito-Modus aktivieren in Google Chrome



1. Klicken Sie auf die **"drei Punkte"** "Google Chrome anpassen und verwalten", hier im Screenshot mit dem Pfeil markiert.
2. Wählen Sie den Menüpunkt **"Neues Inkognitofenster"**.
3. Arbeiten Sie im neuen **Inkognitofenster** weiter, welches Sie am **dunklen Hintergrund** erkennen.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/chrome/de/1.png)

**Screenshot Schritt: 3**

![Screenshot Schritt: 3](../img/rules/chrome/de/incognito_chrome.PNG)
<br>


