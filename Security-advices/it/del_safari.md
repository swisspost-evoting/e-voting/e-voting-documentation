# Apple Safari

## Cancellare la cronologia di navigazione in Safari


1. Fare un clic sul punto del menu **«Safari»**.
2. Fare un clic sul punto del menu **«Cancella cronologia...»**.
3. Selezionare nel menu a tendina almeno l’intervallo di tempo che comprende il processo del vostro voto elettronico. Selezionare per esempio la voce **«Ultima ora»**.
4. Fare un clic sul pulsante del menu **«Cancella cronologia»**.

**Screenshot Passo: 1 + 2**

![Screenshot Passo: 1 + 2](../img/rules/safari/it/1.png)

**Screenshot Passo: 3 + 4**

![Screenshot Passo: 3 + 4](../img/rules/safari/it/2.png)
