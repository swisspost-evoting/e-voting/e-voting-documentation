# Guida per visualizzare e sincronizzare l'orario di sistema

## Generale:
Per consentire il voto, ti raccomandiamo di attivare la sincronizzazione dell'orario sul tuo dispositivo. Di seguito sono riportate brevi istruzioni per vari sistemi operativi.

## Windows:
- Clicca sull'orario in basso a diritto > "Impostazioni data e ora."
- Imposta "Imposta ora automaticamente" su "Attiva."
- Se nessun server di ora è configurato automaticamente, puoi inserire un server qualsiasi (ad esempio, ntp.metas.ch).
- Se necessario, attiva anche l'opzione "Imposta fuso orario automaticamente."
- Fai clic su "Sincronizza ora" successivamente.

Ora il tuo orario dovrebbe essere sincronizzato. Si prega di riavviare il browser ora.

## Mac:
- Clicca sul "Simbolo Apple" in alto a sinistra > "Preferenze di Sistema."
- Clicca su "Generale" a sinistra e quindi seleziona "Data e ora."
- Attiva "Imposta data e ora automaticamente," quindi fai clic su "Imposta" ed inserisci un qualsiasi server di ora (ad esempio, ntp.metas.ch).
- Se necessario, attiva l'opzione "Imposta fuso orario automaticamente in base alla tua posizione."

Ora il tuo orario dovrebbe essere sincronizzato. Si prega di riavviare il browser ora.