# Apple Safari

## Controllo del certificato


1. Fare un clic sul **simbolo del lucchetto** del browser nella barra degli indirizzi, contrassegnato da una freccia nella schermata.
2. Selezionare **"Mostra certificato”**.
3. Scorrere in fondo alla finestra fino a visualizzare **Impronte digitali** e confrontare questo codice con quello riportato sulla tessera elettorale. Deve essere identico.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/edge_zert1.PNG)


**Nota:** L'indirizzo web si compone sempre come segue: ct.evoting.ch, dove "ct" sta per l'abbreviazione del rispettivo Cantone.
