# Istruzioni per la verifica dell'indirizzo web del portale di voto elettronic

Verifica dell'impronta digitale nel browser


## Microsoft Edge

1. Fare un clic sul **simbolo del lucchetto** accanto all'URL per visualizzare le informazioni sul sito web, contrassegnate dalla freccia nell'immagine.
2. Fare un clic su **"La connessione è sicura”** nel menu aperto.
3. Fare un clic su **Icona del certificato** in alto a destra del menu.
4. Ora si vedranno le impronte digitali SHA-256 nel nuovo menu sotto la scheda **"Generale”**. L'impronta digitale del certificato è importante per l'utente. Confrontate questo valore con quello indicato sulla vostra scheda di voto (potete ignorare i punti). Deve essere identico.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/edge_zert1.PNG)

**Nota:** L'indirizzo web si compone sempre come segue: ct.evoting.ch, dove "ct" sta per l'abbreviazione del rispettivo Cantone.