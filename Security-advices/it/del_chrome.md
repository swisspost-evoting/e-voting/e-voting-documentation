# Google Chrome

## Cancellare la cronologia di navigazione in Google Chrome






1. Fare un clic sui **"tre punti"** "Personalizza e gestisci Google Chrome", contrassegnati dalla freccia nell'immagine.
2. Selezionare la voce di menu **"Impostazioni”**.
3. Selezionare la voce di menu **"Privacy e sicurezza”**.


4. Ora fare un clic su **"Elimina dati del browser"** in questa schermata sotto **"Privacy e sicurezza"**.
5. Nell'elenco a discesa, selezionare almeno il periodo di tempo che copre il processo di voto elettronico. Ad esempio, selezionare la voce **"Ultima ora”**.
6. Selezionare le caselle di controllo **"Cookie e altri dati del sito web"** e **"Immagini e file nella cache”**.
7. Fare un clic sul pulsante di menu **"Cancella dati”**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/chrome/de/1.png)
