# Apple Safari

## Modalità di navigazione privata in Safari


1. Fare un clic su **Shelf** per aprire le **Opzioni**.
2. Fare un clic su **Nuova finestra privata**.
3. Continuare a lavorare nella nuova **finestra privata**, che si riconosce per la dicitura **privata**.


**Screenshot Passo: 1 + 2**

![Screenshot Passo: 1 + 2](../img/rules/safari/it/p1.png)
<br>

**Screenshot Passo: 3**

![Screenshot Passo: 3](../img/rules/safari/it/p2.png)