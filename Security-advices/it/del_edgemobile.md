# Microsoft Edge Mobile

## Cencellare la cronlogia di navigazione in Microsoft Edge


1. Fare un clic sulle **"tre linee orizzontali”** in basso a destra, contrassegnate da una freccia nell'immagine.
2. Selezionare la voce di menu **Impostazioni**.
3. Ora fare clic su **"Cancella dati del browser ‘** in questa schermata sotto **"Privacy, sicurezza e servizi”**.
4. Nell'elenco a discesa, selezionare almeno il periodo di tempo che copre il processo di voto elettronico. Ad esempio, selezionare la voce **"Ultima ora”**.
5. Selezionare le caselle di controllo “Cookie e altri dati del sito web” e “Immagini e file nella cache”.
6. Fare un clic sulla voce di menu **Elimina dati**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/edge_mobile1.png)
