# Google Chrome

## Attivare la modalità navigazione privata in Google Chrome


1. Fare un clic sui **“tre punti"** "Personalizza e gestisci Google Chrome”, contrassegnati dalla freccia nell'immagine.
2. Selezionare la voce di menu **"Nuova finestra in incognito”**.
3. Continuare a lavorare nella nuova **finestra in incognito**, riconoscibile dallo **sfondo scuro**.


**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/chrome/de/1.png)

**Screenshot Passo: 3**

![Screenshot Passo: 3](../img/rules/chrome/de/incognito_chrome.PNG)




