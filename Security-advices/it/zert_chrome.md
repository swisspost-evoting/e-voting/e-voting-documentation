# Google Chrome

## Controllo del certificato

1. Fare un clic sul pulsante **"Mostra informazioni sul sito web”** contrassegnato dalla freccia nell'immagine.
2. Ora fare clic su **"La connessione è sicura”** nel menu aperto.
3. Fare clic su **"Il certificato è valido”**.
4. Ora si vedranno le impronte digitali SHA-256 nella nuova finestra sotto la scheda **"Generale”**. L'impronta digitale del certificato è importante per l'utente. Confrontare questo valore con quello indicato sulla carta di voto. Deve essere identico.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/chrome/de/z1.png)

**Nota:** L'indirizzo web si compone sempre come segue: ct.evoting.ch, dove "ct" sta per l'abbreviazione del rispettivo Cantone.
