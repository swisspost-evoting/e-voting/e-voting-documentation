# Istruzioni per verificare nel browser i file JavaScript contenuti nel portale di voto elettronico

## Istruzioni per verificare nel browser i valori hash dei file JavaScript

- Qualora vogliate accertarvi che i file JavaScript utilizzati dal portale ufficiale per il voto elettronico siano invariati, esiste la possibilità di verificare ogni valore hash. Si prega di notare che solo il JavaScript può essere verificato automaticamente, ma non l'integrità del file HTML.
- Questo procedimento è da eseguire prima di esprimere il voto elettronico al fine di escludere la possibilità che i dati JavaScript vengano manipolati o falsificati (sostituiti).
- La seguente descrizione fa riferimento al file JavaScript **main.js** ma può essere utilizzata anche per altri file JavaScript.
- Assicurarsi che non vengano caricati file JavaScript diversi da quelli elencati nel protocollo **(5 file JavaScript)**.

### Procedura con calcolo automatico dei valori hash attraverso il browser (SHA384-Hash)

1. Cercate l’**hash SHA384** dei file JavaScript **main.js** nei protocolli dei Cantoni e degli esperti che sono stati pubblicati. Il link ai protocolli si trova sul portale di voto sotto la voce **Trusted Build**.

2. Aprite il testo sorgente della pagina HTML con un clic destro nel portale di voto.
- Alla fine del file index.html troverete i tag script con i valori hash sotto forma di attributo «integrity». Quest’ultimo ordina al browser di verificare che i valori hash nel tag integrity corrispondano al valore hash del file JavaScript.

Esempio:

```<script src="main.js" type="module" crossorigin="anonymous" integrity="sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0"></script>```

3. Confrontate il **valore hash** visualizzato nel tag integrity con il valore hash indicato nei protocolli pubblicati. Si prega di notare che, conformemente alla [raccomandazione W3C](https://www.w3.org/TR/SRI/), il tag `integrity` mostra il valore hash SHA-384 in [formato Base64](https://www.rfc-editor.org/rfc/rfc4648#section-4), mentre l’SHA-256 calcolato manualmente viene solitamente visualizzato in [formato Base16](https://www.rfc-editor.org/rfc/rfc4648#section-8). Inoltre potete verificare anche i valori hash visualizzati sul portale di voto.

**Se le due sequenze di caratteri sono identiche, il file JavaScript non è stato modificato.**
