# Microsoft Edge

## Cencellare la cronlogia di navigazione in Microsoft Edge


1. Fare un clic sui **“tre puntini"** "Impostazioni e altro”, contrassegnati da una freccia nella schermata.
2. Selezionare la voce di menu **Impostazioni**.
3. Ora fare un clic su **"Elimina dati del browser"** in questa schermata sotto **"Privacy, ricerca e servizi"**, oppure su **"Seleziona elementi da eliminare”**.
5. Nell'elenco a discesa, selezionate almeno il periodo di tempo che copre il vostro processo di voto elettronico. Ad esempio, selezionare la voce **"Ultima ora”**.
6. Selezionate le caselle di controllo “Cookie e altri dati del sito web” e “Immagini e file memorizzati nella cache” e fate clic sulla voce di menu **Elimina ora**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/p1.png)