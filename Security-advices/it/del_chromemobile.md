# Google Chrome Mobile

## Cancellare la cache del browser su iOS


1. Fare un clic sui **“tre puntini"** "Personalizza e gestisci Google Chrome”, contrassegnati dalla freccia nell'immagine.
2. Fare un clic sulla voce di menu **Privacy e sicurezza**.
3. Selezionare la voce di menu **Elimina dati del browser**.
4. Nell'elenco a discesa, selezionare almeno il periodo di tempo che copre il processo di voto elettronico. Ad esempio, selezionare la voce **"Ultima ora”**.
5. Selezionare le caselle di controllo “Cookie e altri dati del sito web” e “Immagini e file memorizzati nella cache”.
6. Fare un clic sul pulsante di menu **Elimina dati**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/chrome/de/chrome_mobile_incognito.png)
