# Microsoft Edge

## Abilitare la modalità InPrivate in Microsoft Edge


1. Fare un clic sui **“tre puntini"** ’Impostazioni e altro”, contrassegnati da una freccia nella schermata.
2. Fare un clic su **"Nuova finestra InPrivate”**.
3. Continuare a lavorare nella nuova **finestra privata**, riconoscibile dallo **sfondo scuro**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/p1.png)
<br>

**Screenshot Passo: 3**

![Screenshot Passo: 3](../img/rules/edge/de/p2.png)