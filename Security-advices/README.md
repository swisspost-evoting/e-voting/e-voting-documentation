# Security advices

Voters can ensure the e-voting process is successful by voting according to the instructions. Additionally, voters can carry out various checks on the device used for voting to ensure their vote has not been manipulated. The various measures are laid out here. 

## The security advices are available in the following languages:

- [Security advices](en/readme.md)
- [Sicherheitsratschläge](de/readme.md)
- [Conseils de sécurité](fr/readme.md)
- [Consigli di sicurezza](it/readme.md)
- [Cussegls per la segirezza](rm/readme.md)