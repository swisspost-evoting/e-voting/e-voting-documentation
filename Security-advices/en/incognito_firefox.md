# Firefox

## Enable private mode in Firefox


1. Click on the **three horizontal lines** at the top right, **marked with an arrow in the screenshot**.
2. Click on **New private window**.
3. Continue working in the new **private window**, which you can recognise by the **dark background**.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/firefox/de/1.png)

**Screenshot Step: 3**

![Screenshot Step: 3](../img/rules/firefox/de/incognito_firefox.png)
<br>
