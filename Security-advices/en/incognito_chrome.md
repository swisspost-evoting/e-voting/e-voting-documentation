# Google Chrome

## Activate incognito mode in Google Chrome

1. Click on the **three dots** "Customise and manage Google Chrome", **marked with the arrow in the screenshot**.
2. Select the menu item **New incognito window**.
3. Continue working in the new **Incognito window**, which you can recognise by the **dark background**.

**Screenshot Step: 1**

![](../img/rules/chrome/de/1.png)

**Screenshot Step: 3**

![Screenshot Step: 3](../img/rules/chrome/de/incognito_chrome.PNG)
<br>

