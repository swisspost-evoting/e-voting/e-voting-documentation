# Apple Safari

## Deleting browsing history in Safari

1. Click on the **Safari** menu item.
2. Click on the **Delete history...** menu item.
3. Select in the dropdown list at least the period that includes the process of your electronic voting. For instance, select the **Last hour** entry.
4. Click on the **Delete history** menu item.

**Screenshot Step: 1 + 2**

![Screenshot Step: 1 + 2](../img/rules/safari/en/1.png)


**Screenshot Step: 3 + 4**

![Screenshot Step: 3 + 4](../img/rules/safari/en/2.png)
