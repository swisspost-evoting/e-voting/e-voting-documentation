# Apple Safari

## Certificate authentication

1. Click on the lock symbol **in the address line**.
2. Select **Show certificate**.
3. Scroll to the end of the window until you see **fingerprints** and compare this code with that on your voting card.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/edge/de/edge_zert1.PNG)


**Note:** The web address is always composed as follows: ct.evoting.ch, where ‘ct’ stands for the abbreviation of the respective canton.