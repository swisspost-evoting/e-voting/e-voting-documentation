# Microsoft Edge

## Enable InPrivate Mode in Microsoft Edge


1. Click on the **three dots** to open the **options**.
2. Click on **New InPrivate Window**
3. Continue working in the new **InPrivate window**, which you can recognise by the **dark background**.

**Screenshot Step: 1**

![](../img/rules/edge/de/p1.png)
<br>

**Screenshot Step: 3**

![Screenshot Step: 3](../img/rules/edge/de/p2.png)