# Microsoft Edge Mobile

## Deleting browsing history in Microsoft Edge




1. Click on the **"three horizontal lines"** at the bottom right, **marked with an arrow in the screenshot.**
2. Select the menu item **Settings**.
3. Now click on **"Delete browser data“** under **”Privacy, search and services“**, then on **”Select elements to be deleted"**.
5. In the drop-down list, select at least the time period that covers your electronic voting process. For example, select the entry **"Last hour"**.
6. Tick the checkboxes "Cookies and other website data" and "Cached images and files" and click on the menu item **Delete now**.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/edge/de/edge_mobile1.png)