# Google Chrome

## Certificate authentication

1. Click on the button "Show website information" **marked with the arrow in the screenshot.**
2. Now click on **"Connection is secure"** in the open menu.
3. Click on **"Certificate is valid"**.
4. You will now see the SHA-256 fingerprints in the new window under the category **"General"**. The fingerprint of the certificate is important for you. Please compare this value with the value stated on your voting card.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/chrome/de/z1.png)

**Hinweis:** The web address is always composed as follows: ct.evoting.ch, where ‘ct’ stands for the abbreviation of the respective canton.