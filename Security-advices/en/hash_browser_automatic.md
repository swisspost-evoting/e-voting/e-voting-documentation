# Instructions on how to verify the JavaScript files of the e-voting portal in the browser

## Verifying the hash value of the JavaScript files in the browser

- If you would like to check that the JavaScript files used by the official e-voting portal have not been altered, you can verify the relevant hash values. Please note that only the JavaScript can be automatically verified, not the integrity of the HTML file.
- If you do this before you cast your e-vote, you can rule out the possibility of the JavaScript files being manipulated or falsified (replaced).
- The following description is based on the JavaScript file **main.js**, though it may alo- be used for other JavaScript files.
- Please ensure that no JavaScript files other than those listed in the protocol are loaded **(5 JavaScript files)**.

### Procedure with automatic calculation of the hash values by the browser (SHA384-Hash)

1. Search for the **SHA384 hash** of the **main.js** JavaScript files in the published protocols of the cantons and the experts. You can find the link to the minutes on the voting portal under the heading **Trusted Build**.

2. To open the source code of the HTML page, right click in the voting portal.
- At the end of the index.html file you will find the relevant scripts tags with the hash values as an “integrity” attribute. The integrity attribute instructs the browser to check if the hash value in the integrity tag matches the hash value of the JavaScript file.

Sample:

```<script src="main.js" type="module" crossorigin="anonymous" integrity="sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0"></script>```


3. Compare the **hash value** displayed in the integrity tag with the hash value displayed in the published protocols. Please note that according to the [W3C recommendation](https://www.w3.org/TR/SRI/), the `integrity` tag displays the SHA–384 hash value in [Base64 format](https://www.rfc-editor.org/rfc/rfc4648#section-4), while the manually calculated SHA–256 is usually displayed in [Base16 format](https://www.rfc-editor.org/rfc/rfc4648#section-8). You can also check the hash values displayed on the election and voting portal.

**If the two strings are identical, the JavaScript file hasn’t been modified.**
