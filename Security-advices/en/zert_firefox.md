# Firefox

## Certificate authentication


1. Click on the green lock next to the address line and then on the arrow **>** .
2. Select **More information**.
3. Under the **Security** menu, click on **Display certificate**.
4. Compare this fingerprint with that on your voting card. It **must be identical**.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/edge/de/edge_zert1.PNG)


**Note:** The web address is always composed as follows: ct.evoting.ch, where ‘ct’ stands for the abbreviation of the respective canton.