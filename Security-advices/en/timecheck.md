# Guide to Displaying and Synchronizing System Time

## General:
To enable voting, we recommend activating time synchronization on your device. Below are brief instructions for various operating systems.

## Windows:
- Click on the time at the bottom right > "Date and Time Settings."
- Set "Set time automatically" to "On."
- If no time server is automatically configured, you can enter any server (e.g., ntp.metas.ch).
- If needed, turn on "Set time zone automatically."
- Click "Sync Now" afterward.

Your time should now be synchronized. Please restart your browser now.

## Mac:
- Click on the "Apple symbol" at the top left > "System Preferences."
- Click on "General" on the left and then select "Date & Time."
- Enable "Set date and time automatically," then click "Set" and enter any time server (e.g., ntp.metas.ch).
- If needed, activate the option "Set time zone automatically based on your location."

Your time should now be synchronized. Please restart your browser now.