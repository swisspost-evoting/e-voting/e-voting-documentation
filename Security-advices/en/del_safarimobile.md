# Safari Mobile

## Deleting the browser cache in iOS

1. In your iPhone, go to **Settings** and select **Safari**
2. Select **Clear history and website data**.
3. Confirm your selection and click on **"Clear History and Data".**

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/safari/mobile/en/1.png)

**Screenshot Step: 2**

![Screenshot Step: 2](../img/rules/safari/mobile/en/2.png)

**Screenshot Step: 3**

![Screenshot Step: 3](../img/rules/safari/mobile/en/3.png)