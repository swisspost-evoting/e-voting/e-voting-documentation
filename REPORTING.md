# Ways to submit

You can hand in your contribution on three distinct channels:  

- It is preferable that you report your findings to us via [GitLab](https://gitlab.com/swisspost-evoting), where you will find an overview of the various reports and can participate in discussions with our e-voting team and the community.  

- Alternatively, you can enter your report via [encrypted e-mail or an online form.](https://evoting-community.post.ch/en/contributions/submitting-findings)

- As part of its [bug bounty programme](https://yeswehack.com/programs/swiss-post-evoting), Swiss Post rewards anyone who reports a confirmed vulnerability. You can apply retroactively for a reward for confirmed reports submitted via GitLab or the online form by entering them additionally on the bug bounty platform or submitting them directly on Yes We Hack.  

> Please read the [Code of Conduct](CONTRIBUTING.md) before you submit a report to us.

---
  
## Submit findings on GitLab

Create a new issue under any of our projects and select the **`Security and Bug`** template from the dropdown.

## Scope of findings

You can analyse our artefacts by completing:

- a static test of documentation and source code
- dynamically testing a self-deployed and running instance of our system
- a periodical public intrusion test with a duration of 4 weeks each temporarily complements the e-voting community bug bounty programme

### Content of findings

We expect a finding to include:

- a basic description of the issue
- a classification of the criticality of the issues (see below)
- a list of affected code, document and their precise location in the GitLab repository
- a reproduction guide for the issue
- accompanying evidence (e.g. screenshots, videos, proof of concept code, dumps, etc.) if possible
- contact information for clarifications (recommended)

### Severity of findings

Please use this scheme to assess the severity of your findings, when you report via GitLab and the online form.

**Critical**  
The exploitation of the finding compromises the application, the system or the voting process severely.  
The exploitation is straightforward to set up. [Important note](#Important-note)

**High**  
The exploitation of the finding could potentially compromise the application, the system or the voting process.  
The exploitation is hard to set up. [Important note](#Important-note)
  
**Medium**
The exploitation of the finding provides limited non-critical access.  
The exploitation of the finding requires the attacker to have privileges, to social engineer or reside in a local network.  
The exploitation is very difficult to set up.  

**Low**  
The exploitation of the finding has very little impact on the application, the system or the voting process.  
The exploitation of the finding requires the attacker to have physical access to the system, or voting materials.  
Findings that have no direct impact on the application, system or voting process also fall into this category. (e.g. typos in the GitLab Markdowns or in documents.  

### Important note

If you consider the severity of the finding to be high or critical, then we ask you to check the **confidential**-checkbox in the template. Issues reported via GitLab are public by default.
So that we can coordinate its publication according to the coordinated vulnerability disclosure.  

## Submit **improvements** on GitLab

Submit an improvement on GitLab by creating a [new issue](https://gitlab.com/groups/swisspost-evoting/-/issues), and selecting the **`Improvements`** template from the dropdown.  
  
## Submit **questions** on GitLab

Create a new issue under any of our projects and select the **`Questions`** template from the dropdown.  
  
## Anonymity and confidentiality of your contributions

### Anonymity

On GitLab, you have the choice to work under a nickname and not disclose your personal information.  

### Confidentiality  

With regards to the confidentiality of your contribution, you are free to set a report as confidential with a checkbox below each report-option, so that your findings is only shared with the Swiss Post initially. Yet remember we aim to ultimately disclose all findings to the public, so we will get in touch with you to see how we could make your contribution public.

## Additional information & support

If you have any other unanswered questions or just want to find out more about how to contribute on Gitlab, please visit
[GitLab Docs](https://docs.gitlab.com/ee/user/project/issues/)

Find answers to frequently asked questions about the community programme, definition of terms and more on our
[community website](https://evoting-community.post.ch/en/).

If you have additional questions or require help, please do not hesitate to [contact us](https://evoting-community.post.ch/en/help-and-contact/support#contact). Our specialists will be pleased to answer your questions regarding cryptography, security, documentation, the Code of Conduct or any other subject relating to Swiss Post’s e-voting system.
