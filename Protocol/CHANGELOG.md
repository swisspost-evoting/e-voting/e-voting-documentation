# Changelog of the *Protocol of the Swiss Post Voting System - Computational Proof of Complete Verifiability and Privacy*

## Changes in version 1.3.0

Version 1.3.0 includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

- Vanessa Teague (Thinking Cybersecurity), Olivier Pereira (Université catholique Louvain), Thomas Edmund Haines (Australian National University)
- Aleksander Essex (Western University Canada)
- Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.3.0 contains the following changes:

- Removed the redundant games after removal of the unnecessary proofs in the CreateLCCShare and CreateLVCCShare algorithms.
- Provided explicit bounds for the electoral board password and discussed the implications when only one electoral board member (and the tally control component) is honest.
- Extended the analysis of universal verifiability with the analysis of vote validity. Strengthened the link of the computational proof to the verifier specification.
- Aligned the computational proof to the minor protocol changes of the e-voting release 1.4.0.

---

## Changes in version 1.2.0

Version 1.2.0 includes some feedback from the Federal Chancellery's mandated experts (see above) and contains the following change:

- Updated the section about the abstraction of voter authentication with a more detailed justification and references to the algorithms in the system specification (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).

---

## Changes in version 1.1.0

Version 1.1.0 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

- Improved the security analysis' use of extractors and added multiple clarifications (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Improved the vote privacy security analysis by adding additional game hops (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Added a reference to the system specification and clarified the handling of inconsistent views among control components and its impact on the security analysis (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Improved multiple definitions in the section on proof systems.
- Clarified the trust assumptions on the protocol participants for different security objectives (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the definition of weak pseudorandom functions (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Added a section on proving that no unallocated credentials were used.
- Replaced the building block symmetric encryption with authenticated encryption with associated data (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).

---

## Changes in version 1.0.0

Version 1.0.0 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

- Updated the security analysis to the latest version of the Federal Chancellery's Ordinance.
- Incorporated the protocol changes from the system specification version 1.0.0
- Added a section to address potential inconsistencies between control components (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines, addresses [GitLab issue #1](https://gitlab.com/swisspost-evoting/verifier/verifier/-/issues/1))
- Improved the clarity of the Return Codes Mapping Table Correctness proof  (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Clarified the definition of Individual Verifiability (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Improved the clarity of each individual game hop and included numerous explicit reductions to make the security analysis more convincing (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Changed the formal vote privacy definition from BPRIV to Benaloh (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Removed the redundant protocol description (fixes [GitLab issue #25](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/25))
- Added general improvements and clarifications to the computational proof (addresses [GitLab issue #3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3))

---

## Changes in version 0.9.12

Version 0.9.12 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

- Updated the threat model and security objectives to the latest version of the Federal Chancellery's Ordinance.
- Detailed the key generation algorithm in the multi-recipient ElGamal setting (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Provided additional details and justification to the hash function and key derivation section (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Replaced PBKDF with Argon2 (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Clarified the security definitions of the non-interactive zero-knowledge proof systems (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Provided additional details on the Common Reference String and the selection of the parameters of the verifiable mix net (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Fixed some errors in the definitions of the computational hardness assumptions (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed minor mistakes and typos in the document.

## Changes in version 0.9.11

Version 0.9.11 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

- Updated the computational proof to the latest draft of the Federal Chancellery's Ordinance on Electronic Voting (OEV) (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis). This includes renaming the algorithms SetupTallyPO and GenEncryptionKeysPO to SetupTallyEB and GenSetupEncryptionKeys.
- Clarified the verifiable generation of small primes encoding the voting options (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Rewritten the section on key derivation and hash functions (incorporates feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Simplified the proof of Return Codes Mapping Table Correctness by removing the KDF-advantage in the security analysis (obsolete in the random oracle model).
- Added a hash of the Ballot Casting Key in the algorithms GenVerDat and CreateConfirmMessage to allow the reduction to the SGSP problem in the privacy security analysis (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Provided a section on the mix net's security properties and on how to simulate mix net proofs for perfect special honest verifier zero-knowledge (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Defined a fully collision-resistant RecursiveHash function with a uniform output in G_q (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Removed the DeriveKey method and replaced it with a KDF function based on the HKDF-expand algorithm.
- Updated the SetupVoting algorithm's sequence diagram by separating setup and printing component.
- Clarified the transmission of the election public key to the control components (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the definition of the decisional Diffie-Hellmann (DDH) game (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the compliant ballot definition (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Streamlined the definition of the auxiliary strings in the zero-knowledge proof systems.
- Fixed minor mistakes and typos in the document.

## Changes in version 0.9.10

Version 0.9.10 of the computational proof contains the following changes:

- Strengthened  the protocol to verify sent-as-intended properties and vote correctness online in the control components. See Gitlab issue [#7](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/7) for further explanations.
- Specified a rollback scenario for incorrectly inserted or discarded votes (tally phase). See Gitlab issue [#8](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/8) for further explanations.
- Expanded the section on two-round return code schemes with alternative scenarios.
- Switched from single-recipient to multi-recipient ElGamal encryption of the Choice Return Codes in the configuration phase.
- Streamlined the usage and name of lists between the computational proofs and the system specifications.
- Improved clarity of sequence diagrams.

## Changes in version 0.9.9

Version 0.9.9 of the computational proof contains the following changes:

- Fixed the error in the GenCMTable algorithm allowing the adversary to learn the set of possible return codes (reported by Thomas Haines in Gitlab [#2](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/2)).
- Added an introduction about two-round return code schemes.
- Referred to standard notions of security (IND-CPA) in the description of the ElGamal and symmetric encryption scheme.
- Provided more explanation on key compression in the ElGamal multi-recipient encryption algorithm. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Verifiable mix net: Detailed the case of m=1 where the number of mixed votes is prime. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Added a sequence diagram for the verifications in the voting phase.
- Removed the Schnorr proof of knowledge, since the combination of plaintext equality proof and exponentiation proof prove equivalent knowledge.
- Added additional auxiliary values to the decryption and plaintext equality proof systems. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Aligned the order of certain algorithms in the protocol description (section 12).
- Removed verification algorithms of untrustworthy components for the sake of readability.
- Expanded the explanations on the length of the voter's codes.
- Fixed smaller typos and errors in various sections.